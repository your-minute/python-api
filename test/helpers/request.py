import json
import sys
import urllib.parse as urllib

from flask import current_app

from conftest import app
from your_talent.classes import Registry


class UTRequestHelper(object):
    BASE_URL = 'http://{}:{}'.format(
        Registry.registered('config').get('server', 'host'),
        Registry.registered('config').get('server', 'port')
    )

    def __init__(self):
        self.default_url = ''
        self.default_method = 'get'
        self.default_url_params = []
        self.default_url_args = {}
        self.default_body = {}
        self.default_admin = None
        self.default_customer = None
        self.default_driver = None
        self.last_request_url = ''
        self.last_request_method = ''
        self.last_request_headers = {}
        self.last_request_body = {}
        self.last_response = {}
        self.last_raw_response = {}

    def reset(self):
        self.__init__()

    def set_default(
            self, url=None, method=None, url_params=None,
            url_args=None, body=None, admin=None, customer=None, driver=None
    ):
        if url:
            self.default_url = url
        if method:
            self.default_method = method

        self.default_url_params = url_params if url_params else []
        self.default_url_args = url_args if url_args else {}
        self.default_body = body if body else {}
        self.default_admin = admin if admin else None
        self.default_customer = customer if customer else None
        self.default_driver = driver if driver else None

        return True

    def build_url_param_str(self, url, url_params=None, url_args=None):
        url_args_str = ''
        if url_args and isinstance(url_args, dict):
            url_args_str = '?' + str(urllib.urlencode(url_args))

        # convert url_params to array if string provided
        if isinstance(url_params, str):
            url_params = [url_params]
        elif not isinstance(url_params, list):
            url_params = []
        return UTRequestHelper.BASE_URL + url.format(*url_params) + url_args_str

    def emit(self, url=None, method=None, url_params=None, url_args=None, body=None,
             admin=None, customer=None, driver=None, prevent_default_user=False,
             force_empty_body=False, json_answer=True):
        with app.app_context():
            app_ctx = current_app.test_client()

            if not url:
                url = self.default_url
            if not method:
                method = self.default_method
            if not url_params:
                url_params = self.default_url_params
            if not url_args:
                url_args = self.default_url_args
            if not body:
                body = self.default_body
            if force_empty_body:
                body = {}

            if not prevent_default_user:
                if not admin:
                    admin = self.default_admin
                if not customer:
                    customer = self.default_customer
                if not driver:
                    driver = self.default_driver

            self.last_request_url = self.build_url_param_str(url, url_params, url_args)
            self.last_request_method = method
            method_callable = getattr(app_ctx, method)

            self.last_request_body = body

            self.last_request_headers = {}
            token = None
            # if admin:
            #     token = UTContextHelper.get_implicit_token(valid=True, user_type='admin', user_id=admin['id'])
            #     assert token, 'Impossible to find a valid token in db for admin ' + str(admin['id'])
            # elif customer:
            #     token = UTContextHelper.get_implicit_token(valid=True, user_type='customer', user_id=customer['id'])
            #     assert token, 'Impossible to find a valid token in db for customer ' + str(customer['id'])
            #
            # elif driver:
            #     token = UTContextHelper.get_implicit_token(valid=True, user_type='driver', user_id=driver['id'])
            #     assert token, 'Impossible to find a valid token in db for driver ' + str(driver['id'])
            if token:
                self.last_request_headers = {
                    'Authorization': 'Bearer ' + token['access_token']
                }

            self.last_raw_response = method_callable(
                self.last_request_url,
                headers=self.last_request_headers,
                data=self.last_request_body
            )
            try:
                self.last_response = json.loads(self.last_raw_response.get_data().decode(sys.getdefaultencoding()))
            except Exception:
                # handle server error as HTML format
                self.last_response = self.last_raw_response.get_data()
                if json_answer:
                    assert False, self.debug()
            finally:
                return self.last_response

    def debug(self, display_last_response=True, display_last_body=True, print_result=True):
        header_color = '\033[1;34m'
        header_color_end = '\033[0m'

        postman_str = '{}Last request URL :{}\n[{}]{}\n{}Last request headers :{}'.format(
            header_color, header_color_end, self.last_request_method.upper(),
            self.last_request_url, header_color, header_color_end
        )
        if self.last_request_headers:
            for k in self.last_request_headers.keys():
                postman_str += '\n{}:{}'.format(k, self.last_request_headers[k])
        else:
            postman_str += '\n(None)'

        if display_last_body:
            postman_str += '\n{} Last request body:{}'.format(header_color, header_color_end)
            for k in self.last_request_body.keys():
                if k != 'logo_data':
                    postman_str += '\n{}:{}'.format(k, self.last_request_body[k])
                else:
                    postman_str += '\n{}: <blob_data>'.format(k)

        if display_last_response:
            postman_str += '\n{} Last response : {}\n{}'.format(
                header_color, header_color, str(self.last_response)[:3000]
            )

        if print_result:
            # print string for easy copy/paste to postman
            print(postman_str)
            return ''
        # return last response str
        return postman_str

    def debug_request(self):
        print('default_url: ' + str(self.default_url))
        print('default_method: ' + str(self.default_method))
        print('default_url_params: ' + str(self.default_url_params))
        print('default_url_args: ' + str(self.default_url_args))
        print('default_body: ' + str(self.default_body))
        print('default_admin: ' + str(self.default_admin))
        print('default_customer: ' + str(self.default_customer))
        print('default_driver: ' + str(self.default_driver))
        print('last_request_url: ' + str(self.last_request_url))
        print('last_request_method: ' + str(self.last_request_method))
        print('last_request_headers: ' + str(self.last_request_headers))
        print('last_request_body: ' + str(self.last_request_body))
        print('last_response: ' + str(self.last_response))

    def is_error(self, expected_status=None, subcode_contains=None, details_contains=None):
        if not expected_status or (not isinstance(expected_status, int) and not isinstance(expected_status, list)):
            expected_status = [400, 401, 403, 404, 500]
        elif isinstance(expected_status, int):
            expected_status = [expected_status]

        if 'status' not in self.last_response or self.last_response['status'] not in expected_status:
            return False

        if (
                subcode_contains
                and
                (
                        'subcode' not in self.last_response or
                        self.last_response['subcode'].find(subcode_contains) == -1
                )
        ):
            return False

        return (
                details_contains and
                (
                        'details' not in self.last_response
                        or self.last_response['details'].find(details_contains) == -1
                )
        )
