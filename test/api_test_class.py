from http import HTTPStatus

from your_talent.helpers import TextHelper


class YourModelUnitTestSet(object):
    url = ''
    method = ''
    request = None
    unit_test_set_name = 'undefined'

    mandatory_fields = list()
    base_body = dict()
    assert_invalid_base_object = dict()

    def set_unite_test_set_name(self, name):
        self.unit_test_set_name = name

    def assert_invalid_field(
            self, field_name, value, subcode_contains, test_name='',
            error_code=HTTPStatus.BAD_REQUEST.value, base_obj=None
    ):
        if TextHelper.is_int(error_code):
            error_code = error_code
        else:
            error_code = HTTPStatus.BAD_REQUEST.value

        if not base_obj:
            base_obj = self.assert_invalid_base_object

        body = base_obj.copy()

        body[field_name] = value
        self.request.emit(
            body=body
        )

        assert (
            self.request.is_error(error_code, subcode_contains=subcode_contains),
            '{} wiht {} set to {} did not failed as expected\n{}\n{}'.format(
                self.unit_test_set_name, field_name, body[field_name],
                test_name, self.debug()
            )
        )

    #######
    # METHODS TO CHECK MANDATORY FIELDS
    ######
    def check_mandatory_fields_missing(self, mandatory_fields=None, base_body=None):
        mandatory_fields = self.mandatory_fields if not mandatory_fields else mandatory_fields
        base_body = self.base_body if not base_body else base_body

        if len(mandatory_fields) > 1:
            for k in mandatory_fields:
                body_incomplete = base_body.copy()
                del body_incomplete[k]

                self.request.emit(
                    body=body_incomplete
                )
                filed = TextHelper.to_camel_case(k)
                assert (
                    self.request.is_error([400], subcode_contains="Missing" + filed),
                    'Request did not failed without field {} did not find "Missing{}" in  error subcode.\n{}'.format(
                        k, filed, self.debug()
                    )
                )
        else:
            self.request.emit(
                force_empty_body=True
            )
            assert (
                self.request.is_error([400], subcode_contains='RequestEmpty'),
                'Request did not failed without empty body in 400 error subcode.' + self.debug()
            )

    def check_mandatory_fields_empty(self, mandatory_fields=None, base_body=None):
        mandatory_fields = self.mandatory_fields if not mandatory_fields else mandatory_fields
        base_body = self.base_body if not base_body else base_body

        for k in mandatory_fields:
            body_w_empty_field = base_body.copy()
            body_w_empty_field[k] = ''

            self.request.emit(
                body=body_w_empty_field
            )
            field = TextHelper.to_camel_case(k)
            assert (
                self.request.is_error([400], subcode_contains='Missing' + field),
                'Request did not fail with field {} empty, did not find "Empty{}" in 400 error subcode.\n{}'.format(
                    k, field, self.debug()
                )
            )

    #######
    # DEBUG
    ######
    def print_debug_title(self, title, print_result=True):
        title_str = "\n\033[1;92m" + title + "\033[0m"
        if print_result:
            print(title_str)
            return ""
        return title_str

    def get_test_header_details(self, print_result=True):
        header = "\n\033[1;92mUnit test set Name: " + str(self.unit_test_set_name) + "\033[0m"
        if print_result:
            print(header)
            return ""
        return header

    def debug(self, display_last_response=True, display_last_body=True):
        return (
            '{}{}{}{}{}'.format(
                self.get_test_header_details(),
                self.print_debug_title("Last request details:"),
                self.request.debug(display_last_response, display_last_body),
                self.print_debug_title("Last SQL Requests:"),
                UTContextHelper.get_last_queries_as_str()
            )
        )
