import importlib

from api import create_app
from your_talent.helpers import IO


def load_modules(path):
    for task in IO.get_files(path, "py"):
        if task != "__init__.py":
            importlib.import_module(path.replace('/', '.') + "." + task.replace(".py", ""))


app, celery = create_app(worker=True)

load_modules("your_talent/tasks")
load_modules("your_talent/cron")
