# YourTalent API

YourMinute python API.

## Aim

The One Minute API is onboarding all the technical functionnalities for the application.
The API has to manage every things allowed by the application expect file uploading (need to be discussed).
The API must be strong and support large amounts of request as well as being easy to maintain, deploy and enriched.

## Install

**Current prod python version: 2.7.12**

### Prerequisites

- Python `virtualenv`
- `pip3`

### Venv usage

- Create: `virtualenv .venv -p /usr/bin/python3.7`
- Connect: `source .venv/bin/activate`

### Requirements

`pip install -r requirements.txt`

## Run API

- `./api.py` or `python api.py`

## Database schema

*keep it simple*

