from api import create_app


application, _ = create_app()

if __name__ == '__main__':
    application.run()
