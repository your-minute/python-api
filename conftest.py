from configparser import ConfigParser
import os

import pytest
from faker import Faker
from sqlalchemy import create_engine
from api import create_app


class DBConnector(object):
    user = ""
    password = ""
    host = ""
    db = ""

    def __init__(self, user, password, host, port, db, db_kind="mysql"):
        if user:
            self.user = user
        if password:
            self.password = password
        if host:
            self.host = host
        if db:
            self.db = db
        if port:
            self.port = port
        self.db_kind = db_kind
        self.tables = []
        self.total_rows_insert = 0
        self.__connect()

    def __connect(self):
        pw = ":" + self.password if self.password else ""
        po = ":" + self.port if self.port else ""
        self.src = "{0}://{1}{2}@{3}{4}/{5}".format(self.db_kind, self.user, pw, self.host, po, self.db)
        self.engine = create_engine(self.src)
        self.con = self.engine.connect()

    def __close_connection(self):
        if self.engine:
            self.con.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__close_connection()

    def execute(self, command):
        self.con.execute(command)


# def init_data(database):
#     commands = data.split(';\n')
#     for command in commands:
#         try:
#             database.execute(command)
#         except Exception as e:
#             print('Command skipped: ', e)


# @todo Don't forget to uncomment before merge

# # # get config
# print(">>>>>>> DB Initialisation")
# conf_file = "./config.test.ini"
# if "ENV_TYPE" in os.environ:
#     if os.environ["ENV_TYPE"] == "local":
#         print("Loading local env")
#         conf_file = "./config.local.ini"
# config = ConfigParser()
# config.read(conf_file)
#
# # init database
# script_file = open('database/init_struct.sql')
# struct = script_file.read()
# script_file.close()
#
# script_file = open('database/new_test_v1.sql')
# data = script_file.read()
# script_file.close()
#
# with DBConnector(
#         config.get("database", "user"),
#         config.get("database", "password"),
#         config.get("database", "host"),
#         config.get("database", "port"),
#         config.get("database", "database")
# ) as db:
#     commands = struct.split(';\n')
#     for command in commands:
#         try:
#             db.execute(command)
#         except Exception as e:
#             print('Command skipped: ', e)
#
#     init_data(db)
#
#     print(">>>>>>> DB Ok")

app = create_app()
fake = Faker('fr_FR')

#
# @pytest.fixture(scope='class', autouse=True)
# def reset_database():
#     with DBConnector(
#             config.get("database", "user"),
#             config.get("database", "password"),
#             config.get("database", "host"),
#             config.get("database", "port"),
#             config.get("database", "database")
#     ) as database:
#         init_data(database)
