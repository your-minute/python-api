#!/usr/bin/env python3.7
import configparser
import logging
import os

# noinspection PyUnresolvedReferences
from celery import Celery
from flask import Flask
from flask_cors import CORS

from your_talent import YourTalent


app_name = __name__

# For locals tests
os.environ['TZ'] = 'GMT'

conf_file = "./config.local.ini"
# Config loading
if "ENV_TYPE" in os.environ:
    env = os.environ['ENV_TYPE'].lower().replace(' ', '.').replace('_', '.')
    conf_file = "./config.{}.ini".format(env)
    logging.info('Loading env: {}'.format(env))

config = configparser.ConfigParser()
config.read(conf_file)


def create_app(worker=False):
    """
        App creation function
    """
    a = Flask(app_name)
    if config.getboolean('log', 'alchemy'):
        a.config['SQLALCHEMY_ECHO'] = True
    CORS(a, supports_credentials=True)
    celery = make_celery(a)
    YourTalent.register(a, config, celery, worker)
    # from your_talent.oauth import config_oauth
    # config_oauth(a)
    return a, celery


#  Celery initialisation
def make_celery(app):
    celery = Celery(
        app.import_name,
        backend=config.get('celery', 'broker_url'),
        broker=config.get('celery', 'broker_url')
    )


    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)


    celery.Task = ContextTask

    celery.config_from_object({
        'task_always_eager':          config.getboolean('celery', 'eager_mode'),
        'task_create_missing_queues': True,
    })
    celery.conf.task_routes = {
        'notifications.*': {'queue': 'celery'},
    }
    return celery


app, celery = create_app()
migrate = YourTalent.migrate

if __name__ == '__main__':
    # register application
    # Running main loop
    app.run(host=config.get('server', 'host'), port=int(config.get('server', 'port')))
