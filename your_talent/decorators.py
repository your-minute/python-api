import math
import time
from functools import wraps
from http import HTTPStatus

import jwt
from flask import jsonify, request
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

from your_talent.classes import Logger, Registry
from your_talent.errors import AuthError, InvalidFieldError, MissingFieldError, UnauthorizedError
from your_talent.helpers import FormsHelper, TextHelper
from your_talent.models import OAuth2Token, Role, User


logger = Logger(__name__)

limiter = Limiter(
    key_func=get_remote_address,
    default_limits=['2000 per day']
)


def app_context(app):
    def inner(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            with app.app_context():
                return func(*args, **kwargs)

        return wrapper

    return inner


def requires_auth(raise_error=True, **_dec_kwargs):
    """
    route decorator to ensure user is auth
    """

    def wrapper(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            def set_user_registry():
                user = User.query.get(token.user_id)
                if raise_error and (not user or user.deleted):
                    raise AuthError()
                if user and not user.deleted:
                    Registry.register('current-role', user.role)
                    Registry.register('current-role-name', user.role.name)
                    Registry.register('current-user', user)
                    Registry.register('current-user-id', user.id)
                    Registry.register('current-first-name', user.first_name)
                    Registry.register('current-last-name', user.last_name)
                    Registry.register('current-user-name', user.name)

            config = Registry.registered('config')
            # set default current-role, current-user ... to guest
            guest = Role.query.get('guest')
            Registry.register('current-role', guest)
            Registry.register('current-role-name', 'guest')
            Registry.register('current-user', None)
            Registry.register('current-user-id', None)
            Registry.register('current-first-name', None)
            Registry.register('current-last-name', None)

            provided_token = request.headers.get('Authorization', 'a  ').split(' ')[1]
            token = OAuth2Token.query.filter_by(access_token=provided_token).first()
            if not provided_token or not token:
                provided_token = request.cookies.get('token')
                if provided_token:
                    provided_token = jwt.decode(
                        provided_token,
                        config.get('jwt', 'key'),
                        algorithm=config.get('jwt', 'algorithm')
                    )
                    token = OAuth2Token.query.filter_by(access_token=provided_token['access_token']).first()

            if raise_error and (not token or token.is_refresh_token_expired()):
                raise AuthError()
            if token:
                set_user_registry()
            return f(*args, **kwargs)

        return decorated

    return wrapper


def requires_rights(*rights, **_dec_kwargs):
    """
    Check if current user as rights to make action

    :param rights: list of rights required for route
    :return: Execute route function
    :raise UnauthorizedError: if user does not have correct rights
    :raises UnauthorizedError: if user does not have correct rights
    """

    def wrapper(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            role = Registry.registered('current-role')
            if not role.user_is_allowed(rights):
                raise UnauthorizedError()
            return f(*args, **kwargs)

        return decorated

    return wrapper


def admin_only(**_dec_kwargs):
    """
    Road is reserved to admin

    :return: Execute route function
    :raise UnauthorizedError: if user does not have correct rights
    :raises UnauthorizedError: if user does not have correct rights
    """

    def wrapper(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            role = Registry.registered('current-role')
            role.link_to_session()
            if role.name != 'admin':
                raise UnauthorizedError()
            return f(*args, **kwargs)

        return decorated

    return wrapper


def add_pagination(default_page=1, default_per_page=25, max_per_page=100, min_per_page=1):
    def wrapper(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            per_page = request.args.get('per_page', default_per_page)
            page = request.args.get('page', default_page)

            if not TextHelper.is_int(per_page):
                per_page = default_per_page

            per_page = int(per_page)
            per_page = max(min(per_page, max_per_page), min_per_page)

            if not TextHelper.is_int(page):
                page = default_page

            page = int(page)
            page = max(1, page) - 1

            res, count = f(page, per_page, *args, **kwargs)

            return jsonify({
                'results':     res,
                'page':        page + 1,
                'per_page':    per_page,
                'total_pages': math.ceil(float(count) / per_page)
            }), HTTPStatus.OK.value

        return decorated

    return wrapper


def mandatory_fields(*fields):
    def wrapper(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            FormsHelper.check_form_is_not_empty(request.form)
            FormsHelper.check_mandatory_fields(request.form, fields)
            return f(*args, **kwargs)

        return decorated

    return wrapper


def mandatory_url_args(*arguments):
    def wrapper(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            for arg in arguments:
                if not request.args.get(arg, False):
                    raise MissingFieldError(field_name=str(arg), details='Missing ' + str(arg))
            return f(*args, **kwargs)

        return decorated

    return wrapper


def request_form_not_empty():
    def wrapper(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            FormsHelper.check_form_is_not_empty(request.form)
            return f(*args, **kwargs)

        return decorated

    return wrapper


def check_email_format(email_field_name='email'):
    def wrapper(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            if email_field_name in request.form and not TextHelper.check_email(request.form[email_field_name]):
                raise InvalidFieldError('Email', 'Email is invalid')
            return f(*args, **kwargs)

        return decorated

    return wrapper


def check_password_format(pass_field_name='password'):  # nosec
    def wrapper(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            if (
                    pass_field_name in request.form  # nosec
                    and not TextHelper.check_password_format(request.form['password'])  # nosec
            ):
                raise InvalidFieldError(
                    'Password',
                    'Password must be 6 characters long, contains one uppercase and one digit.'
                )
            return f(*args, **kwargs)

        return decorated

    return wrapper


def check_phone_format(phone_field_name='phone', region_field_name='country'):
    def wrapper(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            if (
                    phone_field_name in request.form and
                    not TextHelper.check_phone_number(
                        request.form[phone_field_name], request.form.get(region_field_name, 'fr')
                    )
            ):
                raise InvalidFieldError('PhoneNumber', 'Phone number is invalid')
            return f(*args, **kwargs)

        return decorated

    return wrapper


def timeit(f):
    """
    Time function execution (used as decorator or lambda)
    :param f: function to time
    :return: execution result of f
    """

    def timed(*args, **kw):
        ts = time.time()
        result = f(*args, **kw)
        te = time.time()

        logger.log('func:%r took: %2.4f sec' % (f.__name__, te - ts))
        return result

    return timed


def assert_is_number(number_field, number_type='float'):
    def check_field(field, n_type):
        if field in request.form:
            n = request.form.get(field)
            if n_type == 'int':
                if not TextHelper.is_int(n):
                    raise InvalidFieldError(TextHelper.to_camel_case(field), 'Expected integer')
            else:
                if not TextHelper.is_number(n):
                    raise InvalidFieldError(TextHelper.to_camel_case(field), 'Expected number')

    def wrapper(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            if isinstance(number_field, list):
                for fi in number_field:
                    check_field(fi, number_type)
            else:
                check_field(number_field, number_type)
            return f(*args, **kwargs)

        return decorated

    return wrapper
