# coding: utf-8
# third party imports
import sys
from http import HTTPStatus
from traceback import format_exception

import flask
from flask import jsonify, request

# local imports
from your_talent.classes import Slack


def addslashes(s):
    ll = ['\\', ''', ''', '\0', ]
    for i in ll:
        if i in s:
            s = s.replace(i, '\\' + i)
    return s


class BaseError(Exception):
    _channel = '#api_errors'
    status = 0
    message = ''
    subcode = ''
    details = ''
    method = ''
    module = ''
    subcode_key = ''

    def __init__(self, code, message='', subcode=None, details='', **kwargs):
        self.status = code
        self.message = message
        self.subcode = subcode
        self.details = details
        if flask.has_app_context() and flask.has_request_context():
            self.method = request.method if request and request.method else ''
        self.others = kwargs
        if (
                flask.has_app_context() and flask.has_request_context() and
                request and request.blueprint
        ):
            self.module = request.blueprint.split('_')[0].title()
        if subcode:
            self.subcode = subcode
        else:
            self.subcode = self.method + self.module + self.subcode_key

    def to_json(self):
        d = {
            'status':  self.status,
            'message': self.message,
            'subcode': self.subcode,
            'details': self.details
        }
        if self.others:
            d.update(self.others)
        return jsonify(d)

    def to_slack(self):
        try:
            exc_type, exc_obj, tb = sys.exc_info()
            stack = format_exception(exc_type, exc_obj, tb, 100)
            info_stack = '-'
            if stack:
                if len(stack) >= 2:
                    info_stack = stack[-2].replace('\n', ' ')
                elif len(stack) == 1:
                    info_stack = (''.join(stack)).replace('\n', ' ')
        except Exception:
            info_stack = 'Unable to retrieve Stack'

        fields = [
            {
                'title': 'Code',
                'value': str(self.status),
                'short': True
            },
            {
                'title': 'SubCode',
                'value': self.subcode,
                'short': True
            },
            {
                'title': 'Method',
                'value': self.method,
                'short': True
            },
            {
                'title': 'Caller',
                'value': request.remote_addr,
                'short': True
            }
        ]
        if self.others:
            for k in self.others.keys():
                fields.append({
                    'title': k,
                    'value': str(self.others[k]),
                    'short': True
                })

        Slack.attachment(
            self._channel,
            fallback='API Error',
            color='#e74c3c',
            title=request.url,
            text=self.details,
            footer=info_stack,
            fields=fields
        )


class DatetimeError(BaseError):
    def __init__(self, error, **kwargs):
        self.subcode_key = 'InvalidDate'
        details = str(error)
        super(DatetimeError, self).__init__(HTTPStatus.BAD_REQUEST.value, 'Invalid Date', details=details, **kwargs)


class DatabaseConnectionError(BaseError):
    _channel = '#api_db_log'

    def __init__(self, **kwargs):
        super(DatabaseConnectionError, self).__init__(
            HTTPStatus.SERVICE_UNAVAILABLE.value, 'DataBaseConnectionFailure', details='No connection to database',
            **kwargs
        )


class ApiError(BaseError):
    subcode_key = ''

    def __init__(self, code, message='', details='', **kwargs):
        super(ApiError, self).__init__(code, message, '', details, **kwargs)
        self.subcode = self.method + self.module + self.subcode_key


class AuthError(BaseError):
    def __init__(self, details='Authentication failed', subcode='AuthUnauthorized', **kwargs):
        super(AuthError, self).__init__(
            HTTPStatus.UNAUTHORIZED.value, message='Unauthorized', subcode=subcode, details=details,
            **kwargs
        )


class UnauthorizedError(BaseError):
    def __init__(self, details='Your are not allowed to make this action', **kwargs):
        super(UnauthorizedError, self).__init__(
            HTTPStatus.UNAUTHORIZED.value, 'Unauthorized', 'AuthUnauthorized', details, **kwargs
        )


class ServerError(BaseError):
    def __init__(self, details='', key='InternalServerError', **kwargs):
        self._channel = '#api_500'
        super(ServerError, self).__init__(
            HTTPStatus.INTERNAL_SERVER_ERROR.value, 'Internal server error', 'InternalServerError', details, **kwargs
        )


class ForbiddenError(ApiError):
    def __init__(self, details='Access denied', **kwargs):
        self.subcode_key = 'Forbidden'
        super(ForbiddenError, self).__init__(HTTPStatus.FORBIDDEN.value, 'Forbidden', details, **kwargs)


class EmptyRequestError(ApiError):
    def __init__(self, details='The request form is empty', **kwargs):
        self.subcode_key = 'RequestEmpty'
        super(EmptyRequestError, self).__init__(HTTPStatus.BAD_REQUEST.value, 'Bad request', details, **kwargs)


class MissingFieldError(ApiError):
    def __init__(self, field_name, details='Field is missing', **kwargs):
        self.subcode_key = 'Missing' + field_name
        super(MissingFieldError, self).__init__(HTTPStatus.BAD_REQUEST.value, 'Bad request', details, **kwargs)


class EmptyFieldError(ApiError):
    def __init__(self, field_name, details='Field is empty', **kwargs):
        self.subcode_key = 'Missing' + field_name
        super(EmptyFieldError, self).__init__(HTTPStatus.BAD_REQUEST.value, 'Bad request', details, **kwargs)


class InvalidFieldError(ApiError):
    def __init__(self, field_name, details='Field is invalid', **kwargs):
        self.subcode_key = 'Invalid' + field_name
        super(InvalidFieldError, self).__init__(HTTPStatus.BAD_REQUEST.value, 'Bad request', details, **kwargs)


class ResourceNotFoundError(ApiError):
    def __init__(self, details='Resource not found', resource='', **kwargs):
        self.subcode_key = resource + 'ResourceNotFound'
        super(ResourceNotFoundError, self).__init__(HTTPStatus.NOT_FOUND.value, 'Not found', details, **kwargs)


class DuplicateError(ApiError):
    def __init__(self, field, resource='', details='Duplicated resource', **kwargs):
        self.subcode_key = resource + 'DuplicateResource'
        super(DuplicateError, self).__init__(HTTPStatus.UNPROCESSABLE_ENTITY.value, field, details, **kwargs)


class IDNotFoundError(ApiError):
    def __init__(self, details='Resource not found', resource='', **kwargs):
        self.subcode_key = resource + 'IDNotFound'
        super(IDNotFoundError, self).__init__(HTTPStatus.NOT_FOUND.value, 'Not found', details, **kwargs)


class InsertError(ApiError):
    def __init__(self, details='Insertion failed', model='', **kwargs):
        self._channel = '#api_db_log'
        self.subcode_key = model + 'InsertionFailed'
        super(InsertError, self).__init__(HTTPStatus.BAD_REQUEST.value, 'Bad request', details, **kwargs)

    def to_json(self):
        return jsonify({
            'status':  self.status,
            'message': self.message,
            'subcode': self.subcode,
            'details': self.details
        })


class UpdateError(ApiError):
    def __init__(self, details='Update failed', model='', **kwargs):
        self._channel = '#api_db_log'
        self.subcode_key = model + 'UpdateFailed'
        super(UpdateError, self).__init__(HTTPStatus.BAD_REQUEST.value, 'Bad request', details, **kwargs)


class DeleteError(ApiError):
    def __init__(self, details='Delete failed', model='', **kwargs):
        self._channel = '#api_db_log'
        self.subcode_key = model + 'DeleteFailed'
        super(DeleteError, self).__init__(HTTPStatus.BAD_REQUEST.value, 'Bad request', details, **kwargs)


class LoginFailed(ApiError):
    def __init__(self, details='Login failed', **kwargs):
        self.subcode_key = 'LoginFailed'
        super(LoginFailed, self).__init__(HTTPStatus.BAD_REQUEST.value, 'Bad request', details, **kwargs)


class CustomError(ApiError):
    def __init__(self, details='Error', subcode='Error', code=400, message='Bad request', **kwargs):
        self.subcode_key = subcode
        super(CustomError, self).__init__(code, message, details, **kwargs)
