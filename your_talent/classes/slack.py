# coding: utf-8
# local imports
from your_talent.classes import Registry


class Slack(object):
    _active = False
    _host = None
    _headers = {
        'Content-type': 'application/x-www-form-urlencoded',
        'Accept':       'text/plain'
    }
    _channels = {
        '#api_500':     'BMWK4RAKE/EVLJggdieMk4xVVbathwCsTJ',
        '#api_db_log':  'BMWK4THQU/ILz2jULmHz3pqs8mXaAWmo9l',
        '#api_dev':     'BMKKP4XAP/3klbWspVxE768J61V0bthb8O',
        '#api_errors':  'BKL5D6T2L/7OTKjxEhFAlVLCukFyHCSdjk',
        '#api_verbose': 'BMX3F3CFJ/UlYNlJFymT3RurLNWJ1BlMSR',
        '#awards':      'BKL5F9UAZ/0PzNQjvRl8iLZp8S9GCbW565',
        '#new_videos':  'BKKLSJZCL/GkPqhXQ9HR9a7eL1SvDvdrvv',
    }

    _subteams = {
        # '@tech': '<!subteam^SJL6CDHEC|tech>',
    }

    @classmethod
    def init(cls, config=None):
        if not config:
            config = Registry.registered('config')
        if config:
            cls._active = config.getboolean('slack', 'active')
            cls._host = config.get('slack', 'host')

    @classmethod
    def __send(cls, channel, message=None, attachments=None):
        from your_talent.tasks.notification import send_slack
        if cls._active and cls._host:
            send_slack.delay(cls._host, channel, message, attachments, cls._headers)
            return True
        return False

    @classmethod
    def get_channels(cls):
        return cls._channels.keys()

    @classmethod
    def has_channel(cls, channel):
        return channel in cls._channels.keys()

    @classmethod
    def send(cls, channel, message):
        config = Registry.registered('config')
        server_name = config.get('server', 'name') if config.get('server', 'name') else 'undefined'

        if server_name != 'prod':
            message = '[' + server_name.upper() + ' > ' + str(channel) + ']' + message
            channel = '#api_dev'

        if channel in Slack._channels.keys():
            cls.__send(Slack._channels[channel], message)

    @classmethod
    def attachment(cls, channel, **kwargs):
        if channel in Slack._channels.keys():
            config = Registry.registered('config')
            server_name = config.get('server', 'name') if config.get('server', 'name') else 'undefined'

            if server_name != 'your-talent-prod':
                if 'title' in kwargs.keys():
                    kwargs['title'] = '[' + server_name.upper() + ' > ' + str(channel) + ']' + kwargs['title']
                else:
                    kwargs['title'] = '[' + server_name.upper() + ' > ' + str(channel) + ']'

                channel = '#api_dev'

            if 'fallback' not in kwargs.keys():
                kwargs['fallback'] = kwargs['title']

            cls.__send(Slack._channels[channel], attachments=[kwargs])

    @staticmethod
    def get_team(key):
        return Slack._subteams[key]
