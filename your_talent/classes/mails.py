# coding: utf-8
# standard imports
from os.path import basename

# third party imports
from flask import render_template
from validate_email import validate_email
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate, formataddr
from email.header import Header
from your_talent.helpers import IO


class Mail(object):
    def __init__(self, from_name=None, from_addr=None, to_addr=None, title='', subject="", content=""):
        from your_talent.classes import Registry
        self._active = False
        self._from = None
        self._from_name = from_name
        self._reply_to = None
        self._reply_to_name = None
        self._to = []
        self._subject = ""
        self._content = ""
        self._smtp_host = ""
        self._smtp_user = ""
        self._smtp_pass = ""
        self._smtp_port = ""
        self._debug_lvl = 0
        self._files = []
        self._title = title

        conf = Registry.registered('config')
        self._address = ''
        self._yt_mail = 'contact@your-talent.fr'
        try:
            self._address = conf.get('infos', 'address')
        except Exception as _:
            pass
        try:
            self._yt_mail = conf.get('infos', 'mail')
        except Exception as _:
            pass

        self.set_from(from_addr)
        self.add_to(to_addr)

        self._subject = subject
        self._content = content

        config = Registry.registered("config")

        if config:
            self._active = config.getboolean("mail", "active")
            self._smtp_host = config.get("mail", "smtp_host")
            self._smtp_port = config.get("mail", "smtp_port")
            self._smtp_user = config.get("mail", "smtp_user")
            self._smtp_pass = config.get("mail", "smtp_pass")

            if not self._from:
                self._from = config.get("mail", "default_from")
            if not self._from_name:
                self._from_name = config.get("mail", "default_from_name")

            default_reply_to_address = None
            try:
                default_reply_to_address = config.get("mail", "default_reply_to")
            except Exception:
                default_reply_to_address = None
            finally:
                if default_reply_to_address and default_reply_to_address != self._from:
                    self._reply_to = config.get("mail", "default_reply_to")
                    self._reply_to_name = config.get("mail", "default_reply_to_name")

    def add_to(self, email):
        if isinstance(email, str):
            return self._add_to(email)
        elif isinstance(email, list):
            for e in email:
                self._add_to(e)
            return True
        return False

    def _add_to(self, email):
        from your_talent.classes import Registry
        try:
            config = Registry.registered("config")
            testing_email = config.get("mail", "default_to")
            email = testing_email
        except Exception:
            email = email

        if email and validate_email(email) and email not in self._to:
            self._to.append(email)
            return True
        return False

    def set_from(self, email):
        if email and validate_email(email):
            self._from = email
            return True
        return False

    def set_reply_to(self, email):
        if email and validate_email(email):
            self._reply_to = email
            return True
        return False

    def set_subject(self, subject):
        self._subject = subject

    def set_content(self, content):
        self._content = content

    def use_template(self, template_file, **kargs):
        self._content = render_template(
            template_file, title=self._title, address=self._address, yt_mail=self._yt_mail, **kargs
        )

    def set_debug(self, lvl):
        self._debug_lvl = lvl

    def add_file(self, file_path):
        if IO.file_exists(file_path):
            self._files.append(file_path)
            return True
        return False

    def send(self, subtype="html"):
        from your_talent.tasks.notification import send_mail
        if not self._active:
            return True
        else:
            if self._to:
                mail = MIMEMultipart()

                mail['To'] = COMMASPACE.join(self._to)
                author = self._from
                if self._from_name:
                    author = formataddr((str(Header(self._from_name, 'utf-8')), self._from))
                mail['From'] = author

                if self._reply_to_name:
                    mail['Reply-To'] = formataddr((str(Header(self._reply_to_name, 'utf-8')), self._reply_to))

                mail['Subject'] = self._subject
                mail['Date'] = formatdate(localtime=True)

                mail.attach(MIMEText(self._content, subtype))

                for f in self._files or []:
                    with open(f, "rb") as fil:
                        part = MIMEApplication(fil.read(), Name=basename(f))
                        fil.close()
                    part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
                    mail.attach(part)
                for to_addr in self._to:
                    send_mail.delay(
                        author,
                        to_addr,
                        mail.as_string()
                    )
                return True
        return False
