import http.client
import json
import smtplib
import urllib.parse

from your_talent.api import YourTalent
from your_talent.classes import Registry
from your_talent.classes.logger import Logger
from your_talent.helpers import CeleryErrorManager


config = Registry.registered("config")
logger = Logger(__name__)


@YourTalent.celery.task(name='notifications.mail', on_failure=CeleryErrorManager.error_handler)
def send_mail(from_email, to_emails, content):
    if config:
        smtp_host = config.get("mail", "smtp_host")
        smtp_port = config.get("mail", "smtp_port")
        smtp_user = config.get("mail", "smtp_user")
        smtp_pass = config.get("mail", "smtp_pass")
    else:
        return False

    server = smtplib.SMTP(
        host=smtp_host,
        port=smtp_port,
        timeout=10
    )

    try:
        server.set_debuglevel(0)
        server.starttls()
        server.ehlo()
        server.login(smtp_user, smtp_pass)
        server.sendmail(from_email, to_emails, content)
    except smtplib.SMTPException as e:
        logger.error(e)
        return False
    finally:
        server.quit()

    return True


@YourTalent.celery.task(name='notifications.slack', on_failure=CeleryErrorManager.error_handler)
def send_slack(host, channel, message=None, attachments=None, headers=None):
    headers = headers if headers else {}
    data = dict()
    if message:
        data["text"] = message
    elif attachments:
        data["attachments"] = attachments
    data = urllib.parse.urlencode({"payload": json.dumps(data)})
    h = http.client.HTTPSConnection(host)
    h.request('POST', '/services/{}/{}'.format('T7G0EP333', channel), data, headers)
    r = h.getresponse()
    ack = r.read()
    return str(ack)
