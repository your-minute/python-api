from your_talent import YourTalent

from your_talent.helpers import CeleryErrorManager
from your_talent.models import OAuth2Token


@YourTalent.celery.task(name='oauth.delete_previous_implicit_tokens', on_failure=CeleryErrorManager.error_handler)
def delete_previous_implicit_tokens(token_id):
    with YourTalent.app.app_context():
        # load used token
        ref_token = OAuth2Token.query.get(token_id)
        if ref_token and not ref_token.refresh_token:
            # select all previous token for the same client
            tokens = OAuth2Token.query.filter_by(
                client_id=ref_token.client_id,
                user_id=ref_token.user_id,
                scope=ref_token.scope
            ).filter(
                OAuth2Token.refresh_token.is_(None),
                OAuth2Token.id != ref_token.id
            )

            for t in tokens.all():
                t.delete()

            return True
