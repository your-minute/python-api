import json

from google.auth.transport import requests
from google.oauth2 import id_token

from your_talent.classes import Logger, Registry
from your_talent.errors import AuthError


class FacebookLogin(object):
    def __init__(self):
        self.logger = Logger(__name__)
        config = Registry.registered('config')
        self.app_id = config.get('login_provider', 'fb_id')
        self.app_secret = config.get('login_provider', 'fb_access')
        self.url = 'https://graph.facebook.com/debug_token?input_token={input_token}&access_token={token}'

    @property
    def admin_token(self):
        from your_talent.helpers.http_request import HttpRequest
        url = 'https://graph.facebook.com/oauth/access_token?' \
              'client_id={}&client_secret={}& grant_type=client_credentials'.format(self.app_id, self.app_secret)
        res = HttpRequest.get(url, result_format='application/json')

        try:
            res = json.loads(res)

        except Exception:
            return None

        if not res or not res.get('access_token'):
            return None

        return res.get('access_token')

    @staticmethod
    def check_token(token):
        from your_talent.helpers.http_request import HttpRequest
        login = FacebookLogin()
        res = HttpRequest.get(
            login.url.format(input_token=token, token=login.admin_token),
            result_format='application/json'
        )
        try:
            res = json.loads(res)
        except Exception:
            raise AuthError

        if not res or not res.get('data'):
            raise AuthError()

        if not res['data'].get('is_valid') or not res['data'].get('app_id') == login.app_id:
            raise AuthError()

        return token, res['data'].get('user_id'), None


class GoogleLogin(object):
    def __init__(self):
        self.logger = Logger(__name__)
        config = Registry.registered('config')
        self.app_id = config.get('login_provider', 'google_id')

    @staticmethod
    def check_token(token):
        login = GoogleLogin()
        try:
            infos = id_token.verify_oauth2_token(token, requests.Request(), login.app_id)
            # {
            #     'sub':        '108847387422396015721', 'email': 'mail', 'email_verified': True,
            #     'at_hash':    'Uzy-bqqkESDYxJzqJXxU_A', 'name': 'Prénom Nom',
            #     'picture':    'url to avatar',
            #     'given_name': 'Prénnom', 'family_name': 'NOM', 'locale': 'fr', 'iat': 1566575018,
            #     'exp':        1566578618, 'jti': 'f632f849788f84cd6228fd5893ba9f5349ac6aff'
            # }

            user_id = infos['sub']
        except ValueError:
            raise AuthError()

        return token, user_id, {
            'email':               infos.get('email'),
            'name':                '{} {}.'.format(infos.get('given_name', ' '), infos.get('family_name', ' ')[0]),
            'avatar_url':          infos.get('picture'),
            'login_provider_user': user_id
        }
