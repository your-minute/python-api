import logging

import boto3
from werkzeug.utils import secure_filename

from your_talent.classes import Registry
from your_talent.errors import InsertError, ServerError


class S3Helper(object):
    _config = None
    _cli = None
    _resource = None
    bucket = None
    video_bucket = None
    public_bucket = None

    @staticmethod
    def init_config():
        S3Helper._config = Registry.registered('config')

        S3Helper._cli = boto3.client(
            's3',
            region_name=S3Helper._config.get('aws', 'region'),
            aws_access_key_id=S3Helper._config.get('aws', 'key'),
            aws_secret_access_key=S3Helper._config.get('aws', 'secret')
        )
        S3Helper._resource = boto3.session.Session(
            region_name=S3Helper._config.get('aws', 'region'),
            aws_access_key_id=S3Helper._config.get('aws', 'key'),
            aws_secret_access_key=S3Helper._config.get('aws', 'secret')
        ).resource('s3')
        S3Helper.bucket = S3Helper._config.get('aws', 'default_bucket')
        S3Helper.video_bucket = S3Helper._config.get('aws', 'video_bucket')
        S3Helper.public_bucket = S3Helper._config.get('aws', 'public_bucket')

    def _upload(self, upload_file, bucket_name, key_path='', acl='authenticated-read'):
        try:
            key_path = self._config.get('aws', 'base_path') + key_path
            upload_file.filename = secure_filename(upload_file.filename)

            self._cli.upload_fileobj(
                upload_file, bucket_name, key_path + upload_file.filename,
                ExtraArgs={
                    'ACL':         acl,
                    'ContentType': upload_file.content_type
                }
            )

            return (
                '{}.s3.amazonaws.com/{}'.format(
                    format(bucket_name),
                    key_path + upload_file.filename
                ),
                bucket_name,
                key_path + upload_file.filename
            )

        except Exception as e:
            logging.error(str(e))
            raise InsertError(
                details='Could not upload file {} to s3 bucket {}'.format(upload_file.filename, bucket_name),
                model='S3'
            )

    def _delete(self, file_key, bucket):
        try:
            self._cli.delete_object(Bucket=bucket, Key=file_key)
        except Exception as e:
            logging.error(str(e))
            raise InsertError(
                details='Could not delete file {} from s3 bucket {}'.format(file_key, bucket),
                model='S3'
            )

    def upload_image(self, image, key_path='customers', acl='public-read'):
        """
        Upload image to correct s3 bucket
        :param image: image data to upload
        :param key_path: sub location in s3 (bucket/KEY_PATH/name)(default: customers)
        :type key_path: str
        :param acl: access right to the image (default: public-read)
        :type acl: str
        :return: path to uploaded image in s3
        :rtype: str
        """
        key_path = 'images/' + key_path + '/'
        return self._upload(image, self.bucket, key_path, acl)

    def delete_image(self, image):
        image = image.replace(
            'http://{}.s3.amazonaws.com/'.format(self.bucket),
            ''
        )  # Ensure we are passing only the file key to S3
        return self._delete(image, self.bucket)

    def download(self, bucket, key):
        try:
            down_file = self._cli.get_object(Bucket=bucket, Key=key)
            return down_file['Body']
        except Exception as e:
            raise ServerError(details=str(e))

    def get_presigned(self, file_path, url_type='video'):
        try:
            paths = file_path.split('/')
            bucket = self.video_bucket if url_type == 'video' else self.public_bucket
            file_path = ''.join(secure_filename(s) + '/' for s in paths)
            file_path = file_path[:-1]
            file_path = self._config.get('aws', 'base_path') + file_path
            conditions = [{'acl': 'public-read'}] if url_type == 'avatar' else []
            resp = self._cli.generate_presigned_post(
                bucket, file_path, ExpiresIn=1800, Conditions=conditions
            )
            return (
                resp,
                '{}.s3.amazonaws.com/{}'.format(
                    bucket,
                    file_path
                ),
                self.video_bucket,
                file_path
            )

        except Exception as e:
            logging.error((str(e)))
        raise ServerError(
            'Could not create pre-signed url', 'PresignedURLGenerationFailed'
        )

    def get_presigned_download(self, bucket, key):
        try:
            return self._cli.generate_presigned_url(
                'get_object', Params={'Bucket': bucket, 'Key': key}, ExpiresIn=1800
            )
        except Exception as e:
            raise ServerError(
                'Could not create pre-signed url', 'PresignedURLGenerationFailed'
            )

    def change_acl(self, key, bucket=video_bucket, acl='authenticated-read'):
        try:
            self._resource.Object(bucket, key).Acl().put(ACL=acl)
            return True
        except Exception as e:
            logging.error(e)
            return False
