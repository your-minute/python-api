import base64
import hashlib
from Crypto import Random
from Crypto.Cipher import AES

from your_talent.classes import Registry


class CryptHelper(object):
    _BS = 32
    _KEY = None

    @staticmethod
    def init():
        config = Registry.registered("config")
        CryptHelper._KEY = hashlib.sha256(config.get("AES", "key").encode()).digest()

    @staticmethod
    def encrypt(raw):
        if not CryptHelper._KEY:
            CryptHelper.init()
        raw = CryptHelper._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(CryptHelper._KEY, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    @staticmethod
    def decrypt(enc):
        if not CryptHelper._KEY:
            CryptHelper.init()
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(CryptHelper._KEY, AES.MODE_CBC, iv)
        return CryptHelper._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')

    @staticmethod
    def _pad(s):
        s = str(s)
        return s + (CryptHelper._BS - len(s) % CryptHelper._BS) * chr(CryptHelper._BS - len(s) % CryptHelper._BS)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s) - 1:])]
