import base64
import datetime
import hashlib
import secrets
from decimal import Decimal


class PasswordHelper(object):
    algorithm = "pbkdf2_sha512"
    iterations = 180000
    digest = hashlib.sha512
    config = None
    salt = None

    @staticmethod
    def init_config():
        from your_talent.classes import Registry
        PasswordHelper.config = Registry.registered('config')
        PasswordHelper.salt = PasswordHelper.config.get('security', 'api_token_salt')

    def encode(self, password, iterations=None):
        iterations = iterations or self.iterations
        hash = pbkdf2(password, self.salt, iterations, digest=self.digest)
        hash = base64.b64encode(hash).decode('ascii').strip()
        return "%s$%d$%s" % (self.algorithm, iterations, hash)

    def verify(self, password, encoded):
        algorithm, iterations, hash = encoded.split('$', 3)
        encoded_2 = self.encode(password, int(iterations))
        return constant_time_compare(encoded, encoded_2)

    def harden_runtime(self, password, encoded):
        algorithm, iterations, salt, hash = encoded.split('$', 3)
        extra_iterations = self.iterations - int(iterations)
        if extra_iterations > 0:
            self.encode(password, extra_iterations)


def pbkdf2(password, salt, iterations, dklen=0, digest=None):
    """Return the hash of password using pbkdf2."""
    if digest is None:
        digest = hashlib.sha256
    dklen = dklen or None
    password = force_bytes(password)
    salt = force_bytes(salt)
    return hashlib.pbkdf2_hmac(digest().name, password, salt, iterations, dklen)


def force_bytes(s, encoding='utf-8', strings_only=False, errors='strict'):
    """
    Similar to smart_bytes, except that lazy instances are resolved to
    strings, rather than kept as lazy objects.
    If strings_only is True, don't convert (some) non-string-like objects.
    """
    # Handle the common case first for performance reasons.
    if isinstance(s, bytes):
        if encoding == 'utf-8':
            return s
        else:
            return s.decode('utf-8', errors).encode(encoding, errors)
    if strings_only and is_protected_type(s):
        return s
    if isinstance(s, memoryview):
        return bytes(s)
    return str(s).encode(encoding, errors)


def is_protected_type(obj):
    """Determine if the object instance is of a protected type.
    Objects of protected types are preserved as-is when passed to
    force_str(strings_only=True).
    """
    return isinstance(obj, (
        type(None), int, float, Decimal, datetime.datetime, datetime.date, datetime.time,
    ))


def constant_time_compare(val1, val2):
    """Return True if the two strings are equal, False otherwise."""
    return secrets.compare_digest(force_bytes(val1), force_bytes(val2))
