# coding: utf-8
import requests

from your_talent.classes import Logger


logger = Logger(__name__)


class HttpRequest(object):
    @staticmethod
    def format_result(result, r_format="text"):
        try:
            if r_format == "json":
                return result.json()
            elif r_format == "text":
                return result.text
            else:
                return result.content
        except Exception as e:
            logger.error(str(e))
            raise e

    @staticmethod
    def get(url, result_format="text", headers=None, **kargs):
        r = requests.get(url, headers=headers, **kargs)
        return HttpRequest.format_result(r, result_format)

    @staticmethod
    def post(url, data, result_format="text", headers=None, **kargs):
        r = requests.post(url, data, headers=headers, **kargs)
        return HttpRequest.format_result(r, result_format)
