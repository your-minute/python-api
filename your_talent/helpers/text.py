# coding: utf-8
# standard imports
import hashlib
import uuid
import re
import unicodedata
import os
import binascii

# third party imports
import phonenumbers
from validate_email import validate_email
from your_talent.classes import Registry


class TextHelper(object):
    alpha_num = re.compile('^[A-z1-9_-]*$')
    config = None

    @staticmethod
    def init_config():
        TextHelper.config = Registry.registered("config")

    @staticmethod
    def generate_token(txt=None, api_token=False):
        """
        md5 encrypt value
        :param txt: text to hash
        :param api_token: tokenize for API
        :return:
        """
        if txt is None:
            txt = str(uuid.uuid4())

        if api_token:
            txt = str(TextHelper.config.get("security", "api_token_salt")) + txt

        return hashlib.md5(txt.encode('utf-8')).hexdigest()

    @staticmethod
    def is_alphanum(val, is_lower=False):
        """
        Check if provided is alphanumeric
        :param val: Value to check
        :type val: str
        :param is_lower: check if val is only lower
        :type is_lower: bool
        :return: val is alphanumeric
        :rtype: bool
        """
        if is_lower and not val.lower() == val:
            return False
        return TextHelper.alpha_num.match(val)

    @staticmethod
    def country_name_to_region(region):
        supported_country = {
            'fr':         'FR',
            'france':     'FR',
            'be':         'BE',
            'belgium':    'BE',
            'belgique':   'BE',
            'lu':         'LU',
            'lux':        'LU',
            'luxembourg': 'LU'
        }
        if not region:
            return 'FR'
        return supported_country.get(region.lower(), region.upper())

    @staticmethod
    def generate_secret():
        return binascii.hexlify(os.urandom(32))

    @staticmethod
    def clean_and_convert_separated_str(s, new_separator, old_separator=','):
        return new_separator.join([e.replace(' ', '') for e in s.split(old_separator)])

    @staticmethod
    def check_phone_number(phone_number, region='FR'):
        try:
            region = TextHelper.country_name_to_region(region)
            z = phonenumbers.parse(phone_number, region)
        except phonenumbers.NumberParseException:
            return False
        return phonenumbers.is_possible_number(z)

    @staticmethod
    def check_email(email):
        return validate_email(email)

    @staticmethod
    def check_password_format(password):
        _pass_regex = re.compile('(?=.{6})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*')
        if _pass_regex.match(password):
            return True
        return False

    # method to check if string represents an int
    @staticmethod
    def is_int(s):
        try:
            int(s)
            return True
        except ValueError:
            return False

    @staticmethod
    def is_number(s):
        if isinstance(s, (str, int, float)):
            try:
                float(s)
                return True
            except ValueError:
                pass

            try:
                import unicodedata
                unicodedata.numeric(s)
                return True

            except (TypeError, ValueError):
                pass

        return False

    @staticmethod
    def to_bool(s):
        if not s:
            return False
        if isinstance(s, bool):
            return s
        if TextHelper.is_int(s):
            return int(s) == 1
        if not isinstance(s, str):
            return False
        return s.lower() in ('t', 'true', 'ok')

    @staticmethod
    def str_to_url(s):
        # trim whitespaces
        s = s.strip()
        # lowercase
        s = s.lower()
        # remove accents
        s = ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
        # remove all except dash and alphanum
        s = re.sub('[^A-Za-z0-9]+', '-', s)

        if s[-1:] == '-':
            s = s[:-1]
        return s

    @staticmethod
    def to_camel_case(snake_str):
        return ''.join(x.capitalize() or '_' for x in snake_str.split('_'))

    @staticmethod
    def url_from_config(url_type, sub_path="", *args):
        """
        Get config url for type (bo, dash, front, flower)
        :param url_type: kind of url to get (bo/dash/front/flower)
        :param sub_path: rest of url to append to base_path from config
        :param args: args of path
        :type url_type: str|basestring
        :return: url related in config
        :rtype: str|basestring
        """
        config = Registry.registered('config')
        type_to_link = {
            'bo':          'back',
            'back-office': 'back',
            'back':        'back',
            'site':        'front',
            'front':       'front',
            'flower':      'flower',
        }

        key = type_to_link[url_type]
        base_path = config.get('base-urls', key)

        if base_path[-1] != "/":
            base_path += "/"
        if sub_path[0] == "/":
            sub_path = sub_path[1:]

        return base_path + sub_path.format(*args)

    @staticmethod
    def get_user_name_from_registry():
        role = Registry.registered('current-role')

        if role == 'public':
            return 'public user'

        usr = Registry.registered('current-user-name')
        if usr is None:
            return 'system'

        return usr
