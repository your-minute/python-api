from datetime import date, datetime, timedelta

import arrow
import pytz

from your_talent.errors import DatetimeError
from your_talent.helpers import TextHelper


class DateHelper(object):
    DAY_LIST = {
        '0': 'Lundi',
        '1': 'Mardi',
        '2': 'Mercredi',
        '3': 'Jeudi',
        '4': 'Vendredi',
        '5': 'Samedi',
        '6': 'Dimanche'
    }

    MOUNTH_LIST = {
        '01': 'Janvier',
        '02': 'Février',
        '03': 'Mars',
        '04': 'Avril',
        '05': 'Mai',
        '06': 'Juin',
        '07': 'Juillet',
        '08': 'Août',
        '09': 'Septembre',
        '10': 'Octobre',
        '11': 'Novembre',
        '12': 'Décembre'
    }

    @staticmethod
    def get_datetime(datetime_date=None, time_format='%Y-%m-%d %H:%M:%S', old_format='%Y-%m-%d %H:%M:%S'):
        datetime_obj = DateHelper.get_datetime_obj(datetime_date, old_format)
        return datetime_obj.strftime(time_format)

    @staticmethod
    def get_datetime_obj(datetime_date=None, time_format='%Y-%m-%d %H:%M:%S', tz=pytz.utc, default=datetime.now):
        """
        Get datetime object from datetime representative

        :param datetime_date: datetime representative
        :type datetime_date: datetime|str|basestring|int|float
        :param time_format: expected format if string or base string is provided
        :type time_format: str
        :param tz: time zone to use when parsing timestamps (default: utc)
        :param default: lambda function to return a default value
        :type default: func
        :return: datetime object from representative
        :rtype: datetime.datetime
        :raise DatetimeError: if provided string does not match format
        :raises DatetimeError: if provided string does not match format
        """
        try:
            if isinstance(datetime_date, datetime):
                return datetime_date

            if datetime_date is None:
                datetime_date = default()
            elif TextHelper.is_number(datetime_date):
                datetime_date = datetime.fromtimestamp(int(datetime_date / 1e3), tz=tz)
            elif isinstance(datetime_date, str):
                datetime_date = datetime.strptime(datetime_date, time_format)

            return datetime_date

        except ValueError as e:
            raise DatetimeError(str(e))

    @staticmethod
    def datetime_to_timezone(
            datetime_date=None, time_format='%Y-%m-%d %H:%M:%S',
            base_tz=pytz.utc, to_tz='Europe/Paris'
    ):
        dt = DateHelper.get_datetime_obj(datetime_date, time_format, base_tz)
        str_date = DateHelper.date_to_string(dt, time_format, time_format, to_tz)
        return DateHelper.get_datetime_obj(str_date, time_format)

    @staticmethod
    def check_datetime(datetime_date, time_format='%Y-%m-%d %H:%M:%S'):
        try:
            datetime.strptime(str(datetime_date), time_format)
            return True
        except ValueError:
            return False

    @staticmethod
    def compare_datetime(first_datetime, second_datetime, date_format='%Y-%m-%d %H:%M:%S'):
        try:
            time1 = datetime.strptime(str(first_datetime), date_format)
            time2 = datetime.strptime(str(second_datetime), date_format)
        except ValueError:
            return None
        return time2 > time1

    @staticmethod
    def tz_date_string_to_utc(datestring, time_format='%Y-%m-%d %H:%M:%S', tz='Europe/Paris'):
        lo = pytz.timezone(tz)
        naive = DateHelper.get_datetime_obj(datestring, time_format)
        local_dt = lo.localize(naive, is_dst=None)
        return local_dt.astimezone(pytz.utc)

    # function to create french sentence detailing order start & end
    @staticmethod
    def mail_date_format(order, tz='Europe/Paris'):
        end = None
        # get start date
        if order.start_datetime and order.duration:
            start = DateHelper.date_to_string(order.start_datetime, tz=tz)
        else:
            start = DateHelper.date_to_string(order.available_date_min, tz=tz)
            end = DateHelper.date_to_string(order.available_date_max, tz=tz)

        # if either of date is not a datetime, return empty string
        if not isinstance(start, datetime) or (end and not isinstance(end, datetime)):
            try:
                start = DateHelper.get_datetime_obj(start, '%Y-%m-%d %H:%M:%S')
                end = DateHelper.get_datetime_obj(end, '%Y-%m-%d %H:%M:%S', default=lambda: None)
            except DatetimeError:
                return ''

        # decompose start and end date
        start_weekday = start.weekday()
        start_day_num = DateHelper.get_datetime(start, '%d')
        start_month_index = DateHelper.get_datetime(start, '%m')
        start_hour_formatted = DateHelper.get_datetime(start, '%Hh%M')
        end_hour_formatted = DateHelper.get_datetime(end, '%Hh%M')

        # if start datetime
        if start.date() and not end:
            final_format = '{} {} {} à {}'.format(
                DateHelper.DAY_LIST[str(start_weekday)],
                start_day_num,
                DateHelper.MOUNTH_LIST[str(start_month_index)],
                start_hour_formatted
            )

        # if date_min and date_max are the same day
        elif start.date() == end.date():
            final_format = '{} {} {} entre {} et {}'.format(
                DateHelper.DAY_LIST[str(start_weekday)],
                start_day_num,
                DateHelper.MOUNTH_LIST[str(start_month_index)],
                start_hour_formatted,
                end_hour_formatted
            )
        else:
            end_weekday = end.weekday()
            end_day_num = DateHelper.get_datetime(end, '%d')
            end_month_index = DateHelper.get_datetime(end, '%m')

            final_format = 'Entre le {} {} {} à {} et le {} {} {} à {}'.format(
                DateHelper.DAY_LIST[str(start_weekday)],
                start_day_num,
                DateHelper.MOUNTH_LIST[str(start_month_index)],
                start_hour_formatted,
                DateHelper.DAY_LIST[str(end_weekday)],
                end_day_num,
                DateHelper.MOUNTH_LIST[str(end_month_index)],
                end_hour_formatted
            )

        return final_format

    @staticmethod
    def date_to_string(d, input_format='%Y-%m-%d %H:%M:%S', output_format='%Y-%m-%d %H:%M:%S', tz=None):
        if d is None:
            return ''
        date = d
        if not isinstance(d, datetime):
            date = datetime.strptime(d, input_format)
        if tz is not None and tz in pytz.all_timezones:
            date = arrow.get(date).to(tz).datetime
        return datetime.strftime(date, output_format)

    @staticmethod
    def get_start_of_week_form_iso_week(year, week):
        d = date(year, 1, 1)
        delta_days = d.isoweekday() - 1
        delta_weeks = week

        if year == d.isocalendar()[0]:
            delta_weeks -= 1
        delta = timedelta(days=-delta_days, weeks=delta_weeks)
        return d + delta
