import json

import pytz

from your_talent.errors import DatetimeError, EmptyRequestError, InvalidFieldError, MissingFieldError
from your_talent.helpers import TextHelper
from your_talent.helpers.date import DateHelper


class FormsHelper(object):
    @staticmethod
    def check_mandatory_fields(d, fields=None):
        def check_one(f):
            if f not in d:
                return 0
            elif len(d[f]) == 0:
                return 1
            return 2

        fields = fields if fields else []
        try:
            for field in fields:
                if not isinstance(field, list):
                    camel_case_field_name = TextHelper.to_camel_case(field)
                    val = check_one(field)
                else:
                    camel_case_field_name = TextHelper.to_camel_case('one_in_field')
                    for fi in field:
                        camel_case_field_name += '#' + TextHelper.to_camel_case(fi)

                    val = max([check_one(fi) for fi in field])

                if val == 0:
                    raise MissingFieldError(
                        field_name=camel_case_field_name,
                        details=camel_case_field_name + "Missing"
                    )
                if val == 1:
                    raise MissingFieldError(
                        field_name=camel_case_field_name,
                        details=camel_case_field_name + "Empty"
                    )
        except TypeError:
            return True
        return True

    @staticmethod
    def check_either_list_in_fields(d, field_list_1=[], field_list_2=[], raise_msg="One of lists must be in d"):
        if set(field_list_1) < set(d.keys()):
            return 0
        elif set(field_list_2) < set(d.keys()):
            return 1
        else:
            raise MissingFieldError(raise_msg)

    @staticmethod
    def check_form_is_not_empty(d):
        if len(d) == 0:
            raise EmptyRequestError()
        return True

    @staticmethod
    def get_order_by(order_dict=None, args_dict=None, sort_only=False, asc=True):
        """
        Get order by list
        :param order_dict: association between fields and order name
        :type order_dict: dict
        :param args_dict: dict containing orders by (get order form `order_by` and/or `order_by_desc` keys)
        :type args_dict: dict
        :param sort_only: get only order for asc or desc
        :type sort_only: bool
        :param asc: sort in order if sort_only active
        :type asc: bool
        :return: order list to apply
        :rtype: list
        """

        def get_order(args, desc):
            if args:
                for o in args:
                    if o and o != '':
                        val = order_dict.get(o, None)
                        if val is not None and desc:
                            order_by.append(val.desc())
                        elif val is not None:
                            order_by.append(val)

        order_by = []
        if order_dict is None:
            order_dict = {}

        get_order_asc = not sort_only or asc
        get_order_desc = not (sort_only and asc)

        if get_order_asc:
            get_order(args_dict.get('order_by', '').split(','), False)
        if get_order_desc:
            get_order(args_dict.get('order_by_desc', '').split(','), True)

        return order_by

    @staticmethod
    def get_datetime(form, key, t_format='%Y-%m-%d %H:%M:%S', tz=pytz.utc, field=None):
        """
        Get datetime object from args or form
        :param form: Args or form dict to retrieve datetime from
        :type form: dict
        :param key: Field to parse from request
        :type key: str|basestring
        :param t_format: Datetime format to parse if date is represented as a string
        :type t_format: str|basestring
        :param tz: TimeZone to use if date is provided as a timestamp (ms)
        :param field: field name to add to error
        :type field: str|basestring
        :return: datetime
        :rtype: datetime.datetime
        :raises InvalidFieldError: if datetime could not be parsed
        :raise InvalidFieldError: if datetime could not be parsed
        """
        try:
            return DateHelper.get_datetime_obj(form.get(key), time_format=t_format, tz=tz)
        except DatetimeError as date_error:
            if field is None:
                field = key
            raise InvalidFieldError(TextHelper.to_camel_case(field), date_error.details, **date_error.others)

    @staticmethod
    def load_json(form, key, field=None):
        """
        Load json from from. Raise error if json cannot be parsed.
        :param form: form providing json
        :type form: dict
        :param key: key referencing json
        :type key: str|basestring
        :param field: field name to add to error
        :type field: str|basestring
        :return: form.key as dict
        :rtype: dict
        :raises InvalidFieldError: if json could not be parsed
        :raise InvalidFieldError: if json could not be parsed
        """
        try:
            return json.loads(form.get(key))
        except ValueError as e:
            if field is None:
                field = key
            raise InvalidFieldError(TextHelper.to_camel_case(field), str(e))

    @staticmethod
    def get_boolean(form, key, default=False, field=None):
        """
        Get value as boolean or return default
        :param form: Args or form dict to retrieve datetime from
        :type form: dict
        :param key: Field to parse from request
        :type key: str
        :param default: Return default value
        :type default: bool
        :param field: field name to add to error
        :type field: str
        :return: Boolean value of field
        :rtype: bool
        """
        try:
            val = form.get(key, default)
            return val in ('1', 1, 't', 'true', 'True', 'ok')
        except Exception as e:
            if field is None:
                field = key
            raise InvalidFieldError(TextHelper.to_camel_case(field), str(e))

    @staticmethod
    def filters_to_dict(filter_dict, filters_list, filt_from):
        """
        Arguments to filter dict
        :param filter_dict: current filter_by dict to use
        :type filter_dict: dict
        :param filters_list: allowed filters
        :type filters_list: list
        :param filt_from: where to check for filter filed
        :type filt_from: dict
        :return: updated filter_by dict with allowed filter found
        :rtype: dict
        """
        for f in filters_list:
            if filt_from.get(f):
                filter_dict[f] = filt_from.get(f)
        return filter_dict

    @staticmethod
    def get_json(form, key, field=None):
        try:
            return json.loads(form.get(key, '[]'))
        except ValueError as e:
            if field is None:
                field = key
            raise InvalidFieldError(TextHelper.to_camel_case(field), str(e))

    @staticmethod
    def parse_multi_level(form):
        res = {}
        for k in form.keys():
            if '[' in k:
                val = form.get(k)
                k = k.replace(']', '')
                keys = k.split('[')
                base_key = keys.pop(0)
                if base_key not in res:
                    build_dict = dict()
                    res[base_key] = build_dict
                else:
                    build_dict = res[base_key]
                last_key = keys[-1]

                for ke in keys[:-1]:
                    if ke not in build_dict:
                        build_dict[ke] = dict()
                    build_dict = build_dict[ke]
                build_dict[last_key] = val
        return res
