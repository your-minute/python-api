from datetime import datetime

from sqlalchemy import inspect, text

from your_talent.errors import DatabaseConnectionError
from your_talent.classes import Slack
from your_talent.helpers import DateHelper
from your_talent.models import db


class AlchemyHelper(object):
    @staticmethod
    def ping():
        try:
            db.engine.execute('SELECT 1')
            return True
        except Exception:
            return False

    @staticmethod
    def execute(query, **kwargs):
        """
        Run query on alchemy engine
        :param query: Query to run
        :type query: str
        :param kwargs: Argument to pass to engine.execute
        :return: execute query result
        """
        if not AlchemyHelper.ping():
            if not AlchemyHelper.ping():
                raise DatabaseConnectionError()
        return db.engine.execute(text(query), **kwargs)

    @staticmethod
    def row_result_to_json(rows):
        """
        Get engine.execute results as dict list
        :param rows: rows to pass to dict
        :return: dict list corresponding to rows
        """
        res = []
        for r in rows:
            res.append(dict(r))
        return res

    @staticmethod
    def slack_update(object_type, mapper, target):
        changes = []
        for prop in mapper.iterate_properties:
            key = prop.key
            attribute_state = inspect(target).attrs.get(key)
            history = attribute_state.history
            if history.has_changes():
                value = (
                    DateHelper.get_datetime(attribute_state.value, '%Y-%m-%d %H:%M')
                    if isinstance(attribute_state.value, datetime) else str(attribute_state.value)
                )
                changes.append(key + '->' + value)
        if changes:
            changes = ' *{} {}* was changed. Created at: {}. Updated at: {}. List of changes: {}'.format(
                object_type,
                target.id if hasattr(target, 'id') else '',
                DateHelper.date_to_string(
                    DateHelper.get_datetime(target.created_at, '%Y-%m-%d %H:%M'),
                    input_format='%Y-%m-%d %H:%M',
                    tz='Europe/Paris'
                ) if hasattr(target, 'created_at') else '',
                DateHelper.date_to_string(
                    DateHelper.get_datetime(datetime.utcnow(), '%Y-%m-%d %H:%M'),
                    input_format='%Y-%m-%d %H:%M',
                    tz='Europe/Paris'
                ),
                changes
            )
            Slack.send('#api_verbose', changes)

    @staticmethod
    def act_on_update(actions, mapper, target):
        def get_old(cur_history):
            return cur_history.sum()[0] if len(cur_history.sum()) == 1 else cur_history.sum()

        changes = []
        changes_values = {}
        for prop in mapper.iterate_properties:
            key = prop.key
            attribute_state = inspect(target).attrs.get(key)
            history = attribute_state.history
            if history.has_changes():
                changes.append(key)
                changes_values[key] = {
                    'new': attribute_state.value,
                    'old': get_old(history)
                }

        for key in [v for v in changes if v in actions.keys()]:
            action = actions[key]
            if action['old'] and action['new']:
                action['function'](l_target=target, old=changes[key]['old'], new=changes[key]['new'])
            elif action['old']:
                action['function'](l_target=target, old=changes[key]['old'])
            elif action['new']:
                action['function'](l_target=target, new=changes[key]['new'])
            else:
                action['function'](l_target=target)

    @staticmethod
    def set_created_and_updated_fields(target, creation=False):
        """
        Set `created_at`, `created_by`, `updated_at` and `updated_by` fields on object

        :param target: data to update
        :param creation: is creation (default: False)
        :type creation: bool
        """
