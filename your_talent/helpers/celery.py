class ManageErrorLogCelery(object):
    @staticmethod
    def error_handler(_unknown, exc, task_id, _args, _kwargs, e_info):
        from your_talent.api import YourTalent
        from your_talent.classes import Slack
        with YourTalent.app.app_context():
            Slack.attachment(
                "#celery_errors",
                fallback="CRON Error",
                color='#e74c3c',
                title='Celery task {} failed'.format(task_id),
                text='Execution error on celery task: {}.'.format(task_id),
                fields=[
                    {
                        'title': 'Exec details',
                        'value': '{}'.format(exc),
                        'short': False
                    }
                ],
                footer=':x: {}'.format(
                    # TextHelper.url_from_config('flower', '/{}', task_id),
                    e_info
                )
            )
