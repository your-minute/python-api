import uuid
from datetime import datetime

from your_talent.errors import ForbiddenError
from your_talent.helpers import S3Helper as S3, TextHelper
from your_talent.models import db, Role


s3 = S3()


def _gen_default_name():
    return str(uuid.uuid4())


class File(db.Model):
    __tablename__ = 'files'
    use_uuid = False

    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)
    key = db.Column(db.String(500), unique=True, nullable=False)
    s3_key = db.Column(db.String(500), unique=True, nullable=False)
    url = db.Column(db.String(500), unique=True, nullable=False)
    s3_bucket = db.Column(db.String(125), unique=False, nullable=False, default='your-talent')
    you_tube_link = db.Column(db.String(500), unique=True, nullable=True)

    name = db.Column(db.String(500), nullable=False, default=_gen_default_name)
    type = db.Column(db.String(100), nullable=False, default='text')
    mime_type = db.Column(db.String(200), nullable=True, default=None)
    size = db.Column(db.Integer, nullable=True, default=None)
    nb_download = db.Column(db.Integer, nullable=False, default=0)

    public = db.Column(db.Boolean, nullable=False, default=False)
    hidden = db.Column(db.Boolean, nullable=False, default=False)
    uploaded = db.Column(db.Boolean, nullable=False, default=False)
    deleted = db.Column(db.Boolean, nullable=False, default=False)

    tmp_upload_info = db.Column(db.JSON, nullable=True)

    ## owner
    owner_id = db.Column(db.Integer, nullable=True, default=None)
    publisher_id = db.Column(db.Integer, nullable=True, default=None)

    ## dates
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    deleted_at = db.Column(db.DateTime, nullable=True)

    def __init__(self):
        pass

    @staticmethod
    def _clean_key(key):
        return TextHelper.str_to_url(key)

    @staticmethod
    def get(key):
        key = File._clean_key(key)
        return File.query.filter_by(key=key).first()

    # def update(
    #         self, up_file, file_type, key, publisher,
    #         owner=None, public=None, hidden=None, concern=None, s3_path=None
    # ):
    #     key = Files._clean_key(key)
    #     old = self.query.filter_by(key=key).first()
    #
    #     if not old:
    #         if public is None:
    #             public = False
    #         if hidden is None:
    #             hidden = False
    #         return self.upload(up_file, file_type, key, publisher, owner, public, hidden, concern, s3_path)
    #
    #     can_update = (
    #             publisher.get('type', 'public') == 'admin' or
    #             publisher.get('id', '') in (old.publisher_id, old.owner_id) or
    #             (old.owner_type == 'business' and owner.get('id', '') == old.owner_id)
    #     )
    #
    #     if not can_update:
    #         raise ForbiddenError()
    #
    #     if not old.deleted:  ## clean old file
    #         old.delete(old.publisher_id, 'admin')
    #
    #     old.deleted = False
    #     old.deleted_at = None
    #
    #     old.publisher_id = publisher.get('id', None)
    #     old.publisher_type = publisher.get('type', 'admin')
    #     if owner is not None:
    #         old.owner_id = owner.get('id', None)
    #         old.owner_type = owner.get('type', 'admin')
    #
    #     old.update_visibility(old.publisher_id, old.publisher_type, public, hidden)
    #
    #     old.type = up_file.mimetype
    #     old.size = up_file.content_length
    #
    #     if file_type == 'avatar':
    #         up_file.filename = (str(uuid.uuid4()) + str(uuid.uuid4())).replace('-', '')
    #         try:
    #             old.url, old.bucket, old.s3_key = s3.upload_image(up_file, 'avatars')
    #             old.public = True
    #             old.save()
    #             return (
    #                 True,
    #                 {
    #                     'url':                old.url,
    #                     'key':                old.key,
    #                     'bucket':             old.bucket,
    #                     'type':               'avatar',
    #                     'saved_private_info': False,
    #                     'public':             True,
    #                     'others':             None,
    #                 }
    #             )
    #         except InsertError as e:
    #             return False, e.details
    #     old.name = secure_filename(up_file.filename)
    #
    #     if file_type == 'invoice':
    #         try:
    #             old.url, old.bucket, old.s3_key = s3.upload_invoices(up_file, s3_path)
    #             up_file.filename = old.name
    #             old.public = False
    #             old.save()
    #             return (
    #                 True,
    #                 {
    #                     'url':                old.url,
    #                     'key':                old.key,
    #                     'bucket':             old.bucket,
    #                     'type':               'invoice',
    #                     'saved_private_info': True,
    #                     'public':             False,
    #                     'others':             None,
    #                 }
    #             )
    #         except InsertError as e:
    #             return False, e.details
    #
    #     up_file.filename = (str(uuid.uuid4()) + str(uuid.uuid4())).replace('-', '')
    #
    #     ## Add other type filters here
    #     try:
    #         old.url, old.bucket, old.s3_key = s3._upload(
    #             up_file, 'ty-file-upload',
    #             file_type + '/',
    #         )
    #         if old.save():
    #             return (
    #                 True,
    #                 {
    #                     'url':                old.url,
    #                     'key':                old.key,
    #                     'bucket':             old.bucket,
    #                     'type':               file_type,
    #                     'saved_private_info': True,
    #                     'public':             old.public,
    #                     'others':             {
    #                         'name': old.name,
    #                         'size': old.size,
    #                         'type': old.type
    #                     }
    #                 }
    #             )
    #         return False, 'Could not update file in db'
    #     except InsertError as e:
    #         return False, e.details

    def get_upload_url(
            self, name, key, mime_type=None, length=0, publisher=None, owner=None, public=False, hidden=False,
            up_type='video', _s3_path=None
    ):  # For video only for now
        self.key = File._clean_key(key)
        self.name = name
        self.type = up_type
        self.mime_type = mime_type
        self.size = length

        if publisher:
            self.publisher_id = publisher.get('id', None)
            self.publisher_type = publisher.get('type', 'admin')
        if owner:
            self.owner_id = owner.get('id', None)
            self.owner_type = owner.get('type', 'user')

        self.public = public
        self.hidden = hidden
        resp, self.url, self.s3_bucket, self.s3_key = s3.get_presigned(name, up_type)
        return self.save(), self.key, resp

    # def upload(
    #         self, up_file, file_type, key,
    #         publisher=None, owner=None, public=False, hidden=False, _concern=None, s3_path=None
    # ):
    #     """
    #     Upload file to s3 and save it into database
    #
    #     :param up_file: file to store
    #     :param key: in database key to identify element (cat.sub_cat.key)
    #     :type key: str
    #     :param publisher: Publisher info ({'id': , 'type': })
    #     :type publisher: dict
    #     :param owner: Owner info ({'id':, 'type': })
    #     :type owner: dict
    #     :param public: file is public
    #     :type public: int
    #     :param hidden: file is hidden
    #     :type hidden: int
    #     :param _concern: List of element concern by the upload
    #     :type _concern: list|dict
    #     :return: (uploadOk, uploadConfirmationData)
    #     :rtype: tuple
    #     """
    #     self.key = Files._clean_key(key)
    #
    #     self.id = None
    #
    #     if publisher:
    #         self.publisher_id = publisher.get('id', None)
    #         self.publisher_type = publisher.get('type', 'admin')
    #     if owner:
    #         self.owner_id = owner.get('id', None)
    #         self.owner_type = owner.get('type', 'admin')
    #
    #     self.public = public
    #     self.hidden = hidden
    #
    #     self.type = up_file.mimetype
    #     self.size = up_file.content_length
    #
    #     if file_type == 'avatar':
    #         up_file.filename = (str(uuid.uuid4()) + str(uuid.uuid4())).replace('-', '')
    #         try:
    #             self.url, self.bucket, self.s3_key = s3.upload_image(up_file, 'avatars')
    #             self.public = True
    #             self.save()
    #             return (
    #                 True,
    #                 {
    #                     'url':                self.url,
    #                     'key':                self.key,
    #                     'bucket':             self.bucket,
    #                     'type':               'avatars',
    #                     'saved_private_info': False,
    #                     'public':             True,
    #                     'others':             None,
    #                 }
    #             )
    #         except InsertError as e:
    #             return False, e.details
    #     self.name = secure_filename(up_file.filename)
    #
    #     if file_type == 'invoice':
    #         try:
    #             self.url, self.bucket, self.s3_key = s3.upload_invoices(up_file, s3_path)
    #             up_file.filename = self.name
    #             self.public = False
    #             self.save()
    #             return (
    #                 True,
    #                 {
    #                     'url':                self.url,
    #                     'key':                self.key,
    #                     'bucket':             self.bucket,
    #                     'type':               'invoice',
    #                     'saved_private_info': True,
    #                     'public':             False,
    #                     'others':             None,
    #                 }
    #             )
    #         except InsertError as e:
    #             return False, e.details
    #
    #     up_file.filename = (str(uuid.uuid4()) + str(uuid.uuid4())).replace('-', '')
    #
    #     # Add other type filters here
    #     try:
    #         self.url, self.bucket, self.s3_key = s3._upload(
    #             up_file, 'ty-file-upload',
    #             file_type + '/',
    #         )
    #         if self.save():
    #             return (
    #                 True,
    #                 {
    #                     'url':                self.url,
    #                     'key':                self.key,
    #                     'bucket':             self.bucket,
    #                     'type':               file_type,
    #                     'saved_private_info': True,
    #                     'public':             self.public,
    #                     'others':             {
    #                         'name': self.name,
    #                         'size': self.size,
    #                         'type': self.type
    #                     }
    #                 }
    #             )
    #         return False, 'Could not create file in db'
    #     except InsertError as e:
    #         return False, e.details

    def update_visibility(self, user, role, public=None, hidden=None):
        if Role.is_allowed('manage_video', role) or user in (self.owner_id, self.publisher_id):
            if public is not None:
                self.public = public
            if hidden is not None:
                self.hidden = hidden
            self.save()

    def delete(self, user, role):
        """
        Delete file from s3
        """
        if Role.is_allowed('manage_video', role) or user in (self.owner_id, self.publisher_id):
            s3._delete(self.s3_key, 'ty-file-upload')
            self.deleted = True
            self.deleted_at = datetime.utcnow()
            self.save()
            return
        raise ForbiddenError()

    def _download(self):
        down_file = s3.download(self.bucket, self.s3_key)
        self.nb_download += 1
        self.save()
        return (
            down_file,
            self.type,
            {'Content-Disposition': 'attachment;filename=%s' % self.name}
        )

    def download(self, user=None, role=None):
        """
        Download file from s3

        :return: Flask response args: (file data, file_download_name, mimetype, header)
        :rtype: tuple
        """
        if (self.public and not self.hidden) or Role.is_allowed('manage_video', role):
            return self._download()
        # User required if not public
        if user is None:
            raise ForbiddenError()
        #########
        if user in (self.owner_id, self.publisher_id):
            return self._download()
        # If not self, must be visible
        if self.hidden:
            raise ForbiddenError()
        #########
        download = False
        # Cannot see if you are not linked to it
        if not download:
            raise ForbiddenError()
        return self._download()

    def update_acl(self, new_acl='authenticated-read', user=None, role=None):
        """

        :param new_acl:
        :param user:
        :param role:
        :return:
        """
        if not user and not role:
            raise ForbiddenError()

        if Role.is_allowed('manage_video', role) or user in (self.owner_id, self.publisher_id):
            return s3.change_acl(self.s3_key, self.s3_bucket, new_acl)

        raise ForbiddenError()

    def get_presigned_download(self, user=None, role=None):
        if self.public:
            return s3.get_presigned_download(self.s3_bucket, self.s3_key)

        if not user and not role:
            raise ForbiddenError()

        if Role.is_allowed('manage_video', role) or user in (self.owner_id, self.publisher_id):
            return s3.get_presigned_download(self.s3_bucket, self.s3_key)

        raise ForbiddenError()

    def serialize(self):
        return {
            'id':           self.id,
            'key':          self.key,
            's3_key':       self.s3_key,
            'url':          self.url,
            's3_bucket':    self.s3_bucket,
            'youtube_link': self.you_tube_link,
            'name':         self.name,
            'type':         self.type,
            'size':         self.size,
            'nb_download':  self.nb_download,
            'public':       self.public,
            'hidden':       self.hidden,
            'deleted':      self.deleted,
            'owner_id':     self.owner_id,
            'publisher_id': self.publisher_id,
            'created_at':   self.created_at,
            'updated_at':   self.updated_at,
            'deleted_at':   self.deleted_at,
        }

#
# @event.listens_for(Files, 'before_update', propagate=True)
# def receive_before_update(_mapper, _connection, target):
#     target.last_updated_at = datetime.utcnow()
#     if target.public:
#         target.public = int(target.public) == 1 or bool(target.public) is True
#     if target.hidden:
#         target.hidden = int(target.hidden) == 1 or bool(target.hidden) is True
#     if target.deleted:
#         target.deleted = int(target.deleted) == 1 or bool(target.deleted) is True
#
#
# @event.listens_for(Files, 'before_insert', propagate=True)
# def receive_before_insert(_mapper, _connection, target):
#     target.created_at = datetime.utcnow()
#     target.last_updated_at = datetime.utcnow()
#     if target.public:
#         target.public = int(target.public) == 1 or bool(target.public) is True
#     if target.hidden:
#         target.hidden = int(target.hidden) == 1 or bool(target.hidden) is True
#     if target.deleted:
#         target.deleted = int(target.deleted) == 1 or bool(target.deleted) is True
