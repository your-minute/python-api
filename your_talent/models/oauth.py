import time
from datetime import datetime, timedelta

from sqlalchemy import event

from your_talent.models import db, relationship


#
#
# class OAuth2Client(db.Model):
#     __tablename__ = 'oauth_clients'
#
#     id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)
#
#     client_secret = db.Column(db.String(64), nullable=True)
#     token_endpoint_auth_method = db.Column(db.String(20), nullable=True)
#     grant_types = db.Column(db.String(255), nullable=False)
#     scope = db.Column(db.String(255), nullable=False)
#     response_types = db.Column(db.String(255), nullable=False)
#     client_name = db.Column(db.String(255), nullable=False)
#     allowed_redirect_uris = db.Column(db.Text, nullable=False)
#     client_uri = db.Column(db.Text, nullable=False)
#
#     created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
#     last_updated_at = db.Column(db.DateTime, nullable=True)
#     created_by = db.Column(
#         db.Integer,
#         db.ForeignKey(
#             'users.id', ondelete='RESTRICT', onupdate='CASCADE'
#         ), nullable=True
#     )
#     last_updated_by = db.Column(
#         db.Integer,
#         db.ForeignKey(
#             'users.id', ondelete='CASCADE', onupdate='CASCADE'
#         ), nullable=True
#     )
#
#     def check_client_secret(self, client_secret):
#         return self.client_secret == client_secret
#
#     def check_grant_type(self, grant_type):
#         return grant_type in self.grant_types
#
#     def check_response_type(self, response_type):
#         return response_type in self.response_types.splitlines()
#
#     def check_redirect_uri(self, redirect_uri):
#         if redirect_uri not in self.allowed_redirect_uris.splitlines():
#             logging.info('Oauth2Client  :: check_redirect_uri :: invalid uri')
#         return redirect_uri in self.allowed_redirect_uris.splitlines()
#
#     def check_requested_scopes(self, scopes):
#         return set(self.scope.split()).issuperset(scopes)
#
#     @staticmethod
#     def check_token_endpoint_auth_method(method):
#         return method in ['none', 'client_secret_post', 'client_secret_basic']
#
#     @staticmethod
#     def get_default_redirect_uri():
#         return 'http://localhost:5000/api/me'
#
#     def has_client_secret(self):
#         return bool(self.client_secret)
#
#     @property
#     def client_id(self):
#         return self.id
#
#     def serialize(self):
#         return {
#             'id':                         self.id,
#             'client_secret':              self.client_secret,
#             'token_endpoint_auth_method': self.token_endpoint_auth_method,
#             'grant_types':                self.grant_types.split('\n') if self.grant_types else [],
#             'scope':                      self.scope.split(' ') if self.scope else [],
#             'response_types':             self.response_types.split('\n') if self.response_types else [],
#             'client_name':                self.client_name,
#             'allowed_redirect_uris':      self.allowed_redirect_uris.split('\n') if self.allowed_redirect_uris else [],
#             'client_uri':                 self.client_uri,
#             'created_at':                 self.created_at,
#             'last_updated_at':            self.last_updated_at,
#             'created_by':                 self.created_by,
#             'last_updated_by':            self.last_updated_by,
#         }
#
#
# @event.listens_for(OAuth2Client, 'before_insert', propagate=True)
# def receive_before_insert(_mapper, _connection, target):
#     target.created_at = datetime.utcnow()
#
#
# @event.listens_for(OAuth2Client, 'before_update', propagate=True)
# def receive_before_update(_mapper, _connection, target):
#     target.updated_at = datetime.utcnow()
#
#
# @event.listens_for(OAuth2Client.grant_types, 'set', propagate=True, retval=True)
# def before_set_grant_types(_a, value, _b, _c):
#     return TextHelper.clean_and_convert_separated_str(value, new_separator='\n')
#
#
# @event.listens_for(OAuth2Client.allowed_redirect_uris, 'set', propagate=True, retval=True)
# def before_set_allowed_redirect_uris(_a, value, _b, _c):
#     return TextHelper.clean_and_convert_separated_str(value, new_separator='\n')
#
#
# @event.listens_for(OAuth2Client.scope, 'set', propagate=True, retval=True)
# def before_set_scope(_a, value, _b, _c):
#     return TextHelper.clean_and_convert_separated_str(value, new_separator='\n')
#
#
# @event.listens_for(OAuth2Client.response_types, 'set', propagate=True, retval=True)
# def before_set_response_types(_a, value, _b, _c):
#     return TextHelper.clean_and_convert_separated_str(value, new_separator='\n')
#

class OAuth2Token(db.Model):
    __tablename__ = 'oauth2_tokens'
    DEFAULT_TOKEN_TYPE = 'Bearer'
    DEFAULT_EXPIRE_IN = timedelta(days=31).total_seconds()
    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)

    client_id = db.Column(
        db.Integer,
        # db.ForeignKey(
        #     'oauth_clients.id', ondelete='CASCADE', onupdate='CASCADE'
        # ), nullable=True, default=None
    )
    scope = db.Column(db.String(255))

    user_id = db.Column(
        db.Integer,
        db.ForeignKey(
            'users.id', ondelete='CASCADE', onupdate='CASCADE'
        ), nullable=True, index=True
    )

    access_token = db.Column(db.String(255))
    refresh_token = db.Column(db.String(255))
    token_type = db.Column(db.String(45), nullable=False, default=DEFAULT_TOKEN_TYPE)
    revoked = db.Column(db.Integer, nullable=False, default=0)
    expires_in = db.Column(db.Integer, nullable=False, default=DEFAULT_EXPIRE_IN)

    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    last_updated_at = db.Column(db.DateTime, nullable=True)
    created_by = db.Column(
        db.Integer,
        db.ForeignKey(
            'users.id', ondelete='RESTRICT', onupdate='CASCADE'
        ), nullable=True
    )
    last_updated_by = db.Column(
        db.Integer,
        db.ForeignKey(
            'users.id', ondelete='CASCADE', onupdate='CASCADE'
        ), nullable=True
    )

    user = relationship('User', foreign_keys='OAuth2Token.user_id')

    def is_refresh_token_expired(self):
        return self.get_expires_at() < time.time()

    def get_expires_at(self):
        expires_at = self.created_at + timedelta(seconds=self.expires_in)
        return time.mktime(expires_at.timetuple())

    def get_scope(self):
        return self.scope

    def get_expires_in(self):
        return self.expires_in

    def get_user(self):
        user = self.user
        if not user:
            return None, None
        return user.role, user

    def serialize(self):
        return {
            'id':              self.id,
            'client_id':       self.client_id,
            'scope':           self.scope,
            'user_id':         self.user_id,
            'user_type':       self.user_type,
            'access_token':    self.access_token,
            'refresh_token':   self.refresh_token,
            'token_type':      self.token_type,
            'revoked':         self.revoked,
            'expires_in':      self.expires_in,
            'created_at':      self.created_at,
            'last_updated_at': self.last_updated_at,
            'created_by':      self.created_by,
            'last_updated_by': self.last_updated_by,
        }


@event.listens_for(OAuth2Token, 'before_insert', propagate=True)
def receive_before_insert(_mapper, _connection, target):
    target.created_at = datetime.utcnow()


@event.listens_for(OAuth2Token, 'before_update', propagate=True)
def receive_before_update(_mapper, _connection, target):
    target.updated_at = datetime.utcnow()
