import re
from datetime import datetime

from flask import request
from sqlalchemy import event, or_

from your_talent.classes import Registry
from your_talent.errors import InsertError, InvalidFieldError, MissingFieldError, UnauthorizedError
from your_talent.helpers import FormsHelper, PasswordHelper, TextHelper
from your_talent.models import db, get_from, relationship


class User(db.Model):
    __tablename__ = 'users'
    YOUR_TALENT_LOGIN = 0
    FACEBOOK_LOGIN = 1
    GOOGLE_LOGIN = 2
    TWITCH_LOGIN = 3
    YOUTUBE_LOGIN = 4
    OTHERS_LOGIN = 99
    MAX_PASSWORD_FAILURES_ALLOWED = 5
    providers = [YOUR_TALENT_LOGIN, FACEBOOK_LOGIN, GOOGLE_LOGIN]
    DEFAULT_ROLE_NAME = 'guest'

    RESERVED_NAMES = [
        re.compile('ym_.*'), re.compile('.*_ym'),
        re.compile('yt_.*'), re.compile('.*_yt'),
        'system', 'admin', 'support'
    ]

    _provider_to_human = {
        YOUR_TALENT_LOGIN: 'own',
        FACEBOOK_LOGIN:    'fb',
        GOOGLE_LOGIN:      'google',
        TWITCH_LOGIN:      'twitch',
        YOUTUBE_LOGIN:     'yt',
        OTHERS_LOGIN:      'others',
    }
    _human_to_provider = {
        'own':    YOUR_TALENT_LOGIN,
        'fb':     FACEBOOK_LOGIN,
        'google': GOOGLE_LOGIN,
        'twitch': TWITCH_LOGIN,
        'yt':     YOUTUBE_LOGIN,
        'others': OTHERS_LOGIN,
    }

    # Table definition
    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)

    name = db.Column(db.String(255), nullable=False, unique=False)
    email = db.Column(db.String(255), nullable=True, unique=False)
    email_verified = db.Column(db.Boolean, nullable=False, default=False)

    login_provider = db.Column(db.Integer, nullable=False, default=YOUR_TALENT_LOGIN)
    password = db.Column(db.String(255), nullable=True)
    wrong_password_attempt = db.Column(
        db.Integer, nullable=False,
        default=MAX_PASSWORD_FAILURES_ALLOWED
    )
    login_provider_user = db.Column(db.String(255), nullable=True)

    phone = db.Column(db.String(15), nullable=True)
    phone_verified = db.Column(db.Boolean, nullable=False, default=False)

    first_name = db.Column(db.String(255), nullable=True)
    last_name = db.Column(db.String(255), nullable=True)
    address = db.Column(db.String(255), nullable=True)
    city = db.Column(db.String(255), nullable=True)
    country = db.Column(db.String(255), nullable=True)

    public_name = db.Column(db.String(255), nullable=True, unique=True)
    public_email = db.Column(db.String(255), nullable=True, unique=True)
    public_phone = db.Column(db.String(255), nullable=True, unique=True)

    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    last_updated_at = db.Column(db.DateTime, nullable=True)
    last_pwd_update = db.Column(db.DateTime, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted = db.Column(db.Boolean, nullable=False, default=False)
    consent = db.Column(db.Boolean, nullable=False, default=False)
    action_token = db.Column(db.String(555))

    avatar_id = db.Column(
        db.Integer,
        db.ForeignKey(
            'avatars.id', ondelete='CASCADE', onupdate='CASCADE'
        ), nullable=True, index=True
    )
    role_id = db.Column(
        db.Integer,
        db.ForeignKey(
            'roles.id', ondelete='CASCADE', onupdate='CASCADE'
        ), nullable=False, index=True
    )

    avatar = relationship(
        'Avatar', foreign_keys='User.avatar_id',
        single_parent=True, uselist=False
    )
    role = relationship(
        'Role', foreign_keys='User.role_id',
        single_parent=True, uselist=False
    )
    social_networks = relationship('UserSocialNetwork')  # not sure

    @staticmethod
    def get(user_id):
        if TextHelper.is_int(user_id):
            return User.query.get(user_id)
        return User.query.filter(
            or_(User.name == user_id, User.email == user_id)
        ).first()

    @staticmethod
    def default_role_id():
        return Role.get_role_id(User.DEFAULT_ROLE_NAME)

    @staticmethod
    def get_provider(human):
        """
        Get int provider from human readable provider
        :param human: human readable provider
        :type human: str
        :return: int provider for DB
        :rtype: int
        """
        return User._human_to_provider.get(human, 99)

    @staticmethod
    def provider_to_human(provider):
        """
        Get int provider from human readable provider
        :param provider: int provider from DB
        :type provider: int
        :return: human readable provider
        :rtype: str
        """
        return User._provider_to_human.get(provider, 'others')

    def check_password(self, password):
        """
        Verify password for user
        :param password: password to check
        :type password: str
        :return: Password match
        :rtype: bool
        """
        return PasswordHelper().verify(password, self.password)

    def check_login_id(self, token):
        """
        Verify token for user
        :param token: token to check
        :type token: str
        :return: token match
        :rtype: bool
        """
        return PasswordHelper().verify(token, self.login_provider_user)

    def set_avatar(self, avatar):
        file_key = avatar.get('file_key')
        link = avatar.get('link')

        if file_key:
            from your_talent.models import File
            file = File.query.filter_by(key=file_key).first()
            if not file and not link:
                return False, 'Unknow avatar: {}'.format(file_key)
            file.update_acl('public-read')
            link = 'https://' + file.url if 'https://' not in file.url else file.url
            file.uploaded = True
            file.save(False)
        if not self.avatar:
            self.avatar = Avatar()

        self.avatar.file_key = file_key
        self.avatar.link = link
        self.avatar.name = avatar.get('name')
        if not self.avatar.save():
            return False, 'Could not save avatar'
        return True, ''

    def set_social_network(self, name, sn, commit=True):
        network = SocialNetwork.get(name)
        if not network:
            return False, 'Unknow network {}'.format(name)

        usn = UserSocialNetwork.query.filter_by(id_social_network=network.id, id_user=self.id).first()
        if not usn:
            usn = UserSocialNetwork()
            usn.id_social_network = network.id
            usn.id_user = self.id
        usn.use_base_url = FormsHelper.get_boolean(sn, 'use_base_url', True)
        usn.public = FormsHelper.get_boolean(sn, 'public', True)
        usn.link = sn.get('link')

        if not usn.save(commit):
            return False, 'Could not save social network'

        self.social_networks.append(usn)

        return True, None

    def set_social_networks(self, sns):
        if not sns:
            return False, ''

        message = ''
        has_errors = False
        for name, sn in sns.items():
            ok, m = self.set_social_network(name, sn, False)
            if not ok:
                has_errors = True
                message += '\n' + m

        if not self.save():
            raise InsertError()

        return has_errors, message

    def delete(self):
        self.deleted = True
        self.deleted_at = datetime.utcnow()
        return self.save()

    def serialize(self, public=True):
        if public:
            return {
                'name':           (
                    self.public_name
                    if self.public_name
                    else (
                        '{} {}.'.format(self.first_name, self.last_name[0])
                        if self.first_name and self.last_name
                        else self.name
                    )
                ),
                'email':          self.public_email if self.public_email else '',
                'phone':          self.public_phone if self.public_phone else '',
                'avatar_link':    get_from(self.avatar, 'link'),
                'role_name':      self.role.name if self.role else None,
                'social_network': [sn.serialize() for sn in self.social_networks if sn.public],
            }

        return {
            'id':                     self.id,
            'name':                   self.name,
            'email':                  self.email,
            'email_verified':         self.email_verified,
            'login_provider':         self.login_provider,
            'password':               self.password,
            'wrong_password_attempt': self.wrong_password_attempt,
            'login_provider_user':    self.login_provider_user,
            'phone':                  self.phone,
            'phone_verified':         self.phone_verified,
            'first_name':             self.first_name,
            'last_name':              self.last_name,
            'address':                self.address,
            'city':                   self.city,
            'country':                self.country,
            'public_name':            self.public_name,
            'public_email':           self.public_email,
            'public_phone':           self.public_phone,
            'created_at':             self.created_at,
            'last_updated_at':        self.last_updated_at,
            'last_pwd_update':        self.last_pwd_update,
            'deleted':                self.deleted,
            'avatar_id':              self.avatar_id,
            'role_id':                self.role_id,
            'role_name':              get_from(self.role, 'name'),
            'social_network':         [sn.serialize() for sn in self.social_networks],
            'avatar_link':            get_from(self.avatar, 'link'),
            'avatar':                 self.avatar.serialize() if self.avatar else None,
            'role':                   self.role.serialize() if self.role else None,
        }


@event.listens_for(User, 'before_insert', propagate=True)
def receive_before_insert(_mapper, _connection, target):
    if not hasattr(target, 'role_id') or not target.role_id:
        target.role_id = User.default_role_id()
    if (
            (not hasattr(target, 'password') and not hasattr(target, 'login_provider_user')) or
            (not target.password and not target.login_provider_user) or
            (target.password == '' and target.login_provider_user == '')
    ):
        raise MissingFieldError('PasswordOrLoginToken', 'Password or Login User is required to create an user')


@event.listens_for(User.password, 'set', propagate=True, retval=True)
def before_set_password(target, value, _old, _initiator):
    target.last_pwd_update = datetime.utcnow()
    return PasswordHelper().encode(value)


@event.listens_for(User.deleted, 'set', propagate=True, retval=True)
def before_set_password(target, value, _old, _initiator):
    value = TextHelper.to_bool(value)
    if value:
        target.deleted_at = datetime.utcnow()
    return value


@event.listens_for(User.login_provider_user, 'set', propagate=True, retval=True)
def before_set_login_provider_user(target, value, _old, _initiator):
    target.last_pwd_update = datetime.utcnow()
    return PasswordHelper().encode(value)


@event.listens_for(User.login_provider, 'set', propagate=True, retval=True)
def before_set_login_provider(_target, value, _old, _initiator):
    if isinstance(value, str):
        value = User.get_provider(value)
    if value not in User.providers:
        raise InvalidFieldError('LoginProvider', 'Provider is not supported')
    return value


@event.listens_for(User.email, 'set', propagate=True, retval=True)
def before_set_email(target, value, _old, _initiator):
    target.action_token = TextHelper.generate_token(api_token=True)
    return value.strip()


@event.listens_for(User.deleted, 'set', propagate=True, retval=True)
def before_set_email(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


@event.listens_for(User.consent, 'set', propagate=True, retval=True)
def before_set_email(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


@event.listens_for(User.name, 'set', propagate=True)
def before_set_name(_target, value, _old, _initiator):
    if not request or 'users/init' not in request.url:
        for c in User.RESERVED_NAMES:
            if (
                    (isinstance(c, str) and c == value) or
                    (not isinstance(c, str) and c.match(value))
            ):
                raise InvalidFieldError('Name', 'User name is reserved')


class Role(db.Model):
    __tablename__ = 'roles'
    LOCKED_ROLE_NAMES = ['guest', 'admin', 'logged', 'public']
    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)
    name = db.Column(db.String(40), nullable=False, unique=True)

    # manage rights
    manage_user = db.Column(db.Boolean, nullable=False, default=False)
    manage_video = db.Column(db.Boolean, nullable=False, default=False)
    manage_comment = db.Column(db.Boolean, nullable=False, default=False)
    manage_avatar = db.Column(db.Boolean, nullable=False, default=False)
    manage_channel = db.Column(db.Boolean, nullable=False, default=False)
    manage_reward = db.Column(db.Boolean, nullable=False, default=False)
    manage_role = db.Column(db.Boolean, nullable=False, default=False)
    manage_top = db.Column(db.Boolean, nullable=False, default=False)
    manage_calendar = db.Column(db.Boolean, nullable=False, default=False)
    manage_setting = db.Column(db.Boolean, nullable=False, default=False)

    validate_video = db.Column(db.Boolean, nullable=False, default=False)
    moderate_comment = db.Column(db.Boolean, nullable=False, default=False)

    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    last_updated_at = db.Column(db.DateTime, nullable=True)

    users = relationship(
        'User', foreign_keys='User.role_id',
        back_populates='role'
    )

    @staticmethod
    def is_allowed(action, role):
        """
        Check if user having role can make action

        :param action: action name
        :type action: str
        :param role: user role
        :type role: int|str|Role
        :return:
        """
        if isinstance(role, str):
            role = Role.query.filter_by(name=role).first()
        elif isinstance(role, int):
            role = Role.query.get(role)
        if not isinstance(role, Role):
            raise Exception
        return getattr(role, action.strip().replace(' ', '_'))

    @staticmethod
    def get_role_id(role):
        """
        Check if user having role can make action

        :param role: user role
        :type role: int|str|Role
        :return: role id
        :rtype: int
        """
        if isinstance(role, str):
            role = Role.query.filter_by(name=role).first()
        elif isinstance(role, int):
            role = Role.query.get(role)
        if not isinstance(role, Role):
            raise Exception
        return role.id

    def user_is_allowed(self, rights, allow_own=False, item_user_id=None):
        """
        Check if role allow action to user

        :param rights: list of rights to have to perform action
        :type rights: list[str]
        :param allow_own: Allow action if item is owned by user
        :type allow_own: bool
        :param item_user_id: user id to check if allow_on
        :type item_user_id: int|str
        :return: User can make action
        """
        current_user = Registry.registered('current-user-id')
        if allow_own and current_user == item_user_id:
            return True
        if self.name == 'guest':
            return False
        if self.name == 'admin':
            return True
        for r in rights:
            if not Role.is_allowed(r, self):
                return False
        return True

    @staticmethod
    def get(role_id):
        role = Role.query.get(role_id)
        if not role:
            role = Role.query.filter_by(name=role_id).first()
        return role

    def serialize(self, serialize_users=False):
        users = []
        if serialize_users and self.users:
            users = [u.serialize() for u in self.users]
        return {
            'id':               self.id,
            'name':             self.name,
            'manage_user':      self.manage_user,
            'manage_video':     self.manage_video,
            'manage_comment':   self.manage_comment,
            'manage_avatar':    self.manage_avatar,
            'manage_channel':   self.manage_channel,
            'manage_reward':    self.manage_reward,
            'manage_role':      self.manage_role,
            'manage_top':       self.manage_top,
            'manage_calendar':  self.manage_calendar,
            'manage_setting':   self.manage_setting,
            'validate_video':   self.validate_video,
            'moderate_comment': self.moderate_comment,
            'created_at':       self.created_at,
            'last_updated_at':  self.last_updated_at,
            'users':            users,
        }


@event.listens_for(Role.name, 'set', propagate=True)
def before_set_name(_target, value, old, _initiator):
    if not request or 'roles/init' not in request.url:
        if old in Role.LOCKED_ROLE_NAMES or value in Role.LOCKED_ROLE_NAMES:
            raise UnauthorizedError()


@event.listens_for(Role.manage_user, 'set', propagate=True, retval=True)
def before_set_manage_user(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


@event.listens_for(Role.manage_video, 'set', propagate=True, retval=True)
def before_set_manage_video(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


@event.listens_for(Role.manage_comment, 'set', propagate=True, retval=True)
def before_set_manage_comment(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


@event.listens_for(Role.manage_avatar, 'set', propagate=True, retval=True)
def before_set_manage_avatar(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


@event.listens_for(Role.manage_channel, 'set', propagate=True, retval=True)
def before_set_manage_channel(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


@event.listens_for(Role.manage_reward, 'set', propagate=True, retval=True)
def before_set_manage_reward(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


@event.listens_for(Role.manage_role, 'set', propagate=True, retval=True)
def before_set_manage_role(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


@event.listens_for(Role.manage_top, 'set', propagate=True, retval=True)
def before_set_manage_top(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


@event.listens_for(Role.manage_calendar, 'set', propagate=True, retval=True)
def before_set_manage_calendar(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


@event.listens_for(Role.manage_setting, 'set', propagate=True, retval=True)
def before_set_manage_setting(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


@event.listens_for(Role.validate_video, 'set', propagate=True, retval=True)
def before_set_validate_video(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


@event.listens_for(Role.moderate_comment, 'set', propagate=True, retval=True)
def before_set_moderate_comment(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


@event.listens_for(Role, 'before_insert', propagate=True)
def receive_before_insert(_mapper, _connection, target):
    target.created_at = datetime.utcnow()


@event.listens_for(Role, 'before_update', propagate=True)
def receive_before_update(_mapper, _connection, target):
    target.updated_at = datetime.utcnow()


class Avatar(db.Model):
    __tablename__ = 'avatars'
    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=True, unique=True)
    file_key = db.Column(db.String(255), nullable=True, unique=False)
    link = db.Column(db.String(500), nullable=False, unique=False)
    from_defaults = db.Column(db.Boolean, nullable=False, default=False)

    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    last_updated_at = db.Column(db.DateTime, nullable=True)

    def serialize(self):
        return {
            'id':              self.id,
            'name':            self.name,
            'file_key':        self.file_key,
            'link':            self.link,
            'from_defaults':   self.from_defaults,
            'created_at':      self.created_at,
            'last_updated_at': self.last_updated_at,
        }


@event.listens_for(Avatar, 'before_insert', propagate=True)
def receive_before_insert(_mapper, _connection, target):
    target.created_at = datetime.utcnow()


@event.listens_for(Avatar, 'before_update', propagate=True)
def receive_before_update(_mapper, _connection, target):
    target.updated_at = datetime.utcnow()


class SocialNetwork(db.Model):
    __tablename__ = 'social_networks'
    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)
    name = db.Column(db.String(40), nullable=False, unique=True)
    logo = db.Column(db.String(555), nullable=False, unique=True)
    base_link_url = db.Column(db.String(555), unique=True)

    @staticmethod
    def get(sn_id):
        if TextHelper.is_int(sn_id):
            return SocialNetwork.query.get(sn_id)
        return SocialNetwork.query.filter_by(name=sn_id).first()

    def serialize(self):
        return {
            'id':            self.id,
            'name':          self.name,
            'logo':          self.logo,
            'base_link_url': self.base_link_url,
        }


@event.listens_for(SocialNetwork.base_link_url, 'set', retval=True, propagate=True)
def _before_set_link(_target, value, _old, _initiator):
    if value[-1] != '/':
        return value + '/'
    return value


class UserSocialNetwork(db.Model):
    __tablename__ = 'user_social_networks'
    id_social_network = db.Column(
        db.Integer,
        db.ForeignKey('social_networks.id', ondelete='CASCADE', onupdate='CASCADE'),
        primary_key=True, autoincrement=False
    )
    id_user = db.Column(
        db.Integer,
        db.ForeignKey('users.id', ondelete='CASCADE', onupdate='CASCADE'),
        primary_key=True, autoincrement=False
    )
    _link = db.Column(db.String(555), name='link', nullable=False, unique=False)
    use_base_url = db.Column(db.Boolean, nullable=False, default=True)
    public = db.Column(db.Boolean, nullable=False, default=True)

    sn = relationship('SocialNetwork')

    @property
    def name(self):
        return self.sn.name

    @property
    def logo(self):
        return self.sn.logo

    @property
    def link(self):
        if self.use_base_url:
            return '{}{}'.format(self.sn.base_link_url, self._link)
        return self._link

    @link.setter
    def link(self, val):
        self._link = val

    def serialize(self):
        return {
            'social_network_id': self.id_social_network,
            'user_id':           self.id_user,
            'link':              self.link,
            'logo':              self.logo,
            'use_base_url':      self.use_base_url,
            'public':            self.public,
        }


@event.listens_for(UserSocialNetwork._link, 'set', retval=True, propagate=True)
def _before_set_link(_target, value, _old, _initiator):
    if value[0] == '/':
        return value[1:]
    return value
