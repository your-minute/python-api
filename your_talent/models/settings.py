from your_talent.models import db


class Setting(db.Model):
    __tablename__ = 'settings'

    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)

    key = db.Column(db.String(255), nullable=False, unique=True)
    value = db.Column(db.String(255))

    @classmethod
    def set(cls, key, value):
        setting = cls.query.filter_by(key=key).first()
        if not setting:
            setting = Setting()
            setting.key = key
        setting.value = value
        return setting.save()

    @classmethod
    def get(cls, key, default=None, type_check=None, type_convert=None):
        setting = cls.query.filter_by(key=key).first()

        if not setting:
            return default

        if not setting.value or (type_check and not type_check(setting.value)):
            return default

        return type_convert(setting.value) if type_convert else setting.value
