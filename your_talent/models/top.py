from datetime import datetime, timedelta

from dateutil.relativedelta import relativedelta
from sqlalchemy import event
from sqlalchemy.ext.hybrid import hybrid_property

from your_talent.classes import Slack
from your_talent.classes.mails import Mail
from your_talent.helpers import AlchemyHelper, TextHelper
from your_talent.models import Channel, db, Planning, relationship, Video


class Reward(db.Model):
    __tablename__ = 'rewards'

    PUBLICATION = 0
    MONEY = 1
    ITEM = 2
    SPONSOR = 3

    KINDS = [PUBLICATION, MONEY, ITEM, SPONSOR]

    _kind_conv = {
        PUBLICATION:   'publication',
        MONEY:         'money',
        ITEM:          'item',
        SPONSOR:       'sponsor',
        'publication': PUBLICATION,
        'money':       MONEY,
        'item':        ITEM,
        'sponsor':     SPONSOR,
    }

    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)

    kind = db.Column(db.String(20), nullable=False, default='money')
    amount = db.Column(db.DECIMAL)
    value = db.Column(db.String(45), nullable=False, default='euros')
    message = db.Column(db.Text, nullable=False)

    def serialize(self):
        return {
            'id':      self.id,
            'kind':    self.kind,
            'amount':  self.amount,
            'value':   self.value,
            'message': self.message,
        }


class PositionReward(db.Model):
    __tablename__ = 'position_rewards'
    top_id = db.Column(
        db.Integer,
        db.ForeignKey('tops.id', onupdate='CASCADE', ondelete='RESTRICT'),
        nullable=False,
        primary_key=True
    )

    reward_id = db.Column(
        db.Integer,
        db.ForeignKey('rewards.id', onupdate='CASCADE', ondelete='RESTRICT'),
        nullable=False,
        primary_key=True
    )

    position = db.Column(db.Integer, nullable=False, default=1)
    spe_name = db.Column(db.String(255))
    reward = relationship(
        Reward,
        uselist=False
    )

    @property
    def kind(self):
        return self.reward.kind

    @property
    def amount(self):
        return self.reward.amount

    @property
    def value(self):
        return self.reward.value

    @property
    def message(self):
        return self.reward.message


class Top(db.Model):
    __tablename__ = 'tops'

    FREQUENCY_TYPE_DAY = 0
    FREQUENCY_TYPE_WEEK = 1
    FREQUENCY_TYPE_MONTH = 2
    FREQUENCY_TYPE_YEAR = 3

    DURATION_KIND_MINUTE = 0
    DURATION_KIND_HOUR = 1
    DURATION_KIND_DAY = 2
    DURATION_KIND_WEEK = 3
    DURATION_KIND_MONTH = 4
    DURATION_KIND_YEAR = 5

    VOTE_LIKE = 0
    VOTE_NO_PONDERATE = 1
    VOTE_PONDERATED = 2

    _conv_type = {
        FREQUENCY_TYPE_DAY:   'day',
        FREQUENCY_TYPE_WEEK:  'week',
        FREQUENCY_TYPE_MONTH: 'month',
        FREQUENCY_TYPE_YEAR:  'year',
        'day':                FREQUENCY_TYPE_DAY,
        'week':               FREQUENCY_TYPE_WEEK,
        'month':              FREQUENCY_TYPE_MONTH,
        'year':               FREQUENCY_TYPE_YEAR,
    }

    _conv_duration_type = {
        DURATION_KIND_MINUTE: 'min',
        DURATION_KIND_HOUR:   'hour',
        DURATION_KIND_DAY:    'day',
        DURATION_KIND_WEEK:   'week',
        DURATION_KIND_MONTH:  'month',
        DURATION_KIND_YEAR:   'year',
        'min':                DURATION_KIND_MINUTE,
        'hour':               DURATION_KIND_HOUR,
        'day':                DURATION_KIND_DAY,
        'week':               DURATION_KIND_WEEK,
        'month':              DURATION_KIND_MONTH,
        'year':               DURATION_KIND_YEAR,
    }

    _conv_kind = {
        VOTE_LIKE:       'like',
        VOTE_PONDERATED: 'score',
        'like':          VOTE_LIKE,
        'score':         VOTE_PONDERATED,
    }

    _to_fr = {
        'day':   'journalier',
        'week':  'hebdomadaire',
        'month': 'mensuel',
        'year':  'annuel',
    }
    _position = {
        1: '1ère',
        2: '2de',
    }

    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)

    name = db.Column(db.String(555), nullable=False, unique=True)
    repeat = db.Column(db.Boolean, nullable=False, default=True)
    _frequency_type = db.Column(db.Integer, name='frequency_type', nullable=False, default=FREQUENCY_TYPE_WEEK)
    frequency_value = db.Column(db.Integer, nullable=False, default=1)
    description = db.Column(db.String(255))
    duration = db.Column(db.Integer, nullable=False, default=2 * 60 * 60)  # duration to 2 hrs by default
    _duration_kind = db.Column(db.Integer, name='duration_kind', nullable=False, default=DURATION_KIND_MINUTE)
    from_day = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    _for_channels = db.Column(db.String(555), name='for_channels', index=True)
    _vote_kind = db.Column(db.Integer, nullable=False, name='vote_kind', default=VOTE_LIKE)
    pinned = db.Column(db.Boolean, nullable=False, default=False)
    next_top_id = db.Column(
        db.Integer,
        db.ForeignKey('tops.id', onupdate='CASCADE', ondelete='SET NULL'),
        nullable=True
    )

    _rewards = relationship(PositionReward)

    @property
    def next_iteration(self):
        if self._frequency_type == self.FREQUENCY_TYPE_DAY:
            return self.from_day + timedelta(days=self.frequency_value)
        if self._frequency_type == self.FREQUENCY_TYPE_WEEK:
            return self.from_day + timedelta(weeks=self.frequency_value)
        if self._frequency_type == self.FREQUENCY_TYPE_MONTH:
            return self.from_day + relativedelta(months=self.frequency_value)
        if self._frequency_type == self.FREQUENCY_TYPE_YEAR:
            return self.from_day + relativedelta(years=self.frequency_value)

        return None

    @hybrid_property
    def frequency_type(self):
        return self._conv_type.get(self._frequency_type)

    @frequency_type.expression
    def frequency_type(self):
        return self._frequency_type

    @frequency_type.setter
    def frequency_type(self, val):
        self._frequency_type = self._conv_type.get(val, self.FREQUENCY_TYPE_MONTH)

    @hybrid_property
    def duration_kind(self):
        return self._conv_duration_type.get(self._duration_kind)

    @duration_kind.expression
    def duration_kind(self):
        return self._duration_kind

    @duration_kind.setter
    def duration_kind(self, val):
        self._duration_kind = self._conv_duration_type.get(val, self.FREQUENCY_TYPE_MONTH)

    @hybrid_property
    def vote_kind(self):
        return self._conv_kind.get(self._vote_kind)

    @vote_kind.expression
    def vote_kind(self):
        return self._vote_kind

    @vote_kind.setter
    def vote_kind(self, val):
        self._vote_kind = self._conv_kind.get(val, self.VOTE_LIKE)

    @property
    def for_channels(self):
        return self._for_channels.split(',') if self._for_channels else None

    @for_channels.setter
    def for_channels(self, l):
        message = ''
        if not isinstance(l, list):
            l = l.split(',') if ',' in l else l.split('\n')
        res = ''
        for c in l:
            channel = c
            if not isinstance(channel, Channel):
                channel = Channel.get(c)
            if channel:
                res += str(channel.name) + ','
            else:
                message += 'Could not find channel for ref: {}'.format(c)
        if message and message != '':
            Slack.send('#api_verbose', message)
        self._for_channels = res[:-1]

    @property
    def rewards(self):
        if not self._rewards:
            return {}
        res = {}
        for r in self._rewards:
            assert isinstance(r, PositionReward)
            key = r.spe_name if r.position == 0 else r.position
            if key not in res:
                res[key] = []
            res[key].append({'value': r.value, 'message': r.message, 'kind': r.kind, 'amount': float(r.amount)})
        return res

    @staticmethod
    def get(top_id):
        top = None
        if TextHelper.is_number(top_id):
            top = Top.query.get(top_id)

        if not top:
            top = Top.query.filter_by(name=top_id).first()
        return top

    def get_mail_object(self, pos):
        return {
            'position': self._position.get(pos) if pos in self._position else '{}ème'.format(pos),
            'kind':     self._to_fr.get(self.frequency_type),
            'message':  'En récompense, '.join(r.get('message', '') for r in self.rewards.get(pos, []))
        }

    def set_reward(self, pos, reward):
        position_reward = PositionReward.query.filter_by(top_id=self.id, position=pos).first()
        reward_obj = None
        if position_reward:
            reward_obj = position_reward.reward
        elif 'id' in reward:
            reward_obj = Reward.query.get(reward.get('id'))

        if not position_reward:
            position_reward = PositionReward()
            position_reward.top_id = self.id
            position_reward.position = pos
            if pos == 0:
                position_reward.spe_name = reward.get('position_name')

        if not reward_obj:
            reward_obj = Reward()

        reward_obj.set_data(reward, ['kind', 'amount', 'value', 'message'])
        position_reward.reward = reward_obj
        reward_obj.save(False)
        position_reward.reward_id = reward_obj.id
        position_reward.save()

    def set_rewards(self, rewards):
        if not rewards:
            return
        for k, rew in rewards.items():
            self.set_reward(k, rew)

    def serialize(self):
        return {
            'id':              self.id,
            'name':            self.name,
            'repeat':          self.repeat,
            'description':     self.description,
            'for_channels':    self.for_channels,
            'duration':        self.duration,
            'frequency_value': self.frequency_value,
            'frequency_type':  self.frequency_type,
            'rewards':         self.rewards,
            'duration_kind':   self.duration_kind,
            'pinned':          self.pinned,
        }


class Vote(db.Model):
    __tablename__ = 'votes'

    UP = 1
    DOWN = -1
    NEUTRAL = 0
    ALLOWED_VOTE = [UP, DOWN, NEUTRAL]

    video_id = db.Column(
        db.Integer,
        db.ForeignKey('videos.id', onupdate='CASCADE', ondelete='RESTRICT'),
        primary_key=True, index=True
    )
    user_id = db.Column(
        db.Integer,
        db.ForeignKey('users.id', onupdate='CASCADE', ondelete='RESTRICT'),
        primary_key=True, index=True
    )
    planning_id = db.Column(
        db.Integer,
        db.ForeignKey('plannings.id', onupdate='CASCADE', ondelete='RESTRICT'),
        primary_key=True, index=True
    )
    vote = db.Column(db.Integer, nullable=False)

    video = relationship('Video')
    user = relationship('User')
    planning = relationship('Planning')

    def send_mail(self, place):
        top_object = self.top_event.top.get_mail_object(place)
        if not top_object:
            self.send_award_mail = True
            self.save()
            return

        if self.user.deleted or not self.user.mail_verified:
            self.send_award_mail = True
            Slack.send(
                '#awards',
                'Could not send award mail to user: {} for position: {}.'
                'Either is email is not verified or the user deleted his account'.format(self.user.name, place)
            )
            self.save()
            return

        mail = Mail(
            to_addr=self.user.email,
            from_name='YourTalent',
            # from_addr='no-reply@your-talent.fr',
            from_addr='titouanfreville@gmail.com',
        )
        mail.use_template(
            'awarded.html', name=self.user.name,
            video={'name': self.video.name}, top=top_object
        )
        mail.send()
        self.send_award_mail = True
        self.save()

    def serialize(self):
        return {
            'video_id':     self.video_id,
            'user_id':      self.user_id,
            'top_event_id': self.top_event_id,
            'vote':         self.vote,
            'video':        self.video.serialize() if self.video else {},
            'user':         self.user.serialize() if self.user else {},
            'top_event':    self.top_event.serialize() if self.top_event else {},
        }


@event.listens_for(Vote.vote, 'set', propagate=True, retval=True)
def _before_set_vote(_target, value, _old, _initiator):
    if TextHelper.is_int(value) and int(value) in Vote.ALLOWED_VOTE:
        return int(value)
    return 0


Video.votes = relationship('Vote')


class Ranking(db.Model):
    __tablename__ = 'rankings'

    id = db.Column(db.Integer, primary_key=True)
    video_id = db.Column(
        db.Integer,
        db.ForeignKey('videos.id', onupdate='CASCADE', ondelete='RESTRICT'),
        primary_key=True
    )
    top_id = db.Column(
        db.Integer,
        db.ForeignKey('tops.id', onupdate='CASCADE', ondelete='RESTRICT'),
        primary_key=True
    )
    channel_id = db.Column(
        db.Integer,
        db.ForeignKey('channels.id', onupdate='CASCADE', ondelete='RESTRICT'),
        primary_key=True
    )
    send_award_mail = db.Column(db.Boolean, nullable=False, default=False)
    rank = db.Column(db.Integer, nullable=False, index=True)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    video = relationship(Video, uselist=False)

    @staticmethod
    def _next_key():
        key = AlchemyHelper.execute(
            """
                SELECT id
                FROM rankings
                ORDER BY id desc
                LIMIT 1
            """
        ).first()
        return int(key) + 1

    @staticmethod
    def get_ranking_for_id(rank_id, channel_id=1, max_rank=3):
        def get_week_month(created_at):
            created_at -= timedelta(days=5)  # goes back 5 days to be in month/week related to vote
            return created_at.isocalendar()[1], created_at.month, created_at.year,

        rankings = Ranking.query.filter_by(
            id=rank_id, channel_id=channel_id
        ).filter(
            Ranking.rank <= max_rank
        ).order_by(Ranking.rank).all()
        res = None
        for r in rankings:
            if not res:
                w, m, y = get_week_month(r.created_at)
                res = {
                    'top_id':     r.top_id,
                    'created_at': r.created_at,
                    'week':       w,
                    'month':      m,
                    'year':       y,
                    'videos':     {}
                }
            if r.rank not in res['videos']:
                res['videos'][r.rank] = []

            res['videos'][r.rank].append(r.video.serialize())
        return res

    @staticmethod
    def set_ranking(planning, rank_on):
        # orders = {
        #     'like':  lambda video: isinstance(video, Video) and video.nb_like,
        #     'score': lambda video: isinstance(video, Video) and video.score,
        # }
        if not isinstance(planning, Planning):
            planning = Planning.get(planning)

        if not planning:
            return False

        if planning.end_at > datetime.utcnow():
            return False

        videos = Video.query.outerjoin(
            Video.planning
        ).filter(
            Planning.id == planning.id,
            Video.status.in_([Video.PLANNED, Video.TOPPED])
        ).all()

        videos.sort(key=rank_on, reverse=True)
        next_id = Ranking._next_key()
        for ind, v in enumerate(videos):
            ranking = Ranking()
            ranking.id = next_id
            ranking.top_id = planning.top_id
            ranking.date = planning.end_at
            ranking.video_id = v.id
            ranking.rank = ind
            if not ranking.save(commit=False):
                return False
        try:
            db.session.commit()  # commit all result to DB
            return True
        except Exception:
            db.session.rollback()
            return False
