# coding: utf-8
from datetime import datetime, timedelta

from sqlalchemy import event
from sqlalchemy.ext.hybrid import hybrid_property

from your_talent.classes import Slack
from your_talent.errors import ForbiddenError, InsertError, MissingFieldError
from your_talent.helpers import DateHelper, error_light, ok_light, TextHelper
from your_talent.models import db, File, get_from, relationship
from your_talent.models.settings import Setting


class Tag(db.Model):
    __tablename__ = 'tags'

    # Table definition
    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)

    name = db.Column(db.String(155), nullable=True, unique=True)

    videos = relationship('HasTag')

    @staticmethod
    def get(tag_id):
        if TextHelper.is_int(tag_id):
            return Tag.query.get(tag_id)
        return Tag.query.filter_by(name=tag_id).first()

    def serialize(self):
        return {
            'id':     self.id,
            'name':   self.name,
            'videos': [v.serialize() for v in self.videos] if self.videos else []
        }


class HasTag(db.Model):
    __tablename__ = 'has_tags'

    # Table definition
    id_tag = db.Column(
        db.Integer,
        db.ForeignKey('tags.id', ondelete='CASCADE', onupdate='CASCADE'),
        nullable=False, primary_key=True
    )
    id_video = db.Column(
        db.Integer,
        db.ForeignKey('videos.id', ondelete='CASCADE', onupdate='CASCADE'),
        nullable=False, primary_key=True
    )

    tag = relationship('Tag')
    video = relationship('Video')

    def serialize(self):
        return {
            'id_tag':   self.id_tag,
            'id_video': self.id_video,
            'tag':      self.tag.serialize() if self.tag else {},
            'video':    self.video.serialize() if self.video else {},
        }


class Channel(db.Model):
    __tablename__ = 'channels'

    DEFAULT_NB_REQUIRED_VALIDATION = 1

    # Table definition
    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)

    name = db.Column(db.String(155), nullable=True, unique=True)
    description = db.Column(db.String(255), nullable=False, unique=True)
    age_restricted = db.Column(db.Boolean, nullable=True, unique=True, default=False)
    nb_validation_required = db.Column(db.Integer, nullable=True, default=DEFAULT_NB_REQUIRED_VALIDATION)
    required_age = db.Column(db.Integer, nullable=True, unique=False)

    videos = relationship(
        'Video', secondary='planned_videos', order_by='and_(PlannedVideo.diffusion_at.desc())'
    )
    plannings = relationship('Planning', secondary='planned_videos')

    @staticmethod
    def get(channel_id):
        if TextHelper.is_int(channel_id):
            return Channel.query.get(channel_id)
        return Channel.query.filter_by(name=channel_id).first()

    def serialize(self):
        return {
            'id':             self.id,
            'name':           self.name,
            'description':    self.description,
            'age_restricted': self.age_restricted,
            'required_age':   self.required_age,
            'videos':         [v.serialize() for v in self.videos] if self.videos else [],
            'plannings':      [p.serialize() for p in self.plannings] if self.plannings else []
        }


@event.listens_for(Channel.age_restricted, 'set', propagate=True, retval=True)
def before_set_age_restricted(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


@event.listens_for(Channel.required_age, 'set', propagate=True, retval=True)
def before_set_age_restricted(_target, value, _old, _initiator):
    if not TextHelper.is_number(value):
        raise ValueError()
    return float(value)


class PlanningSetting(db.Model):
    __tablename__ = 'planning_settings'

    LIVE = 0
    FREE = 1

    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)

    name = db.Column(db.String(155), nullable=True, unique=True)

    time_shift = db.Column(db.Integer, nullable=False, default=5)
    last_vote_duration = db.Column(db.Integer, nullable=False, default=60)
    summary_duration = db.Column(db.Integer, nullable=False, default=300)
    transition_duration = db.Column(db.Integer, nullable=False, default=3)

    is_fake_live = db.Column(db.Boolean, nullable=False, default=False)
    type = db.Column(db.Integer, nullable=False, default=FREE)

    @staticmethod
    def get(setting_id):
        if TextHelper.is_int(setting_id):
            return PlanningSetting.query.get(setting_id)
        return PlanningSetting.query.filter_by(name=setting_id).first()

    def serialize(self):
        return {
            'id':                  self.id,
            'name':                self.name,
            'time_shift':          self.time_shift,
            'last_vote_duration':  self.last_vote_duration,
            'summary_duration':    self.summary_duration,
            'transition_duration': self.transition_duration,
            'is_fake_live':        self.is_fake_live,
        }


class Planning(db.Model):
    __tablename__ = 'plannings'

    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)

    start_at = db.Column(db.DateTime, nullable=False)
    setting_id = db.Column(
        db.Integer,
        db.ForeignKey('planning_settings.id', ondelete='CASCADE', onupdate='CASCADE'),
        nullable=False, index=True
    )
    top_id = db.Column(
        db.Integer,
        db.ForeignKey('tops.id', ondelete='RESTRICT', onupdate='CASCADE'),
        nullable=False, index=True
    )
    channel_id = db.Column(
        db.Integer,
        db.ForeignKey('channels.id', ondelete='CASCADE', onupdate='CASCADE'),
        nullable=False, index=True, default=1
    )
    end_at = db.Column(db.DateTime, nullable=False)

    setting = relationship('PlanningSetting', single_parent=True, uselist=False)
    channel = relationship('Channel', secondary='planned_videos', single_parent=True, uselist=False)
    top = relationship('Top', single_parent=True, uselist=False)

    @property
    def time_shift(self):
        return self.setting.time_shift

    @property
    def last_vote_duration(self):
        return self.setting.last_vote_duration

    @property
    def summary_duration(self):
        return self.setting.summary_duration

    @property
    def transition_duration(self):
        return self.setting.transition_duration

    @property
    def type(self):
        return self.setting.type

    @property
    def is_fake_live(self):
        return self.setting.is_fake_live

    @staticmethod
    def get(planning_id):
        if TextHelper.is_int(planning_id):
            return Planning.query.get(planning_id)
        return Planning.query.filter_by(planning_id).first()

    def serialize(self):
        return {
            'id':                  self.id,
            'start_at':            self.start_at,
            'end_at':              self.end_at,
            'setting_id':          self.setting_id,
            'time_shift':          self.time_shift,
            'last_vote_duration':  self.last_vote_duration,
            'summary_duration':    self.summary_duration,
            'transition_duration': self.transition_duration,
            'is_fake_live':        self.is_fake_live,
            'type':                self.type,
            'top_id':              self.top_id,
        }


class Video(db.Model):
    __tablename__ = 'videos'

    WAITING = 0
    VALIDATING = 1
    VALID = 2
    PLANNED = 3
    TOPPED = 4
    REFUSED = 5
    DELETED = 6

    _status_conv = {
        WAITING:      'waiting',
        WAITING:      'waiting',
        VALIDATING:   'validating',
        VALID:        'valid',
        REFUSED:      'refused',
        PLANNED:      'planned',
        TOPPED:       'topped',
        DELETED:      'deleted',
        'waiting':    WAITING,
        'validating': VALIDATING,
        'valid':      VALID,
        'refused':    REFUSED,
        'planned':    PLANNED,
        'topped':     TOPPED,
        'deleted':    DELETED,
    }

    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)

    name = db.Column(db.String(555), nullable=True, unique=False)
    category = db.Column(db.String(255), nullable=True, unique=False)
    comment = db.Column(db.Text, nullable=True)
    file_key = db.Column(db.String(255), nullable=True, unique=True)
    youtube_key = db.Column(db.String(255), nullable=True, unique=True)
    sponsored = db.Column(db.Boolean, nullable=False, default=False)

    posted_by = db.Column(
        db.Integer,
        db.ForeignKey('users.id', ondelete='RESTRICT', onupdate='CASCADE'),
        nullable=False, index=True
    )
    posted_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    _status = db.Column(db.Integer, name='status', nullable=False, default=WAITING)

    poster = relationship('User', primaryjoin='User.id == Video.posted_by')
    tags = relationship('HasTag')
    planned = relationship(
        'PlannedVideo',
        single_parent=True, uselist=False
    )
    channel = relationship(
        'Channel', secondary='planned_videos',
        single_parent=True, uselist=False
    )
    planning = relationship(
        'Planning', secondary='planned_videos',
        uselist=False, order_by=Planning.id.desc()
    )
    validations = relationship('ValidationInfo')
    votes = None

    @staticmethod
    def get(video_id):
        if TextHelper.is_int(video_id):
            return Video.query.get(video_id)
        return Video.query.filter_by(name=video_id).first()

    @property
    def diffused_at(self):
        return get_from(self.planned, 'diffusion_at')

    @hybrid_property
    def status(self):
        return self._status_conv.get(self._status, 'waiting')

    @status.setter
    def status(self, val):
        val = self._status_conv.get(val, 0)
        if val < self._status:
            raise ForbiddenError('Cannot downgrade status')
        self._status = val

    @status.expression
    def status(self):
        return self._status

    @property
    def republish_delay(self):
        video = Video.query.filter_by(posted_by=self.posted_by).order_by(Video.posted_at).first()
        if not video:
            return 0
        return max(
            (
                    video.posted_at -
                    (datetime.utcnow() - timedelta(days=int(Setting.get(
                        'video_republish_delay', 8 * 7, TextHelper.is_int
                    ))))
            ).days,
            0
        )

    @property
    def score(self):
        s = 0
        nb_vote = 0
        c_date = datetime.utcnow()
        votes = self.votes
        if self.planning and (self.planning.start_at <= c_date <= self.planning.end_at):
            votes = filter(lambda x: x.planning_id == self.planning.id, votes)
        for v in votes:
            nb_vote += 1
            s += v.vote
        return (s / nb_vote / 2 + 0.5) * 100 if nb_vote > 0 else 50

    @property
    def nb_like(self):
        res = 0
        c_date = datetime.utcnow()
        votes = self.votes
        if self.planning and self.planning.start_at <= c_date <= self.planning.end_at:
            votes = filter(lambda x: x.planning_id == self.planning.id, votes)
        for v in votes:
            if v.vote > 0:
                res += 1
        return res

    @property
    def download_infos(self):
        try:
            if self.file_key is not None:
                file = File.query.filter_by(key=self.file_key).first()
                if file:
                    from your_talent.classes import Registry
                    return file.get_presigned_download(
                        Registry.registered('current-user-id'),
                        Registry.registered('current-role-name')
                    )
        except ForbiddenError:
            return None

    @staticmethod
    def conv_status(key):
        return Video._status_conv.get(key)

    def _check_validation(self):
        return all(
            [v.status == 'valid' for v in self.validations]
        ), any(
            [v.status == 'refused' for v in self.validations]
        ), sum([1 if v.status == 'valid' else 0 for v in self.validations])

    def plan_video(self, channel, planning, plan_at):
        planned_video = PlannedVideo.query.filter_by(
            channel_id=channel.id, planning_id=planning.id, video_id=self.id
        ).first()

        if not planned_video:
            planned_video = PlannedVideo()
            planned_video.planning = planning
            planned_video.video = self
            planned_video.channel = channel

        if planning.type == PlanningSetting.FREE:
            planned_video.diffusion_at = plan_at

        if self.status == 'valid':
            self.status = 'planned'

        return planned_video.save()

    def validate_video(self, valid, user, reason=None):
        """
        Validate a video

        :param valid:
        :type valid: bool
        :param reason:
        :type reason: str
        :param user:
        :type user: User
        :return:
        :rtype: bool
        """
        if self.status == 'waiting':
            self.status = 'validating'

        validation = ValidationInfo.query.filter_by(video_id=self.id, user_id=user.id).first()
        if not validation:
            validation = ValidationInfo.request_validation(user.id, self.id)
            validation.save(commit=False)

        ok, valid = validation.validate(valid, reason, True)
        if not ok:
            raise InsertError('ValidationInfos', 'Validation could not be saved')

        self.link_to_session()
        self.refresh()

        all_valid, refused, nb_valid = self._check_validation()

        if refused:
            self.status = 'refused'
        elif (
                valid and all_valid and nb_valid + 1 > get_from(
            self.channel, 'nb_validation_required', Setting.get('minimal_validation_required', 1)
        )):
            self.status = 'valid'

        if self.status == 'valid' and self.planned is not None:
            self.status = 'planned'

        return self.save()

    def serialize(self, public=True, admin=False):
        poster_serial = self.poster.serialize(public) if self.poster else {}
        data = {
            'id':              self.id,
            'name':            self.name,
            'category':        self.category,
            'file_key':        self.file_key,
            'posted_by':       self.posted_by,
            'posted_at':       self.posted_at,
            'status':          self.status,
            'comment':         self.comment,
            'youtube_key':     self.youtube_key,
            'poster_name':     poster_serial.get('user_name', {}),
            'poster':          poster_serial,
            'channel':         get_from(self.channel, 'name'),
            'age_restricted':  get_from(self.channel, 'age_restricted'),
            'required_age':    get_from(self.channel, 'required_age'),
            'sponsored':       self.sponsored,
            'planned_at':      get_from(self.planned, 'diffusion_at', get_from(self.planning, 'start_at')),
            'republish_delay': self.republish_delay,
            'score':           self.score,
            'nb_like':         self.nb_like,
            'download_infos':  self.download_infos,
        }
        if admin:
            data['planning'] = self.planning.serialize() if self.planning else None

        return data


@event.listens_for(Video.sponsored, 'set', propagate=True, retval=True)
def before_set_age_restricted(_target, value, _old, _initiator):
    return TextHelper.to_bool(value)


class PlannedVideo(db.Model):
    __tablename__ = 'planned_videos'

    channel_id = db.Column(
        db.Integer,
        db.ForeignKey('channels.id', ondelete='CASCADE', onupdate='CASCADE'),
        nullable=False, primary_key=True, index=True
    )
    planning_id = db.Column(
        db.Integer,
        db.ForeignKey('plannings.id', ondelete='CASCADE', onupdate='CASCADE'),
        nullable=False, primary_key=True, index=True
    )
    video_id = db.Column(
        db.Integer,
        db.ForeignKey('videos.id', ondelete='CASCADE', onupdate='CASCADE'),
        nullable=False, primary_key=True, index=True
    )
    diffusion_at = db.Column(db.DateTime)

    video = relationship('Video')
    channel = relationship('Channel')
    planning = relationship('Planning')


class ValidationInfo(db.Model):
    __tablename__ = 'validation_infos'

    REFUSED = 0
    VALID = 1
    WAITING = 2

    _status_conv = {
        REFUSED:   'refused',
        VALID:     'valid',
        WAITING:   'waiting',
        'refused': REFUSED,
        'valid':   VALID,
        'waiting': WAITING
    }

    video_id = db.Column(
        db.Integer,
        db.ForeignKey('videos.id', ondelete='CASCADE', onupdate='CASCADE'),
        nullable=False, primary_key=True, unique=False
    )
    user_id = db.Column(
        db.Integer,
        db.ForeignKey('users.id', ondelete='CASCADE', onupdate='CASCADE'),
        nullable=False, primary_key=True, unique=False
    )
    _status = db.Column(db.Integer, name='status', nullable=False, default=WAITING)
    message = db.Column(db.Text, nullable=True)

    video = relationship('Video')

    @hybrid_property
    def status(self):
        return self._status_conv.get(self._status, 'waiting')

    @status.setter
    def status(self, val):
        if self._status == self.WAITING:
            self._status = self._status_conv.get(val, self.WAITING)

    @status.expression
    def status(self):
        return self._status

    @staticmethod
    def request_validation(user_id, video_id):
        val = ValidationInfo()
        val.user_id = user_id
        val.video_id = video_id
        val._status = ValidationInfo.WAITING
        return val

    def validate(self, val, reason=None, slack=True):
        """

        :param val:
        :type val: bool
        :param reason:
        :param slack:
        :return:
        """

        def to_slack(valid, video, send, r=None):
            if not send:
                return

            fields = [
                {
                    'title': 'Video',
                    'value': '{}'.format(video.name),
                },
                {
                    'title': 'Channel',
                    'value': '{}'.format(get_from(video.channel, 'name')),
                },
                {
                    'title': 'Plannifiée pour',
                    'value': '{}'.format(DateHelper.date_to_string(
                        get_from(video.planning, 'start_at'),
                        output_format='%d-%m-%Y %H:%M',
                        tz='Europe/Paris'
                    )),
                    'short': True
                }
            ]

            if r is not None:
                fields.append({
                    'title': 'Motif de refus',
                    'value': '{}'.format(r)
                })

            Slack.attachment(
                '#new_videos',
                fallback='New videos',
                color=ok_light if valid else error_light,
                title='Vidéo validée' if valid else 'Vidéo refusée',
                text='Vidéo validée :v:' if valid else 'Vidéo refusée :x:',
                footer='Video: {}'.format(TextHelper.url_from_config('bo', '/videos?id={}', video.id)),
                fields=fields
            )

        if not val and not reason:
            raise MissingFieldError('Reason', 'Reason is required when refusing a video')

        if not val:
            self._status = self.REFUSED
            self.message = reason
        else:
            self.message = None
            self._status = self.VALID
        res = self.save()
        to_slack(val, self.video, slack, self.message)
        self.link_to_session()
        return res, self._status == self.VALID
