# coding: utf-8
from celery.schedules import crontab

from your_talent import YourTalent
from your_talent.cron.clean_token import clean_token
from your_talent.cron.manage_top import manage_top


@YourTalent.celery.on_after_configure.connect
def setup_periodic_task(sender, **_kwargs):
    sender.add_periodic_task(
        crontab(hour='0', minute='0', day_of_week='*'),
        clean_token.s()
    )
    sender.add_periodic_task(
        crontab(hour='0', minute='30', day_of_week='*'),
        manage_top.s()
    )
