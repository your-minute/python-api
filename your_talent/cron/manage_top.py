from celery import chain

from your_talent import YourTalent
from your_talent.classes import Slack
from your_talent.helpers import AlchemyHelper, CeleryErrorManager, info_light
from your_talent.models import Ranking, Video


@YourTalent.celery.task(name='top.manage', on_failure=CeleryErrorManager.error_handler)
def manage_top():
    chain(
        set_top_videos.s(),
        send_top_mails.s(),
        set_top_planning.s()
    )


@YourTalent.celery.task(name='top.set_videos', on_failure=CeleryErrorManager.error_handler)
def set_top_videos():
    def slack_ranking(top_id, fields):
        Slack.attachment(
            '#awards', color=info_light, fallback='#awards',
            title=':trophy: ranking :trophy:',
            message='Ranking for top: {}'.format(top_id),
            fields=fields,
        )

    def get_rank_value(rank):
        if rank == 1:
            return ':first_place_medal: 1'
        elif rank == 2:
            return ':second_place_medal: 2'
        elif rank == 3:
            return ':third_place_medal: 3'
        else:
            return str(rank)

    to_process = AlchemyHelper.execute(
        """
            SELECT
                v.id as video_id, vo.planning_id as planning_id, v.youtube_key as yt_key,
                coalesce(u.public_name, concat(u.first_name, u.last_name), u.name) as user,
                p.top_id as top_id, p.channel_id as channel_id, coalesce(sum(vo.vote),0) as vote
            from votes vo
            left join plannings p on vo.planning_id = p.id
            left join videos v on vo.video_id = v.id and v.status not in :forbidden_status
            left join users u on v.posted_by = u.id
            left join rankings r on v.id = r.video_id and p.top_id = r.top_id
            where date(p.end_at) < date(now()) and r.id is null
            group by vo.planning_id, v.id
            order by vo.planning_id, vote desc
        """,
        forbidden_status=(Video.DELETED, Video.VALID, Video.VALIDATING, Video.REFUSED, Video.WAITING)
    )

    last_rank = Ranking.query.order_by(Ranking.id.desc()).limit(1).first()
    last_rank_id = last_rank.id if last_rank else 0
    last_planning_id = None
    last_top_id = None
    fields = []
    for e in to_process:
        if not last_planning_id or last_planning_id != e.planning_id:
            if last_planning_id:
                slack_ranking(last_top_id, fields)
            last_rank_id += 1
            last_planning_id = e.planning_id
            last_top_id = e.top_id
            last_rank = 0
            fields = [
                {'title': 'Planning', 'value': last_planning_id, 'short': True},
                {'title': 'Top', 'value': e.top_id, 'short': True}
            ]
        last_rank += 1
        new_rank = Ranking()
        new_rank.id = last_rank_id
        new_rank.channel_id = e.channel_id
        new_rank.video_id = e.video_id
        new_rank.rank = last_rank
        new_rank.top_id = e.top_id
        new_rank.save()
        fields.extend([
            {'title': 'Rank', 'value': '{}'.format(get_rank_value(last_rank)), 'short': True},
            {'title': 'Video', 'value': '{}'.format(e.video_id), 'short': True},
            {'title': 'YouTube', 'value': '{}'.format(e.yt_key), 'short': True},
            {'title': 'User', 'value': '{}'.format(e.user), 'short': True},
            {'title': 'Nb like', 'value': '{}'.format(e.vote), 'short': True},
            {'title': '', 'value': '', 'short': True}
        ])

    slack_ranking(last_top_id, fields)


@YourTalent.celery.task(name='top.send_mails', on_failure=CeleryErrorManager.error_handler)
def send_top_mails(_):
    pass


@YourTalent.celery.task(name='top.set_top_planning', on_failure=CeleryErrorManager.error_handler)
def set_top_planning(_):
    pass
