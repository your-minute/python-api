from datetime import datetime

from sqlalchemy import func, text

from your_talent import YourTalent
from your_talent.helpers import CeleryErrorManager
from your_talent.models import OAuth2Token


@YourTalent.celery.task(name='clean.token', on_failure=CeleryErrorManager.error_handler)
def clean_token():
    revoked = OAuth2Token.query.filter_by(revoked=1)
    for t in revoked:
        t.delete()

    expired = OAuth2Token.query.filter(
        func.date_add(
            OAuth2Token.created_at, text('interval {} second'.format(OAuth2Token.expires_in.expression))
        ) < datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    )

    for t in expired:
        t.delete()
