import importlib
import logging
import urllib.parse
from http import HTTPStatus

from flask import g, jsonify, request, url_for
from flask_migrate import Migrate

from your_talent.models import db


class YourTalent:
    app = None
    celery = None
    migrate = None
    LOG_LEVEL = logging.ERROR
    _METHODS = ['get', 'post', 'delete', 'put']

    @staticmethod
    def init_db(your_talent_app, config):
        """
        Init SQLAlchemy DB
        """
        your_talent_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        your_talent_app.config['SQLALCHEMY_DATABASE_URI'] = '{}://{}:{}@{}/{}'.format(
            config.get('database', 'connector'),
            config.get('database', 'user'),
            config.get('database', 'password'),
            config.get('database', 'host'),
            config.get('database', 'database')
        )
        db.init_app(your_talent_app)
        YourTalent.migrate = Migrate(your_talent_app, db)

    @staticmethod
    def register(your_talent_app, config, celery, worker=False):
        # if not db:
        YourTalent.init_db(your_talent_app, config)

        # mandatory to load observers
        from your_talent.errors import BaseError, ServerError, CustomError
        from your_talent.classes import Slack
        from your_talent.classes import Registry

        # Init logging
        YourTalent.app = your_talent_app
        YourTalent.celery = celery
        YourTalent.celery.conf.update(YourTalent.app.config)
        # Registering config
        Registry.register('config', config, False)
        YourTalent.LOG_LEVEL = config.get('log', 'level')

        if worker:
            YourTalent.app.config['CELERY_BROKER_URL'] = config.get('celery', 'broker_url')
            YourTalent.app.config['CELERY_RESULT_BACKEND'] = config.get('celery', 'broker_url')
        # Initialising Slack object
        Slack.init(config)

        @your_talent_app.errorhandler(BaseError)
        def handle_invalid_usage(error):
            """
            Register error handler
            Register error 500 handler
            """
            error.to_slack()
            return error.to_json(), error.status

        @your_talent_app.errorhandler(500)
        def internal_error(error):
            server_error = ServerError(str(error))
            server_error.to_slack()
            return server_error.to_json(), server_error.status

        @your_talent_app.errorhandler(404)
        def w404_error(e):
            server_error = CustomError(e.description, 'NotFound', 404)
            server_error.to_slack()
            return server_error.to_json(), server_error.status

        @your_talent_app.errorhandler(429)
        def w404_error(error):
            e = CustomError('Too Many Requests.' + error.description, 'TooManyRequest', 429, 'Too many request')
            e.to_slack()
            return e.to_json(), e.status

        @your_talent_app.teardown_appcontext
        def close_db(_):
            """
            Close DB connexion at end of context
            """
            if hasattr(g, 'database'):
                g.database.close()

        if not worker:
            with your_talent_app.app_context():
                logging.basicConfig()

        @your_talent_app.before_request
        def before_all():
            """
            Before all request, we'll check for OPTIONS requests
            Then define caller role if not OPTIONS request
            """
            if request.method == 'OPTIONS':
                return jsonify({}), HTTPStatus.OK.value
            else:
                # registry if workers
                Registry.register('workers', worker)

                app_id = request.headers.get('X-Application-Id', '-')
                app_version = request.headers.get('X-Application-Version', 'beta')
                tz = request.form.get('tz', request.args.get('tz', 'Europe/Paris'))

                Registry.register('current-request-path', request.path)
                Registry.register('app-id', app_id)
                Registry.register('app-version', app_version)
                Registry.register('tz', tz)

        @your_talent_app.after_request
        def after_all(result):
            # cleaning Registry
            Registry.remove('current-user-api-token')
            Registry.remove('current-admin')
            Registry.remove('current-customer')
            Registry.remove('current-drive')
            Registry.remove('app-id')
            Registry.remove('device-token')
            Registry.remove('current-request-path')
            Registry.remove('current-user-id')
            Registry.remove('current-role')
            return result

        @your_talent_app.route('/', methods=['GET'])
        def main_route():
            """
            Default Routes
            """
            return jsonify({'hello': 'world new test'})

        @your_talent_app.route('/ping', methods=['GET'])
        def ping_pong_route():
            """
            Default Routes
            """
            return jsonify({'pong': 'No db yet'})

        @your_talent_app.route('/routes', methods=['GET'])
        def list_routes():
            import urllib
            output = {}
            for rule in your_talent_app.url_map.iter_rules():
                options = {}
                for arg in rule.arguments:
                    options[arg] = '[{0}]'.format(arg)
                methods = set(rule.methods)
                if 'OPTIONS' in methods:
                    methods.remove('OPTIONS')
                if 'HEAD' in methods:
                    methods.remove('HEAD')
                methods = ','.join(methods)
                url = url_for(rule.endpoint, **options)
                k = urllib.parse.unquote('[{}] {}'.format(methods, url))
                output[k] = rule.endpoint
            return jsonify(output), 200

        def load_modules(base_path, api_module, parent_module=None):
            """
            Helper to load route files
            """
            from your_talent.helpers.io import IO
            path = base_path + '/' + api_module
            py_path = path.replace('/', '.')
            methods = IO.get_files(path, 'py')
            module_key = api_module
            if parent_module is not None:
                module_key = parent_module + '_' + api_module
            url_prefix = '/yourtalent/' + module_key.replace('_', '/')
            for method in YourTalent._METHODS:
                if method + '.py' in methods:
                    mo = importlib.import_module(py_path + '.' + method, module_key + '_' + method)
                    your_talent_app.register_blueprint(getattr(mo, module_key + '_' + method), url_prefix=url_prefix)
            for sub in IO.get_sub_dir(path):
                load_modules(path, sub, module_key)

        def init_base_modules():
            from your_talent.helpers.io import IO
            for base_modules in IO.get_sub_dir(main_path):
                load_modules(main_path, base_modules)

        # Loading modules
        main_path = 'your_talent/routes'
        if not worker:
            init_base_modules()

        from your_talent.helpers import PasswordHelper, TextHelper
        from your_talent.helpers.s3 import S3Helper
        PasswordHelper.init_config()
        S3Helper.init_config()
        TextHelper.init_config()
        return your_talent_app
