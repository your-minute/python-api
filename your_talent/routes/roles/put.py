from flask import Blueprint, request, jsonify

from your_talent import YourTalent
from your_talent.decorators import requires_auth, requires_rights
from your_talent.errors import IDNotFoundError, UpdateError
from your_talent.models import Role


roles_put = Blueprint('roles_put', __name__)


@roles_put.route('<role_id>', methods=['PUT'])
# @requires_auth()
# @requires_rights('manage_role')
def role_update(role_id):
    role = Role.query.get(role_id)
    if not role:
        raise IDNotFoundError()

    role.set_data(
        request.form,
        [
            'name', 'manage_user', 'manage_video', 'manage_comment', 'manage_avatar', 'manage_channel', 'manage_reward',
            'manage_role', 'manage_top', 'manage_calendar', 'manage_setting', 'validate_video', 'moderate_comment',
        ]
    )

    if not role.save():
        raise UpdateError()
    
    return jsonify(role.serialize())
