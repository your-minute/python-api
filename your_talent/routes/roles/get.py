from flask import Blueprint, jsonify

from your_talent.decorators import add_pagination, requires_auth, requires_rights
from your_talent.errors import IDNotFoundError
from your_talent.models import Role


roles_get = Blueprint('roles_get', __name__)


@roles_get.route('', methods=['GET'])
@requires_auth()
@requires_rights('manage_role')
@add_pagination()
def get_all(page, per_page):
    roles, count = Role().paginated_query(page, per_page)
    res = []
    for r in roles:
        res.append(r.serialize(True))
    return res, count


@roles_get.route('<role_id>', methods=['GET'])
@requires_auth()
def get_one(role_id):
    role = Role.query.get(role_id)
    if not role:
        raise IDNotFoundError()

    return jsonify(role.serialize(True))
