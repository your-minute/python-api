from flask import Blueprint, jsonify

from your_talent.decorators import requires_auth, requires_rights
from your_talent.errors import IDNotFoundError, DeleteError
from your_talent.models import Role


roles_delete = Blueprint('roles_delete', __name__)


@roles_delete.route('<role_id>', methods=['DELETE'])
# @requires_auth()
# @requires_rights('manage_role')
def role_update(role_id):
    role = Role.query.get(role_id)
    if not role:
        raise IDNotFoundError()

    if not role.delete():
        raise DeleteError()
    return jsonify(role.serialize())
