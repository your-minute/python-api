from flask import Blueprint, jsonify, request

from your_talent.decorators import mandatory_fields, requires_auth, requires_rights
from your_talent.errors import InsertError
from your_talent.models import Role


roles_post = Blueprint('roles_post', __name__)


@roles_post.route('init', methods=['POST'])
def init_roles():
    roles = Role.query.all()
    if roles:
        return jsonify('Already initialized')

    admin = Role()
    admin.set_data(
        {
            'name':             'admin',
            'manage_user':      True,
            'manage_video':     True,
            'manage_comment':   True,
            'manage_avatar':    True,
            'manage_channel':   True,
            'manage_reward':    True,
            'manage_role':      True,
            'manage_top':       True,
            'manage_calendar':  True,
            'manage_setting':   True,
            'validate_video':   True,
            'moderate_comment': True,
        },
        [
            'name',
            'manage_user', 'manage_video', 'manage_comment', 'manage_avatar', 'manage_channel', 'manage_reward',
            'manage_role', 'manage_top', 'manage_calendar', 'manage_setting', 'validate_video', 'moderate_comment',
        ]
    )
    if not admin.save():
        raise InsertError(details='Could not insert role admin')

    guest = Role()
    guest.set_data(
        {
            'name':             'guest',
            'manage_user':      False,
            'manage_video':     False,
            'manage_comment':   False,
            'manage_avatar':    False,
            'manage_channel':   False,
            'manage_reward':    False,
            'manage_role':      False,
            'manage_top':       False,
            'manage_calendar':  False,
            'manage_setting':   False,
            'validate_video':   False,
            'moderate_comment': False,
        },
        [
            'name',
            'manage_user', 'manage_video', 'manage_comment', 'manage_avatar', 'manage_channel', 'manage_reward',
            'manage_role', 'manage_top', 'manage_calendar', 'manage_setting', 'validate_video', 'moderate_comment',
        ]
    )
    if not guest.save():
        raise InsertError(details='Could not insert role guest')
    return jsonify('Correctly initialize')


@roles_post.route('', methods=['POST'])
@mandatory_fields('name')
@requires_auth()
@requires_rights('manage_role')
def new_role():
    role = Role()
    role.set_data(
        request.form,
        [
            'name',
            'manage_user', 'manage_video', 'manage_comment', 'manage_avatar', 'manage_channel', 'manage_reward',
            'manage_role', 'manage_top', 'manage_calendar', 'manage_setting', 'validate_video', 'moderate_comment',
        ]
    )
    if not role.save():
        raise InsertError()
    return jsonify(role.serialize())
