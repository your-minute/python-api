from flask import Blueprint, jsonify, request

from your_talent.decorators import mandatory_fields
from your_talent.errors import InsertError
from your_talent.helpers import FormsHelper
from your_talent.models import Channel, PlanningSetting


channels_post = Blueprint('channels_post', __name__)


@channels_post.route('init', methods=['POST'])
def init_users():
    channels = Channel.query.all()
    if channels:
        return jsonify('Already initialized')

    default = Channel()
    default.set_data({
        'name':                   'global',
        'description':            'Channel for global purpose.',
        'age_restricted':         False,
        'nb_validation_required': 1,
    }, [
        'name', 'description', 'age_restricted', 'nb_validation_required'
    ])
    if not default.save(commit=False):
        raise InsertError(details='Could not insert default channel')

    nsfw = Channel()
    nsfw.set_data({
        'name':                   'nsfw',
        'description':            'Channel for adult purpose.',
        'age_restricted':         True,
        'required_age':           18,
        'nb_validation_required': 1,
    }, [
        'name', 'description', 'age_restricted', 'nb_validation_required', 'required_age'
    ])
    if not nsfw.save(commit=False):
        raise InsertError(details='Could not insert nsfw channel')

    default_planning_setting = PlanningSetting()
    default_planning_setting.set_data({
        'name': 'default',
        'type': PlanningSetting.FREE
    }, ['name', 'type'])
    if not default_planning_setting.save(commit=False):
        raise InsertError(details='Could not insert default planning setting')

    default_fake_planning_setting = PlanningSetting()
    default_fake_planning_setting.set_data({
        'name':         'fake_default',
        'is_fake_live': True,
        'type':         PlanningSetting.FREE
    }, ['name', 'type', 'is_fake_live'])

    if not default_fake_planning_setting.save():
        raise InsertError(details='Could not insert fake default planning setting')

    return jsonify('Init ok')


@channels_post.route('', methods=['POST'])
@mandatory_fields('name')
def post_one():
    channel = Channel()
    channel.set_data(request.form, ['name', 'description', 'required_age'])
    channel.age_restricted = FormsHelper.get_boolean(request.form, 'age_restricted')
    if not channel.save():  # pragma : no-cover
        raise InsertError()
    return jsonify(channel.serialize())
