from flask import Blueprint, jsonify

from your_talent.errors import IDNotFoundError, DeleteError
from your_talent.models import Channel


channels_delete = Blueprint('channels_delete', __name__)


@channels_delete.route('<channel_id>', methods=['DELETE'])
def delete_one(channel_id):
    channel = Channel().get(channel_id)
    if not channel:
        raise IDNotFoundError()
    if not channel.delete():  # pragma : no-cover
        raise DeleteError()
    return jsonify(channel.serialize())
