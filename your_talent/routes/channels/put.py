from flask import Blueprint, jsonify, request

from your_talent.errors import IDNotFoundError, UpdateError
from your_talent.helpers import FormsHelper
from your_talent.models import Channel


channels_put = Blueprint('channels_put', __name__)


@channels_put.route('<channel_id>', methods=['PUT'])
def put_one(channel_id):
    channel = Channel().get(channel_id)
    if not channel:
        raise IDNotFoundError()
    channel.set_data(request.form, ['name', 'description', 'required_age'])
    if request.form.get('age_restricted'):
        channel.age_restricted = FormsHelper.get_boolean(request.form, 'age_restricted')
    if not channel.save():  # pragma : no-cover
        raise UpdateError()
    return jsonify(channel.serialize())
