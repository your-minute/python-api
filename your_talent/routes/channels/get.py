from flask import Blueprint, jsonify, request
from sqlalchemy import and_, or_

from your_talent.decorators import add_pagination
from your_talent.errors import IDNotFoundError
from your_talent.helpers import FormsHelper
from your_talent.models import Channel, Planning, Video
from your_talent.models.settings import Setting
from your_talent.models.top import Top


channels_get = Blueprint('channels_get', __name__)

order_dict = {
    'name':           Channel.name,
    'description':    Channel.description,
    'required_age':   Channel.required_age,
    'age_restricted': Channel.age_restricted
}


@channels_get.route('', methods=['GET'])
@add_pagination()
def get_all(page, per_page):
    filter_by, filters = get_filters()
    order = FormsHelper.get_order_by(order_dict, request.args)
    channels, count = Channel().paginated_query(page, per_page, filter_by, filters, order)

    return [c.serialize() for c in channels], count


@channels_get.route('<channel_id>', methods=['GET'])
def get_one(channel_id):
    channel = Channel.get(channel_id)
    if not channel:
        raise IDNotFoundError()

    return jsonify(channel.serialize())


@channels_get.route('<channel_id>/videos', methods=['GET'])
def get_channel_videos(channel_id):
    date = FormsHelper.get_datetime(request.args, 'diffusion_at')
    planning_top_kind = request.args.get('planning_kind', Setting.get('default_top_kind', 'facebook_week'))

    planning = Planning.query.outerjoin(Planning.channel).outerjoin(Planning.top).filter(
        Channel.id == channel_id,
        Planning.start_at <= date.strftime('%Y-%m-%d %H:%M:%S'),
        Planning.end_at >= date.strftime('%Y-%m-%d %H:%M:%S'),
        Top.name == planning_top_kind,
    ).first()

    live = True

    if not planning:
        live = False
        planning = Planning.query.filter(
            Channel.id == channel_id,
            Planning.start_at > date.strftime('%Y-%m-%d %H:%M:%S'),
        ).outerjoin(
            Planning.channel,
            Planning.top
        ).order_by(Planning.start_at).limit(1).first()

    if not planning:
        return jsonify({})

    vids = []

    if planning and (live or ((planning.start_at - date).seconds // 60) <= Setting.get('video_plan_prompt', 10)):
        videos = Video.query.outerjoin(
            Video.planning
        ).filter(
            Planning.id == planning.id,
            Video._status.in_([Video.PLANNED, Video.TOPPED])
        ).order_by().all()

        for v in videos:
            vids.append(v.serialize())

    vids.sort(key=lambda x: x.get('planned_at'))

    return jsonify({
        'live':     live,
        'start_at': planning.start_at,
        'end_at':   planning.end_at,
        'videos':   vids
    })


@channels_get.route('<channel_id>/plannings', methods=['GET'])
def get_channel_planning(channel_id):
    channel = Channel.get(channel_id)
    if not channel:
        raise IDNotFoundError()

    ser = channel.serialize()

    return jsonify(ser['plannings'])


def get_filters():
    """
    Get filters fot channel

    :return: filters list and filter_by dict
    :rtype: tuple
    """
    filters, filter_by = [], {}

    name = request.args.get('name')
    if name:
        tmp = []
        for n in name.split(','):
            tmp.append(Channel.name.like('%{}%'.format(n)))
        filters.append(or_(*tmp))

    description = request.args.get('description')
    if description:
        tmp = []
        for d in description.split(','):
            tmp.append(Channel.description.like('%{}%'.format(d)))
        filters.append(or_(*tmp))

    if 'age_restricted' in request.args:
        filter_by['age_restricted'] = FormsHelper.get_boolean(request.args, 'age_restricted')

    required_age = request.args.get('required_age')
    if required_age:
        tmp = []
        for r in required_age.split(','):
            tmp.append(Channel.required_age == r)
            filters.append(
                or_(
                    and_(Channel.age_restricted.is_(True), or_(*tmp)),
                    Channel.age_restricted.is_(False)
                )
            )

    return filter_by, filters
