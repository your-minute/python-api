from http import HTTPStatus

from flask import Blueprint, jsonify, make_response, request

from your_talent.classes import Registry
from your_talent.decorators import requires_auth, requires_rights
from your_talent.errors import DeleteError, AuthError
from your_talent.models import User, OAuth2Token


users_delete = Blueprint('users_delete', __name__)


@users_delete.route('<user_id>', methods=['delete'])
@requires_auth()
@requires_rights('manage_user')
def delete_user(user_id):
    user = User.get(user_id)
    if not user.delete():
        raise DeleteError()

    return jsonify(user.serialize(public=False))


@users_delete.route('logout', methods=['DELETE'])
@requires_auth()
def logout():
    logged_user = Registry.registered('current-user-id')

    if not logged_user:
        raise AuthError()

    response = make_response(
        jsonify('ok'), HTTPStatus.OK.value
    )

    if 'token' in request.cookies:
        response.delete_cookie('token')

    tokens = OAuth2Token.query.filter_by(user_id=logged_user).all()
    for t in tokens:
        t.delete(commit=False)

    OAuth2Token.commit()
    return response
