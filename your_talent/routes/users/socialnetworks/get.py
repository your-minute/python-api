from flask import Blueprint, jsonify, request

from your_talent.classes import Logger
from your_talent.helpers import FormsHelper
from your_talent.models import SocialNetwork


users_socialnetworks_get = Blueprint('socialnetworks_get', __name__)
logger = Logger(__name__)


@users_socialnetworks_get.route('', methods=['GET'])
def get_sn():
    filter_by, filters = FormsHelper.filters_to_dict(
        {},
        ['id'],
        request.args,
    ), []

    networks = SocialNetwork.query.filter_by(**filter_by).filter(*filters).all()
    return jsonify([n.serialize() for n in networks])
