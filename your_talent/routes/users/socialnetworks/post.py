from flask import Blueprint, jsonify, request

from your_talent.classes import Logger
from your_talent.decorators import admin_only, mandatory_fields, requires_auth
from your_talent.errors import InsertError
from your_talent.models import SocialNetwork


users_socialnetworks_post = Blueprint('socialnetworks_post', __name__)
logger = Logger(__name__)


@users_socialnetworks_post.route('init', methods=['POST'])
def init_social_networks():
    users = SocialNetwork.query.all()
    if users:
        return jsonify('Already initialized')

    fb = SocialNetwork()
    fb.set_data(
        {
            'name':          'facebook',
            'logo':          'https://your-talent-public.s3.eu-west-3.amazonaws.com/images/fb.png',
            'base_link_url': 'https://www.facebook.com'
        },
        ['name', 'logo', 'base_link_url']
    )
    fb.save()

    yt = SocialNetwork()
    yt.set_data(
        {
            'name':          'youtube',
            'logo':          'https://your-talent-public.s3.eu-west-3.amazonaws.com/images/yt.png',
            'base_link_url': 'https://www.youtube.com/channel/'
        },
        ['name', 'logo', 'base_link_url']
    )
    yt.save()

    sc = SocialNetwork()
    sc.set_data(
        {
            'name':          'soundcloud',
            'logo':          'https://your-talent-public.s3.eu-west-3.amazonaws.com/images/sc.jpg',
            'base_link_url': 'https://soundcloud.com'
        },
        ['name', 'logo', 'base_link_url']
    )
    sc.save()

    insta = SocialNetwork()
    insta.set_data(
        {
            'name':          'instagram',
            'logo':          'https://your-talent-public.s3.eu-west-3.amazonaws.com/images/insta.png',
            'base_link_url': 'https://www.instagram.com/'
        },
        ['name', 'logo', 'base_link_url']
    )
    insta.save()
    return jsonify('Ok'), 201


@users_socialnetworks_post.route('', methods=['POST'])
@requires_auth()
@admin_only()
@mandatory_fields('name', 'logo')
def new_network():
    sn = SocialNetwork()
    sn.set_data(
        request.form,
        ['name', 'logo', 'base_link_url']
    )
    if not sn.save():
        raise InsertError()
    return jsonify(sn.serialize()), 201
