from flask import Blueprint, jsonify, request

from your_talent.classes import Logger
from your_talent.decorators import admin_only, requires_auth
from your_talent.errors import UpdateError
from your_talent.models import SocialNetwork


users_socialnetworks_put = Blueprint('socialnetworks_put', __name__)
logger = Logger(__name__)


@users_socialnetworks_put.route('<sn_id>', methods=['PUT'])
@requires_auth()
@admin_only()
def new_network(sn_id):
    sn = SocialNetwork.get(sn_id)
    sn.set_data(
        request.form,
        ['name', 'logo', 'base_link_url']
    )
    if not sn.save():
        raise UpdateError()
    return jsonify(sn.serialize())
