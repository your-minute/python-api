import uuid
from http import HTTPStatus

import jwt
from flask import Blueprint, jsonify, make_response, request

from your_talent.classes import Logger, Registry
from your_talent.classes.mails import Mail
from your_talent.decorators import check_email_format, check_phone_format, mandatory_fields
from your_talent.errors import AuthError, DuplicateError, InsertError, InvalidFieldError
from your_talent.helpers import CryptHelper, FacebookLogin, FormsHelper, GoogleLogin, TextHelper
from your_talent.models import OAuth2Token, User


users_post = Blueprint('users_post', __name__)
logger = Logger(__name__)


@users_post.route('init', methods=['POST'])
def init_users():
    users = User.query.all()
    if users:
        return jsonify('Already initialized')

    system = User()
    system.set_data({
        'name':           'system',
        'email':          'system@your-minute.fr',
        'email_verified': True,
        'password':       uuid.uuid4(),
    }, [
        'name', 'email', 'email_verified', 'password'
    ])
    if not system.save():
        raise InsertError(details='Could not insert user system')

    admin = User()
    admin.set_data({
        'name':           'admin',
        'email':          'admin@your-minute.fr',
        'email_verified': True,
        'password':       uuid.uuid4(),
    }, [
        'name', 'email', 'email_verified', 'password'
    ])
    if not admin.save():
        raise InsertError(details='Could not insert user admin')

    support = User()
    support.set_data({
        'name':           'support',
        'email':          'support@your-minute.fr',
        'email_verified': True,
        'password':       uuid.uuid4(),
    }, [
        'name', 'email', 'email_verified', 'password'
    ])
    if not support.save():
        raise InsertError(details='Could not insert user support')

    return jsonify('Init ok')


@users_post.route('', methods=['POST'])
@mandatory_fields('name', 'email', 'password')
@check_email_format()
@check_phone_format()
def post_user():
    def duplicate_check():
        check_user = User.query.filter_by(name=request.form['name']).first()
        if check_user:
            raise DuplicateError('name')
        if 'email' in request.form:
            check_user = User.query.filter_by(email=request.form['email']).first()
            if check_user:
                raise DuplicateError('email')
        if 'phone' in request.form:
            check_user = User.query.filter_by(email=request.form['phone']).first()
            if check_user:
                raise DuplicateError('phone')

    if not TextHelper.check_password_format(request.form['password']):
        raise InvalidFieldError(
            'field_name',
            'Password must be at least 6 characters long and contain an uppercase, a lower case and a digit'
        )

    duplicate_check()
    user = User()
    user.set_data(
        request.form,
        [
            'name', 'email', 'login_provider', 'password', 'login_token',
            'phone', 'first_name', 'last_name', 'address', 'city', 'country',
            'public_name', 'public_email', 'public_phone', 'avatar_id', 'role_id'
        ]
    )

    if not user.save():
        raise InsertError()

    if user.email:
        mail = Mail(
            to_addr=user.email,
            from_name='YourTalent',
            # from_addr='no-reply@your-talent.fr',
            from_addr='titouanfreville@gmail.com',
            subject='YourTalent validation Email',
        )
        url = TextHelper.url_from_config('front', 'validation/{}', user.action_token)
        mail.use_template('user-activation.html', name=user.name, url=url)
        mail.send()

    user.link_to_session()
    return jsonify(user.serialize(public=False))


@users_post.route('login', methods=['POST'])
@mandatory_fields('user_name', 'login_provider')
def login_user():
    config = Registry.registered('config')
    provider = User.get_provider(request.form['login_provider'])
    user = User.get(request.form['user_name'])
    provided_token = request.headers.get('Authorization', 'a  ').split(' ')[1]

    if not provided_token or provided_token == ' ':
        provided_token = request.cookies.get('token')
        if provided_token:
            provided_token = jwt.decode(
                provided_token,
                config.get('jwt', 'key'),
                algorithm=config.get('jwt', 'algorithm')
            )['access_token']

    if user and provided_token:
        token = OAuth2Token.query.filter_by(user_id=user.id, access_token=provided_token).first()

        if token and not token.is_refresh_token_expired():
            return jsonify({
                'response': 'Logged in',
                'user':     user.serialize(public=False),
                'is_admin': user.role and user.role.name == 'admin',
            }), HTTPStatus.OK.value

    if user and provider == User.YOUR_TALENT_LOGIN:
        FormsHelper.check_mandatory_fields('password')
        log_ok = user.check_password(request.form['password'])
    else:
        log_ok, user = log_or_sign(provider, user)

    if not log_ok:
        raise AuthError('Either the user is not known or the login failed', subcode='LoginFailed')

    token = str(uuid.uuid4())

    token_obj = OAuth2Token()
    token_obj.user_id = user.id
    token_obj.access_token = token
    token_obj.scope = 'full'

    if not token_obj.save():
        raise InsertError('Token')

    response = make_response(
        jsonify({
            'csrf_token': token,
            'user':       user.serialize(public=False),
            'is_admin':   user.role and user.role.name == 'admin',
        }), HTTPStatus.OK.value
    )
    credentials = dict()
    credentials['access_token'] = token
    credentials['csrf_token'] = CryptHelper.encrypt(token).decode('utf-8')
    config = Registry.registered('config')
    response.set_cookie(
        'token',
        jwt.encode(
            credentials,
            config.get('jwt', 'key'),
            algorithm=config.get('jwt', 'algorithm')
        ),
        httponly=config.getboolean('cookies', 'httponly'),
        secure=config.getboolean('cookies', 'secure')
    )
    return response


def log_or_sign(provider, user=None):
    FormsHelper.check_mandatory_fields('login_token')
    log_token = request.form['login_token']

    if provider == User.FACEBOOK_LOGIN:
        log_ok, user_id, user_info = FacebookLogin.check_token(log_token)
    elif provider == User.GOOGLE_LOGIN:
        log_ok, user_id, user_info = GoogleLogin.check_token(log_token)
    else:
        raise AuthError()

    if user and user.deleted:
        raise AuthError('User was deleted', subcode='UserDeleted')

    if user and not user.check_login_id(user_id):
        raise AuthError('UserId does not match registered user from email: {}. Check your login provider'.format(
            user.email
        ), subcode='LoginProviderIssue')

    if not user:
        user = User()
        if user_info:
            user.set_data(user_info, ['login_provider_user', 'email', 'name'])
        else:
            user.login_provider_user = user_id
            user.email = request.form['user_name']
            user.name = request.form['user_name'].split('@')[0]
            if '.' in user.name:
                splited = user.name.split('.')
                if len(splited) > 1 and splited[1]:
                    user.name = '{} {}.'.format(splited[0], splited[1][0])
        user.login_provider = provider
        user.email_verified = 1

    if not user.save():
        return None

    return log_ok, user
