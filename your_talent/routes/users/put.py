from flask import Blueprint, jsonify, request

from your_talent.classes import Registry
from your_talent.decorators import requires_auth
from your_talent.errors import UnauthorizedError, UpdateError
from your_talent.helpers import FormsHelper
from your_talent.models import User


users_put = Blueprint('users_put', __name__)


@users_put.route('', methods=['PUT'])
@users_put.route('<user_id>', methods=['put'])
@requires_auth()
def put_user(user_id=None):
    user_id = user_id if user_id else Registry.registered('current-user-id')
    user = User.get(user_id)
    role = Registry.registered('current-role')
    if not role or not role.user_is_allowed('manage_user', True, user.id):
        raise UnauthorizedError()

    allowed_update = [
        'name', 'email', 'login_provider', 'password', 'login_token',
        'phone', 'first_name', 'last_name', 'address', 'city', 'country',
        'public_name', 'public_email', 'public_phone', 'avatar_id'
    ]

    if role.user_is_allowed('manage_role', False):
        allowed_update.extend('role_id')

    user.set_data(
        request.form, allowed_update
    )
    multi_form = FormsHelper.parse_multi_level(request.form)
    sn_errors, message = user.set_social_networks(
        multi_form.get('social_network')
    )
    avatar_ok, av_message = user.set_avatar(
        multi_form.get('avatar')
    )
    if not user.save():
        raise UpdateError()

    return jsonify(
        {
            'user':          user.serialize(public=False),
            'has_errors':    sn_errors,
            'error_message': message,
        }
    )
