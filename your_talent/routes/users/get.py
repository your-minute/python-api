from flask import Blueprint, jsonify, request
from sqlalchemy.sql.elements import or_

from your_talent.classes import Registry
from your_talent.classes.logger import Logger
from your_talent.decorators import add_pagination, requires_auth
from your_talent.errors import IDNotFoundError
from your_talent.helpers import FormsHelper, TextHelper
from your_talent.models import User


logger = Logger(__name__)

users_get = Blueprint('users_get', __name__)


@users_get.route('', methods=['GET'])
@add_pagination()
@requires_auth(False)
def get_all(page, per_page):
    filter_by, filters, order = get_filters_and_orders()
    logger.debug('filter_by: {}\nfilters: {}\norder: {}'.format(filter_by, filters, order))
    users, count = User().paginated_query(page, per_page, filter_by, filters, order)
    role = Registry.registered('current-role')
    public_serial = True
    if role and role.user_is_allowed('manage_user'):
        public_serial = False

    res = []

    for u in users:
        res.append(u.serialize(public_serial))
    return res, count


@users_get.route('<user_id>', methods=['GET'])
@requires_auth()
def get_one(user_id):
    user = User.get(user_id)
    if not user:
        raise IDNotFoundError()
    public_serial = True
    role = Registry.registered('current-role')
    if role and role.user_is_allowed('manage_user', True, user.id):
        public_serial = False
    return jsonify(user.serialize(public=public_serial))


def get_filters_and_orders():
    order_dict = {
        'name':           User.name,
        'email':          User.email,
        'login_provider': User.login_provider,
        'phone':          User.phone,
        'public_name':    User.public_name,
        'public_email':   User.public_email,
        'public_phone':   User.public_phone
    }

    filter_by, filters = FormsHelper.filters_to_dict(
        {},
        ['id', 'avatar_id', 'role_id', 'email_verified', 'phone_verified', 'wrong_password_attempt'],
        request.args,
    ), []

    deleted = request.args.get('deleted')
    if deleted:
        deleted = TextHelper.to_bool(deleted)
        filter_by['deleted'] = deleted

    name = request.args.get('name')
    if name:
        filters.append(or_(
            User.name.like('%{}%'.format(name)),
            User.first_name.like('%{}%'.format(name)),
            User.last_name.like('%{}%'.format(name)),
        ))

    email = request.args.get('email')
    if email:
        filters.append(or_(
            User.email.like('%{}%'.format(email)),
            User.public_email.like('%{}%'.format(email)),
        ))

    phone = request.args.get('phone')
    if phone:
        filters.append(or_(
            User.phone.like('%{}%'.format(phone)),
            User.public_phone.like('%{}%'.format(phone)),
        ))

    address = request.args.get('address')
    if address:
        filters.append(User.address.like('%{}%'.format(address)))

    login_provider = request.args.get('login_provider')
    if login_provider:
        tmp_dict = []
        for lp in login_provider.split(','):
            if not TextHelper.is_number(lp):
                lp = User.get_provider(lp)
            tmp_dict.append(User.login_provider == lp)
        filters.append(or_(*tmp_dict))

    return filter_by, filters, FormsHelper.get_order_by(order_dict, request.args)
