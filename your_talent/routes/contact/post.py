from flask import Blueprint, jsonify, request

from your_talent.classes.mails import Mail
from your_talent.decorators import mandatory_fields


contact_post = Blueprint('contact_post', __name__)


@contact_post.route('', methods=['POST'])
@mandatory_fields('email', 'message', 'first_name', 'last_name')
def get_contact():
    first_name = request.form.get('first_name')
    last_name = request.form.get('last_name')
    name = '{} {}'.format(first_name, last_name)
    email = request.form.get('email')
    message = request.form.get('message')
    mail = Mail(
        to_addr='titouanfreville@gmail.com',
        from_name=name,
        from_addr=email,
        subject='Demande de contact'
    )
    mail.use_template(
        'contact.html', first_name=first_name, last_name=last_name, message=message
    )
    mail.send()
    return jsonify('ok')
