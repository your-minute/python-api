from datetime import datetime, timedelta

from flask import Blueprint, jsonify, request

from your_talent.classes import Logger, Registry, Slack
from your_talent.decorators import assert_is_number, mandatory_fields, requires_auth
from your_talent.errors import AuthError, CustomError, DuplicateError, InsertError, ResourceNotFoundError
from your_talent.helpers import DateHelper, info_light, TextHelper
from your_talent.models import File, User, Video
from your_talent.models.settings import Setting
from your_talent.routes.videos.get import display_public


logger = Logger(__name__)

videos_post = Blueprint('videos_post', __name__)


@videos_post.route('', methods=['POST'])
@requires_auth()
@mandatory_fields('name', 'category', 'file_key')
def post_one():
    cur_user = Registry.registered('current-user')
    role = Registry.registered('current-role')
    check_delay = True

    if role.is_allowed('manage_video', role):
        user_id = request.form.get('user_id')
        if user_id:
            user = User.get(user_id)
            if not user or user.deleted:
                raise ResourceNotFoundError('Provided user id does not match', )
            cur_user = user
            check_delay = False

    if role.name != 'admin' and check_delay:
        video = Video.query.filter_by(posted_by=cur_user.id).order_by(Video.posted_at).first()
        if video and video.republish_delay > 0:
            raise CustomError(
                'Last published video less than {} days  ago.'.format(Setting.get('video_republish_delay', 8 * 7)),
                subcode='PublishedTooSoon',
                message='Wait {} days to publish a new video'.format(video.republish_delay)
            )

    video = Video()
    video.set_data(
        request.form,
        [
            'name', 'category', 'file_key', 'comment', 'youtube_key'
        ]
    )

    video.poster = cur_user

    if not video.save():  # pragma : no-cover
        raise InsertError()

    file = File.get(video.file_key)
    if file:
        file.update_acl(
            'authenticated_read', user=Registry.registered('current-user-id'),
            role=Registry.registered('current-role')
        )
        file.uploaded = True
        file.save(False)
    else:
        Slack.send('#api_errors', 'No file found for video: {}, key: {}'.format(video.name, video.file_key))

    video.link_to_session()
    Slack.attachment(
        '#new_videos',
        fallback='New Video',
        color=info_light,
        title='Nouvelle Vidéo',
        text="Une vidéo vient d'être postée",
        footer='Video: {}'.format(TextHelper.url_from_config('bo', '/videos?id={}', video.id)),
        fields=[
            {
                'title': 'Vidéo',
                'value': '{}'.format(video.name),
                'short': True
            },
            {
                'title': 'Par',
                'value': '{}'.format(video.poster.name),
                'short': True
            },
            {
                'title': 'Category',
                'value': '{}'.format(video.category),
                'short': True
            },
        ]
    )
    video.link_to_session()
    return jsonify(video.serialize(display_public(video.posted_by)))


@videos_post.route('upload', methods=['POST'])
@requires_auth()
@mandatory_fields('video_name')
@assert_is_number('length', 'int')
def upload():
    logged_user = Registry.registered('current-user')

    if not logged_user:
        raise AuthError()

    user = logged_user
    role = Registry.registered('current-role')
    if role.is_allowed('manage_video', role):
        user_id = request.form.get('user_id')
        if user_id:
            tmp_user = User.get(user_id)
            if not tmp_user or tmp_user.deleted:
                raise ResourceNotFoundError('Provided user id does not match', )
            user = tmp_user

    file_key = '{}_{}'.format(
        user.name,
        request.form['video_name']
    )

    video_key = '{}/{}_{}'.format(
        user.name,
        DateHelper.date_to_string(datetime.utcnow(), output_format='%Y/%m/%d'),
        request.form['video_name']
    )
    file = File.get(file_key)

    # duplicated link generation
    if file and file.uploaded:
        raise DuplicateError('Video')

    elif file and file.created_at + timedelta(minutes=30) < datetime.utcnow():
        return {'response': file.tmp_upload_info, 'key': file.key}

    if not file:
        file = File()

    ok, res_key, response = file.get_upload_url(
        video_key, file_key, mime_type=request.form.get('type'), length=request.form.get('length', 0),
        publisher={
            'id':   logged_user.id,
            'type': role.name,
        },
        owner={
            'id':   user.id,
            'type': user.role.name
        },
        up_type='video'
    )
    if not ok:
        raise InsertError()
    # The response contains the presigned URL and required fields
    return {'response': response, 'key': res_key}
