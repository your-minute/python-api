from datetime import datetime, timedelta

from flask import Blueprint, jsonify, request
from sqlalchemy import or_

from your_talent.classes import Logger, Registry, Slack
from your_talent.decorators import admin_only, assert_is_number, mandatory_fields, requires_auth, requires_rights
from your_talent.errors import (
    CustomError, DuplicateError, IDNotFoundError, InsertError, ResourceNotFoundError, UpdateError
)
from your_talent.helpers import DateHelper, FormsHelper, ok_light, TextHelper
from your_talent.models import Channel, Planning, PlanningSetting, Role, Video
from your_talent.models.top import Vote
from your_talent.routes.videos.get import display_public


logger = Logger(__name__)
videos_put = Blueprint('videos_put', __name__)


@videos_put.route('<video_id>', methods=['PUT'])
@requires_auth()
@requires_rights('manage_video')
def update_video(video_id):
    video = Video.get(video_id)
    if not video:
        raise ResourceNotFoundError(resource='Video')

    video.set_data(request.form, ['category', 'youtube_key', 'sponsored'])
    if not video.save():
        raise UpdateError()
    return jsonify(video.serialize(False))


@videos_put.route('<video_id>/plan', methods=['PUT'])
@requires_auth()
@requires_rights('manage_video')
@mandatory_fields('channel', ['plan_at', 'planning_id'])
def plan_video(video_id):
    video = Video.get(video_id)
    channel = Channel.get(request.form.get('channel'))
    if not video:
        raise ResourceNotFoundError(resource='Video')
    if not channel:
        raise ResourceNotFoundError(resource='Channel')

    yt_key = request.form.get('youtube_key')
    if yt_key:
        yt_video = Video.query.filter_by(youtube_key=yt_key).filter(Video.id != video_id).first()
        if yt_video:
            raise DuplicateError('youtube_key')
        video.youtube_key = yt_key
        if not video.save():
            raise UpdateError()

    plan_at = FormsHelper.get_datetime(request.form, 'plan_at', t_format='%Y-%m-%d %H:%M')
    planning = None
    if plan_at:
        plannings = Planning.query.filter(Planning.start_at <= plan_at, Planning.end_at >= plan_at).all()
        for p in plannings:
            if p.channel == channel:
                planning = p
                break
    if 'planning_id' in request.form and Role.is_allowed('manage_video', Registry.registered('current-role-name')):
        planning = Planning.query.get(request.form.get('planning_id'))

    if not planning:
        planning = Planning()
        planning.start_at = plan_at
        planning.end_at = plan_at + timedelta(hours=1)
        planning.setting = PlanningSetting.query.filter_by(name='default').first()
        if not planning.save():
            raise InsertError('Could not save planning instance for video', 'Planning')

    if not video.plan_video(channel, planning, plan_at):
        raise InsertError('Could not plan video', 'PlannedVideo')

    video.link_to_session()

    Slack.attachment(
        '#new_videos',
        fallback='New videos',
        color=ok_light,
        title='Vidéo plannifiée',
        text='Vidéo plannifiée :V:',
        footer='Vidéo: {}\nChannel: {}'.format(
            TextHelper.url_from_config('bo', '/videos?id={}', video.id),
            TextHelper.url_from_config('bo', '/channels/{}', channel.id),
        ),
        fields=[
            {
                'title': 'Vidéo', 'value': video.name, 'short': True
            },
            {
                'title': 'Channel', 'value': channel.name, 'short': True
            },
            {
                'title': 'At',
                'value': '{}'.format(
                    DateHelper.date_to_string(plan_at, output_format='%d-%m-%Y %H:%M', tz='Europe/Paris')
                ),
                'short': True
            },
        ]
    )

    video.link_to_session()
    return jsonify(video.serialize(display_public(video.posted_by)))


@videos_put.route('<video_id>/validate', methods=['PUT'])
@requires_auth()
@requires_rights('validate_video')
@mandatory_fields('valid')
def validate_video(video_id):
    video = Video.get(video_id)
    if not video:
        raise IDNotFoundError()

    valid = FormsHelper.get_boolean(request.form, 'valid')
    reason = request.form.get('reason')

    if not valid:
        FormsHelper.check_mandatory_fields(request.form, ['reason'])

    user = Registry.registered('current-user')
    video.validate_video(valid, user, reason)
    video.link_to_session()
    return jsonify(video.serialize())


@videos_put.route('<video_id>/reset_validation_status', methods=['PUT'])
@requires_auth()
@admin_only()
def reset_validation(video_id):
    video = Video.get(video_id)
    if not video:
        raise IDNotFoundError()
    video._status = video.VALIDATING
    if not video.save():
        raise UpdateError()
    return jsonify(video.serialize())


@videos_put.route('<video_id>/vote', methods=['PUT'])
@requires_auth()
@mandatory_fields('vote')
@assert_is_number('vote', 'int')
def vote(video_id):
    user = Registry.registered('current-user')
    video = Video.query.filter(
        or_(Video.id == video_id, Video.name == video_id),
        Video._status.in_([Video.TOPPED, Video.PLANNED])
    ).first()

    if not video:
        raise ResourceNotFoundError('Provided video does not exist or is not planned', 'Video')

    channel = video.channel
    planning = video.planning

    if not planning:
        raise ResourceNotFoundError('Provided video does not exist or is not planned', 'Video')

    vote = None

    logger.debug(planning.type)
    logger.debug(planning.start_at)
    logger.debug(planning.end_at)

    if (
            planning.type == PlanningSetting.FREE
            and planning.start_at <= datetime.utcnow() <= planning.end_at
    ):

        vote = Vote.query.filter_by(video_id=video.id, user_id=user.id, planning_id=planning.id).first()
        if not vote:
            vote = Vote()
            vote.video_id = video.id
            vote.user_id = user.id
            vote.planning_id = planning.id

        vote.vote = request.form.get('vote')

        if not vote.save():
            raise InsertError()

    if not vote:
        raise CustomError('Not implemented yet')
    video.refresh()
    return jsonify(video.serialize())
