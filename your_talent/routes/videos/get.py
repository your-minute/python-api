from flask import Blueprint, jsonify, request
from sqlalchemy import or_

from your_talent.classes import Registry
from your_talent.classes.logger import Logger
from your_talent.decorators import add_pagination, requires_auth
from your_talent.errors import IDNotFoundError
from your_talent.helpers import FormsHelper, TextHelper
from your_talent.models import Planning, Top, Video


logger = Logger(__name__)

videos_get = Blueprint('videos_get', __name__)

order_dict = {
    'name':     Video.name,
    'planning': Planning.id,
    'top':      Top.id,
}


def display_public(user_id=None):
    role = Registry.registered('current-role')
    if role and role.user_is_allowed('manage_user', user_id is not None, user_id):
        return False
    return True


@videos_get.route('', methods=['GET'])
@requires_auth(False)
@add_pagination()
def get_all(page, per_page):
    filter_by, filters = get_filters()
    order = FormsHelper.get_order_by(order_dict, request.args)
    order.append(Video.id)
    videos, count = Video().paginated_query(
        page, per_page, filter_by, filters, order,
        Video.query.outerjoin(Video.planning).outerjoin(Top)
    )
    res = []

    for v in videos:
        res.append(v.serialize(display_public(), Registry.registered('current-role-name') == 'admin'))
    return res, count


@videos_get.route('<video_id>', methods=['GET'])
@requires_auth(False)
def get_one(video_id):
    video = Video.get(video_id)
    if not video:
        raise IDNotFoundError()

    return jsonify(
        video.serialize(display_public(video.posted_by), Registry.registered('current-role-name') == 'admin')
    )


@videos_get.route('<video_id>/score', methods=['GET'])
@requires_auth(False)
def get_video_score(video_id):
    video = Video.get(video_id)
    if not video:
        raise IDNotFoundError()

    return jsonify(video.score)


def get_filters():
    """
    Get filters fot video

    :return: filters list and filter_by dict
    :rtype: tuple
    """
    filter_by, filters = FormsHelper.filters_to_dict(
        {},
        ['id'],
        request.args,
    ), []

    name = request.args.get('name', '').split(',')
    if name:
        tmp = []
        for n in name:
            tmp.append(Video.name.like('%{}%'.format(n)))
        filters.append(or_(*tmp))

    status = request.args.get('status', '').split(',')
    if status:
        tmp = []
        for s in status:
            if not TextHelper.is_int(s):
                s = Video.conv_status(s)
            if s:
                tmp.append(Video.status == s)
            filters.append(or_(*tmp))

    if 'sponsored' in request.args:
        filter_by['sponsored'] = FormsHelper.get_boolean(request.args, 'sponsored')

    if Registry.registered('current-role-name') == 'admin':
        top_kind = request.args.get('top_kind')
        if top_kind:
            filters.append(Top.name.like('%{}%'.format(top_kind)))

        plannings = request.args.get('plannings', '').split(',')
        if plannings:
            tmp = []
            for p in plannings:
                if TextHelper.is_int(p):
                    tmp.append(Planning.id == p)
            filters.append(or_(*tmp))

    return filter_by, filters
