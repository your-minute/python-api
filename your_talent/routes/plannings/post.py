from http import HTTPStatus

from flask import Blueprint, jsonify, request

from your_talent.decorators import admin_only, mandatory_fields, requires_auth
from your_talent.errors import InsertError
from your_talent.helpers import FormsHelper
from your_talent.models import Planning, PlanningSetting


plannings_post = Blueprint('plannings_post', __name__)


@plannings_post.route('', methods=['POST'])
@requires_auth()
@admin_only()
@mandatory_fields('start_at', 'end_at', 'top_id', ['setting_id', 'setting'])
def planning_post_one():
    planning = Planning()
    planning.set_data(
        request.form, ['setting_id', 'description', 'required_age', 'top_id', 'channel_id']
    )
    
    planning.start_at = FormsHelper.get_datetime(request.form, 'start_at')
    planning.end_at = FormsHelper.get_datetime(request.form, 'end_at')

    if 'setting' in request.form:
        setting_json = FormsHelper.get_json(request.form, 'setting')
        FormsHelper.check_mandatory_fields(setting_json, ['name'])
        setting = PlanningSetting()
        setting.set_data(
            setting_json, [
                'name', 'time_shift', 'last_vote_duration', 'summary_duration',
                'transition_duration', 'is_fake_live', 'type'
            ]
        )
        setting.is_fake_live = FormsHelper.get_boolean(setting_json, 'is_fake_live')
        setting.save(commit=False)
        planning.setting = setting
        planning.setting_id = setting.id

    if not planning.save():  # pragma : no-cover
        raise InsertError()

    return jsonify(planning.serialize()), HTTPStatus.CREATED.value
