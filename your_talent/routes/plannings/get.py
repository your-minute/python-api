from flask import Blueprint, request

from your_talent.decorators import add_pagination, admin_only, requires_auth
from your_talent.helpers import FormsHelper
from your_talent.models import Planning


plannings_get = Blueprint('plannings_get', __name__)


@plannings_get.route('', methods=['GET'])
@requires_auth()
@admin_only()
@add_pagination()
def get_plannings(page, per_page):
    filter_by, filters = FormsHelper.filters_to_dict(
        {},
        ['id'],
        request.args,
    ), []

    if 'start_at' in request.args:
        start_at = FormsHelper.get_datetime(request.args, 'start_at')
        filters.append(Planning.start_at >= start_at)

    if 'end_at' in request.args:
        end_at = FormsHelper.get_datetime(request.args, 'end_at')
        filters.append(Planning.end_at <= end_at)

    plannings, count = Planning().paginated_query(page, per_page, filter_by, filters)

    return [p.serialize() for p in plannings], count
