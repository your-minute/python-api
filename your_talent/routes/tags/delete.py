from flask import Blueprint, jsonify

from your_talent.errors import IDNotFoundError, DeleteError
from your_talent.models import Tag


tags_delete = Blueprint('tags_delete', __name__)


@tags_delete.route('<tag_id>', methods=['DELETE'])
def delete_one(tag_id):
    tag = Tag().get(tag_id)
    if not tag:
        raise IDNotFoundError()
    if not tag.delete():  # pragma : no-cover
        raise DeleteError()
    return jsonify(tag.serialize())
