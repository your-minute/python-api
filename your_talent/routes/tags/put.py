from flask import Blueprint, jsonify, request

from your_talent.errors import IDNotFoundError, UpdateError
from your_talent.models import Tag


tags_put = Blueprint('tags_put', __name__)


@tags_put.route('<tag_id>', methods=['PUT'])
def put_one(tag_id):
    tag = Tag().get(tag_id)
    if not tag:
        raise IDNotFoundError()
    tag.set_data(request.form, ['name', 'description'])
    if not tag.save():  # pragma : no-cover
        raise UpdateError()
    return jsonify(tag.serialize())
