from flask import Blueprint, jsonify, request
from sqlalchemy import or_

from your_talent.decorators import add_pagination
from your_talent.errors import IDNotFoundError
from your_talent.helpers import FormsHelper
from your_talent.models import Tag


tags_get = Blueprint('tags_get', __name__)

order_dict = {
    'name': Tag.name,
}


@tags_get.route('', methods=['GET'])
@add_pagination()
def get_all(page, per_page):
    filter_by, filters = get_filters()
    order = FormsHelper.get_order_by(order_dict, request.args)
    tags, count = Tag().paginated_query(page, per_page, filter_by, filters, order)
    res = []
    for c in tags:
        res.append(c.serialize())
    return res, count


@tags_get.route('<tag_id>', methods=['GET'])
def get_one(tag_id):
    tag = Tag.get(tag_id)
    if not tag:
        raise IDNotFoundError()

    return jsonify(tag.serialize())


def get_filters():
    """
    Get filters fot tag

    :return: filters list and filter_by dict
    :rtype: tuple
    """
    filters, filter_by = [], {}

    name = request.args.get('name', '').split_by(',')
    if name:
        tmp = []
        for n in name:
            tmp.append(Tag.name.like('%{}%'.format(n)))
        filters.append(or_(*tmp))

    description = request.args.get('description', '').split_by(',')
    if description:
        tmp = []
        for d in description:
            tmp.append(Tag.description.like('%{}%'.format(d)))
        filters.append(or_(*tmp))

    return filter_by, filters
