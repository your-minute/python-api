from flask import Blueprint, jsonify, request

from your_talent.decorators import mandatory_fields
from your_talent.errors import InsertError
from your_talent.models import Tag


tags_post = Blueprint('tags_post', __name__)


@tags_post.route('', methods=['POST'])
@mandatory_fields('name')
def post_one():
    tag = Tag()
    tag.set_data(request.form, ['name', 'description'])
    if not tag.save():  # pragma : no-cover
        raise InsertError()
    return jsonify(tag.serialize())
