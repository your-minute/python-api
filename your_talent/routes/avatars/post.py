import uuid
from datetime import datetime, timedelta

from flask import Blueprint, request

from your_talent.classes import Registry
from your_talent.decorators import assert_is_number, requires_auth
from your_talent.errors import AuthError, InsertError, ResourceNotFoundError
from your_talent.models import File, User


avatars_post = Blueprint('avatars_post', __name__)


@avatars_post.route('upload', methods=['POST'])
@requires_auth()
@assert_is_number('length', 'int')
def upload():
    logged_user = Registry.registered('current-user')

    if not logged_user:
        raise AuthError()

    user = logged_user
    role = Registry.registered('current-role')
    if role.is_allowed('manage_avatar', role):
        user_id = request.form.get('user_id')
        if user_id:
            tmp_user = User.get(user_id)
            if not tmp_user or tmp_user.deleted:
                raise ResourceNotFoundError('Provided user id does not match', )
            user = tmp_user

    file_key = 'avatar_{}'.format(
        user.name
    )

    file = File.get(file_key)
    avatar_key = 'avatars/{}'.format(
        str(uuid.uuid4())
    )
    # duplicated link generation
    if file and file.created_at + timedelta(minutes=30) < datetime.utcnow():
        return {'response': file.tmp_upload_info, 'key': file.key}

    if file:
        avatar_key = file.s3_key
    else:
        file = File()

    ok, res_key, response = file.get_upload_url(
        avatar_key, file_key, mime_type=request.form.get('type'),
        length=request.form.get('length', 0),
        publisher={
            'id':   logged_user.id,
            'type': role.name,
        },
        owner={
            'id':   user.id,
            'type': user.role.name
        },
        up_type='avatar'
    )

    if not ok:
        raise InsertError()

    # The response contains the presigned URL and required fields
    return {'response': response, 'key': res_key}
