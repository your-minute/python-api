from flask import Blueprint, jsonify, request

from your_talent.decorators import assert_is_number, requires_auth, requires_rights
from your_talent.errors import IDNotFoundError, InsertError
from your_talent.helpers import FormsHelper
from your_talent.models.top import Top


tops_put = Blueprint('tops_put', __name__)


@tops_put.route('<top_id>', methods=['PUT'])
@requires_auth()
@requires_rights('manage_top')
@assert_is_number(['duration', 'frequency_value'], 'int')
def update_top(top_id):
    top = Top.query.get(top_id)
    if not top:
        raise IDNotFoundError()

    top.set_data(request.form, ['name', 'description', 'duration'])

    if 'pinned' in request.form:
        top.pinned = FormsHelper.get_boolean(request.form, 'pinned')

    if 'from_day' in request.form:
        top.from_day = FormsHelper.get_datetime(request.form, 'from_day', '%Y-%m-%d %H:%M:%S')

    if 'repeat' in request.form:
        top.repeat = FormsHelper.get_boolean(request.form, 'repeat')

    if not top.save(commit=False):  # pragma : no-cover
        raise InsertError()
    if 'for_channels' in request.form:
        top.for_channels = request.form.get('for_channels')

    if not top.save():  # pragma : no-cover
        raise InsertError()

    top.set_rewards(FormsHelper.parse_multi_level(request.form).get('reward'))

    return jsonify(top.serialize())
