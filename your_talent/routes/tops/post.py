from datetime import datetime
from http import HTTPStatus

from flask import Blueprint, jsonify, request

from your_talent.decorators import assert_is_number, mandatory_fields
from your_talent.errors import InsertError
from your_talent.helpers import FormsHelper
from your_talent.models.top import Top


tops_post = Blueprint('tops_post', __name__)


@tops_post.route('init', methods=['POST'])
def init_tops():
    tops = Top.query.all()
    if tops:
        return jsonify('Already initialized')

    weekly_top = Top()
    weekly_top.set_data({
        'name':            'facebook_week',
        'description':     'Weekly facebook top',
        'repeat':          True,
        'frequency_type':  'week',
        'frequency_value': 1,
        'duration':        3 * 60 * 60,
        'duration_kind':   'min',
        'from_day':        datetime.utcnow(),
    }, [
        'name', 'description', 'repeat', 'frequency_type',
        'frequency_value', 'duration', 'from_day'
    ])
    if not weekly_top.save(commit=False):
        raise InsertError(details='Could not insert default top')

    monthly_top = Top()
    monthly_top.set_data({
        'name':            'facebook_month',
        'description':     'Monthly facebook top',
        'repeat':          True,
        'frequency_type':  'month',
        'frequency_value': 1,
        'duration':        3 * 60 * 60,
        'duration_kind':   'min',
        'from_day':        datetime.utcnow(),
        'pinned':          True,
    }, [
        'name', 'description', 'repeat', 'frequency_type',
        'frequency_value', 'duration', 'from_day', 'pinned'
    ])
    if not monthly_top.save(commit=True):
        raise InsertError(details='Could not insert default top')

    return jsonify('Init ok'), HTTPStatus.CREATED.value


@tops_post.route('', methods=['POST'])
@mandatory_fields('name', 'repeat')
@assert_is_number(['duration', 'frequency_value'], 'int')
def post_one():
    top = Top()
    top.set_data(request.form, ['name', 'description', 'repeat', 'duration', 'for_channels', 'duration_kind'])
    top.from_day = FormsHelper.get_datetime(request.form, 'from_day', '%Y-%m-%d %H:%M:%S')
    top.repeat = FormsHelper.get_boolean(request.form, 'repeat')
    top.pinned = FormsHelper.get_boolean(request.form, 'pinned')
    if not top.save():  # pragma : no-cover
        raise InsertError()
    top.set_rewards(FormsHelper.parse_multi_level(request.form).get('reward'))
    return jsonify(top.serialize()), HTTPStatus.CREATED.value
