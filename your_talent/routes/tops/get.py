from flask import Blueprint, request
from sqlalchemy import func, or_

from your_talent.decorators import add_pagination, mandatory_url_args
from your_talent.errors import IDNotFoundError
from your_talent.helpers import FormsHelper
from your_talent.models import Ranking, Top


tops_get = Blueprint('tops_get', __name__)


@tops_get.route('', methods=['GET'])
@add_pagination()
def post_one(page, per_page):
    filter_by, filters = FormsHelper.filters_to_dict(
        {},
        ['id'],
        request.args,
    ), []
    name = request.args.get('name', '').split(',')
    if name:
        tmp = []
        for n in name:
            tmp.append(Top.name.like('%{}%'.format(n)))
        filters.append(or_(*tmp))

    tops, count = Top().paginated_query(page, per_page, filter_by, filters)

    return [top.serialize() for top in tops], count


@tops_get.route('/rankings', methods=['GET'])
@add_pagination()
@mandatory_url_args('top_kind')
def get_ranking(page, per_page):
    top = Top.get(request.args.get('top_kind'))
    if not top:
        raise IDNotFoundError()
    max_rank = request.args.get('max_rank', 1)
    base_query = Ranking.query.with_entities(func.distinct(Ranking.id))
    ranking_id, count = Ranking().paginated_query(
        page, per_page,
        filter_by={'top_id': top.id},
        filters=[Ranking.rank <= max_rank],
        orders=[Ranking.created_at.desc()],
        base_query=base_query
    )
    res = []
    for rid in ranking_id:
        res.append(Ranking.get_ranking_for_id(rid[0], max_rank=max_rank))
    return res, count
