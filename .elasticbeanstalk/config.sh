#!/usr/bin/env bash
set -x
set -e

{
    AWS_CONFIG_FILE=~/.aws/config

    mkdir ~/.aws
    touch $AWS_CONFIG_FILE
    chmod 600 $AWS_CONFIG_FILE

    echo "[profile eb-cli]"                       > $AWS_CONFIG_FILE
    echo "aws_access_key_id=${BEANSTALK_USER}"    >> $AWS_CONFIG_FILE
    echo "aws_secret_access_key=${BEANSTALK_KEY}" >> $AWS_CONFIG_FILE
} &> /dev/null
